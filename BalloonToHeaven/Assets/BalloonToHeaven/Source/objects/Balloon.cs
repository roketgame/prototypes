﻿using System;
using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;

public class Balloon : HyperSceneObj
{
    public LineRenderer lineRenderer;
    public int segment = 8;

    public Transform firstTransform;
    private Transform _hand;

    private void LateUpdate()
    {
        if (_hand != null)
        {
            lineRenderer.SetPosition(0, firstTransform.position);
            lineRenderer.SetPosition(1, _hand.position);
        }
    }

    public override void Update()
    {
        base.Update();
       

        /*
        for (int i = 0; i < _lineRenderer.positionCount; i++)
        {
            if (i != 0)
            {
                float value = Time.deltaTime * 10;

                Vector3 newVector = new Vector3(value, value, -i);

                _lineRenderer.SetPosition(i, newVector);
            }
        }
        */
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);
        if (result == 2)
        {
            _hand = null;
            lineRenderer.useWorldSpace = false;
            lineRenderer.SetPosition(0, new Vector3(0, 0, 0));
            lineRenderer.SetPosition(1, new Vector3(0, 0, -3.5f));
        }
    }

    public void CollectBalloon(Transform handTransform)
    {
        _hand = handTransform;
        lineRenderer.useWorldSpace = true;
        Destroy(GetComponent<Collider>());
    }
}