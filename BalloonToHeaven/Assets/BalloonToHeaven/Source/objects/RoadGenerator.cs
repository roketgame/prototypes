﻿using System;
using System.Collections.Generic;
using System.Linq;
using RoketGame;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoadGenerator : CoreSceneObject
{
    [Header("Prefabs")] public GameObject bombPrefab;
    public GameObject balloonPrefab;
    public GameObject cloudsPrefab;
    public List<GameObject> cloudPrefabs;
    public GameObject stairPrefab;

    [Header("Road Properties")] public List<RoadProperty> roadProperties;
    public RoadProperty currentProperty;
    public List<float> roadYPositions;

    [Header("Movement Properties")] [Range(10f, 100.0f)]
    public float totalDistance;

    public bool generate;

    public Vector3 lastSpawnPoint;

    public float _firstRoadXPosition;
    
    public override void Init()
    {
        currentProperty = roadProperties.Find(property =>
            property.minLevel <= (LevelCont.Instance).CurrLevel && property.maxLevel > (LevelCont.Instance).CurrLevel);

        if (currentProperty == null)
        {
            currentProperty = roadProperties[Random.Range(0, roadProperties.Count)];
        }

        _firstRoadXPosition = 0;

        List<CloudGenerator> cloudGenerators = new List<CloudGenerator>();


        float lastZ = currentProperty.startingZPoint;
        for (float z = 0; z < totalDistance; z++)
        {
            lastZ = currentProperty.startingZPoint +
                    (z * Random.Range(currentProperty.minForwardUnit, currentProperty.maxForwardUnity));
            for (float y = 0; y <= z; y++)
            {
                _firstRoadXPosition =
                    -((currentProperty.totalObjectCountEachOther - 1) * currentProperty.distanceBetweenObstacles) / 2;

                int random = Random.Range(currentProperty.minBombCount, currentProperty.maxBombCount + 1);

                List<byte> emptyList = new List<byte>(currentProperty.totalObjectCountEachOther);


                if (!roadYPositions.Contains(currentProperty.startingYPoint + y * currentProperty.upperUnit))
                {
                    roadYPositions.Add(currentProperty.startingYPoint + y * currentProperty.upperUnit);
                }


                for (int i = 0; i < currentProperty.totalObjectCountEachOther; i++)
                {
                    if (i < random)
                    {
                        emptyList.Add(1);
                    }
                    else
                    {
                        emptyList.Add(0);
                    }
                }

                emptyList = emptyList.OrderBy(x => Guid.NewGuid()).ToList();

                for (int x = 0; x < currentProperty.totalObjectCountEachOther; x++)
                {
                    GameObject spawnObject;


                    Vector3 spawnPosition = new Vector3(
                        _firstRoadXPosition + currentProperty.distanceBetweenObstacles * x,
                        currentProperty.startingYPoint + y * currentProperty.upperUnit,
                        lastZ);

                    lastSpawnPoint = spawnPosition;

                    if (emptyList[x] == 0)
                    {
                        // spawnObject = balloonPrefab;
                        //Instantiate(spawnObject, spawnPosition, Quaternion.Euler(-90, 0, 180), transform);
                    }
                    else
                    {
                        spawnObject = bombPrefab;

                        Instantiate(spawnObject, spawnPosition, Quaternion.identity, transform);
                    }
                }

                if (y + 1 > z)
                {
                    Vector3 spawnPosition = new Vector3(
                        0,
                        currentProperty.startingYPoint + y * currentProperty.upperUnit +
                        currentProperty.cloudHeightAboveGround,
                        0);


                    cloudGenerators.Add(Instantiate(cloudsPrefab, spawnPosition, Quaternion.identity, transform)
                        .GetComponent<CloudGenerator>());
                }

                /*
                {
                    int cloudCount = Random.Range(roadProperty.minCloudCount, roadProperty.maxCloudCount);

                    for (int i = 0; i < cloudCount; i++)
                    {
                        Vector3 spawnPosition = new Vector3(
                            firstRoadXPosition + roadProperty.distanceBetweenObstacles * i,
                            roadProperty.startingYPoint + y * roadProperty.upperUnit +
                            roadProperty.cloudHeightAboveGround,
                            lastZ);

                        int randomCloud = Random.Range(0, cloudPrefabs.Count);
                        Instantiate(cloudPrefabs[randomCloud], spawnPosition, Quaternion.Euler(-90, 0, 180), transform);
                    }
                }
                */
            }
        }

        float lastZBalloon = currentProperty.startingZPoint;

        for (float z = 0; z < totalDistance; z++)
        {
            lastZBalloon = currentProperty.startingZPoint +
                           (z * Random.Range(currentProperty.minBalloonForwardUnit,
                               currentProperty.maxBalloonForwardUnit));
            if (z != 0)
            {
                for (float y = 0; y <= z; y++)
                {
                    _firstRoadXPosition =
                        -((currentProperty.totalObjectCountEachOther - 1) * currentProperty.distanceBetweenObstacles) /
                        2;
                    int random = Random.Range(currentProperty.minBalloonCount, currentProperty.maxBalloonCount + 1);

                    List<byte> emptyList = new List<byte>(currentProperty.totalObjectCountEachOther);

                    for (int i = 0; i < currentProperty.totalObjectCountEachOther; i++)
                    {
                        if (i < random)
                        {
                            emptyList.Add(1);
                        }
                        else
                        {
                            emptyList.Add(0);
                        }
                    }

                    emptyList = emptyList.OrderBy(x => Guid.NewGuid()).ToList();

                    for (int x = 0; x < currentProperty.totalObjectCountEachOther; x++)
                    {
                        if (lastZBalloon < lastZ)
                        {
                            Vector3 balloonSpawnPosition = new Vector3(
                                _firstRoadXPosition + currentProperty.distanceBetweenObstacles * x,
                                currentProperty.balloonHeightOffset + currentProperty.startingYPoint +
                                y * currentProperty.upperUnit, lastZBalloon);


                            if (emptyList[x] == 1)
                            {
                                // Instantiate(balloonPrefab, balloonSpawnPosition, Quaternion.Euler(-90, 0, 180), transform);
                                Balloon balloonObject =
                                    SceneCont.Instance.SpawnItem<Balloon>(balloonPrefab, balloonSpawnPosition,
                                        transform);
                                balloonObject.transform.eulerAngles = new Vector3(-90, 0, 180);
                            }
                        }
                    }
                }
            }
        }


        foreach (var cloudGenerator in cloudGenerators)
        {
            cloudGenerator.Generate(currentProperty, lastZ);
        }

        /*
               GameObject g = new GameObject();
       
               for (float i = -10; i < 10; i += 4)
               {
                   for (float j = roadProperty.startingZPoint; j < lastZ; j += 4)
                   {
                       float perlin = Mathf.PerlinNoise(i, j);
       
                       if (perlin < roadProperty.cloudDensity)
                       {
                           Vector3 spawnPosition =
                               new Vector3(i + Random.Range(-3.5f, 3.5f), 0, j + Random.Range(-3.5f, 3.5f));
       
                           int randomCloud = Random.Range(0, cloudPrefabs.Count);
                           Instantiate(cloudPrefabs[randomCloud], spawnPosition,
                               Quaternion.Euler(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360)), g.transform);
                       }
                   }
               }
       
              
               foreach (var cloudYPosition in roadYPositions)
               {
                   float z = roadProperty.startingZPoint;
                   float x = firstRoadXPosition;
                   float lastX = -x;
       
                   while (z < lastZ)
                   {
                       Debug.Log("Z" + z);
                       while (x < lastX)
                       {
                           float perlin = Mathf.PerlinNoise(x, z);
       
                           if (perlin < roadProperty.cloudDensity)
                           {
                               Vector3 spawnPosition = new Vector3(x, cloudYPosition, z);
       
                               int randomCloud = Random.Range(0, cloudPrefabs.Count);
                               Instantiate(cloudPrefabs[randomCloud], spawnPosition, Quaternion.Euler(0, 0, 0), transform);
                           }
       
                           x++;
                       }
       
                       z++;
                   }
               }
               */
    }

    public GameObject GenerateStair(Vector3 playerPosition)
    {
        Vector3 spawnPosition =
            new Vector3(playerPosition.x, playerPosition.y, lastSpawnPoint.z + 175);

        GameObject gg = Instantiate(stairPrefab, spawnPosition, Quaternion.identity, transform);
        LeanTween.moveLocal(gg.transform.Find("BigStairs").gameObject, Vector3.zero, 1.75f).setOnComplete(o =>
        {
            Transform confettiParent = gg.transform.Find("Confetti");


            foreach (Transform confettiObj in confettiParent.transform)
            {
                if (confettiObj != confettiParent)
                {
                    confettiObj.gameObject.SetActive(true);
                }
            }

            GameObject doorObject = gg.transform.Find("BigStairs").Find("Door").gameObject;
            LeanTween.rotateY(doorObject.transform.Find("LeftDoor").gameObject, -90, 1.0f)
                .setEase(LeanTweenType.easeInOutSine);
            LeanTween.rotateY(doorObject.transform.Find("RightDoor").gameObject, -90, 1.0f)
                .setEase(LeanTweenType.easeInOutSine);
        });
        return gg.transform.Find("PlayerPoint").gameObject;
    }
}