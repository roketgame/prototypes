﻿using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;

public class CloudGenerator : CoreSceneObject
{
    public List<GameObject> cloudPrefabs;

    public void Generate(RoadProperty roadProperty, float lastZ)
    {
        base.Init();
        for (float i = -10; i < 10; i += 4)
        {
            for (float j = roadProperty.startingZPoint; j < lastZ; j += 4)
            {
                float xCoor = i + Random.Range(-10.5f, 10.5f);
                float zCoor = j + Random.Range(-3.5f, 3.5f);
                float perlin = Mathf.PerlinNoise(xCoor, zCoor);

                if (perlin < roadProperty.cloudDensity)
                {
                    Vector3 spawnPosition = new Vector3(xCoor, transform.position.y, zCoor);

                    int randomCloud = Random.Range(0, cloudPrefabs.Count);
                    Instantiate(cloudPrefabs[randomCloud], spawnPosition,
                        Quaternion.Euler(0, 0, 0),
                        transform);
                }
            }
        }
    }
}