﻿using System;
using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;
using Random = UnityEngine.Random;

public class BalloonHolder : CoreSceneObject
{
    public List<Vector3> points;

    private void Start()
    {
        LeanTween.scale(transform.GetChild(0).gameObject, Vector3.one * Random.Range(1, 1.15f), 5)
            .setEase(LeanTweenType.easeShake).setLoopPingPong(-1);;
    }

    public void AddBalloon(GameObject balloon)
    {
        Debug.Log("AddBalloon - " + balloon);
        if (points.Count > 0)
        {
            balloon.transform.position = transform.position + points[0];
            balloon.transform.parent = transform;
            LeanTween.scale(balloon.transform.gameObject, Vector3.one * Random.Range(1, 1.15f), 5)
                .setEase(LeanTweenType.easeShake).setLoopPingPong(-1);;
            points.RemoveAt(0);
        }
    }

    private void OnDrawGizmos()
    {
        foreach (var point in points)
        {
            Gizmos.DrawSphere(transform.position + point, 0.15f);
        }
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);

        if (result == 2)
        {
            foreach (Transform child in transform)
            {
                if (child != transform)
                {
                    LeanTween.moveY(child.gameObject, child.position.y + 10, 2.5f);
                }
                
            }
        }
    }
}