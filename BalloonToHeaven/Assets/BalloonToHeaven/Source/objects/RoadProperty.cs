﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "road_property_data", menuName = "BalloonToHeaven/RoadProperty", order = 1)]
public class RoadProperty : ScriptableObject
{
    public int minLevel;
    public int maxLevel;
    public Difficulty difficulty = Difficulty.Medium;

    [Header("Bomb")] public int minBombCount = 2;
    public int maxBombCount = 5;

    [Header("Balloon")] public int minBalloonForwardUnit = 5;
    public float balloonHeightOffset = 3;
    public int maxBalloonForwardUnit = 8;
    public int minBalloonCount = 1;
    public int maxBalloonCount = 2;

    [Header("X Properties")] public float distanceBetweenObstacles = 3.25f;
    [Range(3, 15)] public int totalObjectCountEachOther = 5;
    [Range(1, 3)] public int frequencyBeingEmpty = 2;

    [Header("Y Properties")] [Range(0, 30.0f)]
    public float startingYPoint = 4f;

    [Range(3.0f, 250.0f)] public float upperUnit = 15.5f;

    [Header("Z Properties")] [Range(7.5f, 30.0f)]
    public float startingZPoint = 12.5f;

    public float minForwardUnit = 10;
    public float maxForwardUnity = 50;

    [Header("Cloud Property")] [Range(0, 1)]
    public float cloudDensity = 0.5f;

    public float cloudHeightAboveGround = 5;
}

public enum Difficulty
{
    Easy,
    Medium,
    Hard,
    VeryHard,
    Insane
}