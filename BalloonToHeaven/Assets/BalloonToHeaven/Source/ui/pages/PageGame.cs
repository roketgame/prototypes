﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;
using UnityEngine.UI;

public class PageGame : HyperPageGame
{
    [Header("Balloon")] public Image image;

    private PlayerController _playerController;
    private RoadGenerator _roadGenerator;

    public TextMeshProUGUI heightMeter;

    private float firstSize;
    private float lastSize;


    public override void Init()
    {
        base.Init();

        _playerController = FindObjectOfType<PlayerController>();
        _roadGenerator = FindObjectOfType<RoadGenerator>();

        firstSize = _playerController.firstBalloonTransform.position.y;
        heightMeter.enabled = false;
    }

    public override void Update()
    {
        base.Update();

        //image.fillAmount = (_playerController.firstBalloonTransform.position.y - firstSize) /
        //((_roadGenerator.lastSpawnPoint.y) - firstSize);

        if (IsGameStarted)
        {
            heightMeter.enabled = true;
            heightMeter.text = _playerController.GetScore() + " M";
        }
    }


    protected override void onBtnStartClicked(TouchUI _touch)
    {
        base.onBtnStartClicked(_touch);
    }

    public void UpdateWillCatch(Dictionary<string, float> listWillCatch)
    {
        foreach (KeyValuePair<string, float> elem in listWillCatch)
        {
            SetText(elem.Key, elem.Value.ToString());
        }
    }
}