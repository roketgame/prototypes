﻿using System;
using System.Collections;
using RoketGame;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : HyperPlayerCont
{
    public Vector3 startPoint;
    private CharacterJoint _characterJoint;
    public Transform firstBalloonTransform;
    public float moveSpeed = 5;
    public float strafeSpeed = 3;
    public Animator animator;
    private float _cameraZposition;
    private float _maxXPosition;
    private BalloonHolder _balloonHolder;
    private RoadGenerator _roadGenerator;

    public Transform handTransform;

    private bool _isStart;
    private bool _isControl;

    private float _risingHeight;

    private void Awake()
    {
        GetComponent<Rigidbody>().isKinematic = true;

        _cameraZposition = Camera.main.GetComponent<CameraController>().offset.z;
        _balloonHolder = firstBalloonTransform.GetComponent<BalloonHolder>();
        _roadGenerator = FindObjectOfType<RoadGenerator>();
    }

    public override void StartGame()
    {
        base.StartGame();
        RoadGenerator roadGenerator = FindObjectOfType<RoadGenerator>();
        RoadProperty roadProperty = roadGenerator.currentProperty;
        _maxXPosition = roadProperty.distanceBetweenObstacles * (roadProperty.totalObjectCountEachOther - 1) / 2;

        _risingHeight = roadProperty.upperUnit;


        animator.SetTrigger("Start");

        LeanTween.move(gameObject, startPoint, 1.5f).setOnComplete(o =>
        {
            GetComponent<Rigidbody>().isKinematic = false;
            _characterJoint = GetComponent<CharacterJoint>();
            _characterJoint.connectedBody = firstBalloonTransform.GetComponent<Rigidbody>();
            animator.SetTrigger("Grab");
            _isStart = true;
            _isControl = true;
            firstBalloonTransform.GetComponentInChildren<Balloon>().CollectBalloon(handTransform);
        });
    }

    private Vector3 _firstPosition;
    private Vector3 _firstBalloonPosition;
    private Vector3 _lastPosition;

    private void FixedUpdate()
    {
    }

    private bool _isPhysics = true;

    public override void Update()
    {
        base.Update();
        if (_isStart)
        {
            if (!IsGameStarted)
            {
                return;
            }


            if (_isControl && firstBalloonTransform.position.z >= _roadGenerator.lastSpawnPoint.z + 35)
            {
                _isControl = false;
                GameObject playerPoint = _roadGenerator.GenerateStair(transform.position);

                LeanTween.move(firstBalloonTransform.gameObject, playerPoint.transform.position, 3f)
                    .setEase(LeanTweenType.easeOutQuad).setOnComplete(
                        o =>
                        {
                            animator.SetTrigger("Finish");
                            Destroy(GetComponent<CharacterJoint>());
                            Destroy(GetComponent<Rigidbody>());
                            transform.rotation = Quaternion.Euler(-90, 0, 180);
                            ((LevelCont) (HyperLevelCont.Instance)).SetLevelScore(GetScore());
                            ((LevelCont) (HyperLevelCont.Instance)).EndLevel(2);
                        });
            }

            if (!_isControl)
            {
                return;
            }

            firstBalloonTransform.Translate(Vector3.forward * moveSpeed * Time.deltaTime, Space.World);

            if (Input.GetMouseButton(0))
            {
                if (Input.GetMouseButtonDown(0))
                {
                    _firstPosition = GetMousePosition();
                    _firstBalloonPosition = firstBalloonTransform.position;
                }
                //Vector3 lastPosition = new Vector3(-GetMousePosition().x, firstBalloonTransform.position.y,
                //firstBalloonTransform.position.z);

                _lastPosition = GetMousePosition();

                Vector3 direction = new Vector3(
                    strafeSpeed * (_firstBalloonPosition.x + _firstPosition.x - _lastPosition.x),
                    firstBalloonTransform.position.y,
                    firstBalloonTransform.position.z);

                direction.x = Mathf.Clamp(direction.x, _roadGenerator._firstRoadXPosition,
                    -_roadGenerator._firstRoadXPosition);

                firstBalloonTransform.position =
                    Vector3.MoveTowards(firstBalloonTransform.position, direction, 0.1f);
            }

            CheckCollision();
        }
    }

    private void CheckCollision()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1.0f);

        if (colliders.Length > 0 && _isPhysics)
        {
            GameObject collisionObject = colliders[0].gameObject;
            if (collisionObject.CompareTag("Balloon"))
            {
                Vibration.Vibrate();
                collisionObject.GetComponent<Balloon>().CollectBalloon(handTransform);
                _balloonHolder.AddBalloon(collisionObject);

                StartCoroutine(PhysicDisableAndEnable(3.0f));

                LeanTween.moveY(firstBalloonTransform.gameObject,
                        firstBalloonTransform.position.y +
                        _risingHeight, 2.5f)
                    .setEase(LeanTweenType.linear);
            }
            else if (collisionObject.CompareTag("Obstacle"))
            {
                foreach (Transform balloon in firstBalloonTransform)
                {
                    if (balloon != firstBalloonTransform)
                    {
                        Destroy(_characterJoint);
                        Destroy(balloon.gameObject);
                    }
                }

                if (Application.platform == RuntimePlatform.Android)
                {
                    Vibration.Vibrate(600);
                }
                else
                {
                    Vibration.VibratePeek();
                }

                ((LevelCont) (HyperLevelCont.Instance)).EndLevel(1);
            }
        }
    }

    private IEnumerator PhysicDisableAndEnable(float time)
    {
        _isPhysics = false;
        yield return new WaitForSeconds(time);
        _isPhysics = true;
    }

    Vector3 GetMousePosition()
    {
        Vector3 inputPosition = Input.mousePosition;
        inputPosition.z = _cameraZposition;
        Vector3 wordPoint = Camera.main.ScreenToWorldPoint(inputPosition);
        wordPoint.x = Mathf.Clamp(wordPoint.x, -_maxXPosition, _maxXPosition);
        return wordPoint;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(startPoint, 0.25f);
    }

    private void onTouch(RoketGame.Touch _touch)
    {
        Debug.Log(Camera.main.ViewportToWorldPoint(_touch.Points[0]) + " - " + _touch.Points[1]);
    }

    public int GetScore()
    {
        int score = (int) (firstBalloonTransform.position.y / _risingHeight) * 100;
        Debug.Log("Score : " + score);
        return score;
    }
}