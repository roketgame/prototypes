﻿using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;

public class CameraController : CoreCont<CameraController>
{
    public Transform target;

    public Vector3 offset;

    public override void StartGame()
    {
        base.StartGame();
    }

    public override void Update()
    {
        base.Update();
        if (IsGameStarted)
        {
            Vector3 lastPosition = target.position + offset;
            lastPosition.x = 0;
            transform.position = Vector3.Slerp(transform.position, lastPosition, 1 / Time.deltaTime);
        }
    }
}