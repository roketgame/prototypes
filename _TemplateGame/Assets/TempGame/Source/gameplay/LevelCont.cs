﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public struct LevelData
{
    public BallType Yellow;
    public BallType Red;
    public BallType Blue;

}

public class LevelCont : HyperLevelCont
{

    [SerializeField]  public LevelData Data;


    public LevelData GetLevelData()
    {
        LevelData data = Data;
        data.Blue.CatchCount = 2 + (CurrLevel * 2);
        if (CurrLevel > 2)  data.Yellow.CatchCount = 0 + CurrLevel;
        if (CurrLevel > 5)
        {
            data.Red.CatchCount = 0 + (CurrLevel * 2);
            data.Red.CatchCount += CurrLevel ;
        }


        data.Blue.SpawnTimeDelay = Mathf.Clamp(1 - (CurrLevel * 0.05f), 0.2f, 10);
        data.Yellow.SpawnTimeDelay = Mathf.Clamp(2 - (CurrLevel * 0.1f), 0.2f, 10);
        data.Red.SpawnTimeDelay = Mathf.Clamp(3 - (CurrLevel * 0.1f), 0.2f, 10);


        float forceBlue = Mathf.Clamp(0.2f + (CurrLevel * 0.01f), 0, 5);
        float forceYellow = Mathf.Clamp(0.2f + (CurrLevel * 0.02f), 0, 5);
        float forceRed = Mathf.Clamp(0.3f + (CurrLevel * 0.03f), 0, 5);

        data.Blue.StartForce = new Vector3(0, -forceBlue, 0);
        data.Yellow.StartForce = new Vector3(0, -forceYellow, 0);
        data.Red.StartForce = new Vector3(0, -forceRed, 0);

        return data;
    }

    public override int GetLevelResult()
    {
        return  ((GameCont) HyperGameCont.Instance).basket.GetGameResult() ? 2 : 1; ;
    }

    public override void EndLevel(int levelResult)
    {
        base.EndLevel(levelResult);

        if(levelResult == 2)
        {
            SetLevelScore(50);
        }
        


    }

}
