﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;
using Random = UnityEngine.Random;

public class Spawner : HyperSceneObj
        
{
    public GameObject PrefabItem;
    private bool isStartSpawn;
    private float timer = 0;

    public override void Init()
    {
        base.Init();
        isStartSpawn = true;
        enabled = true;
    }

    public override void StartGame()
    {
        base.StartGame();
        timer = 0;

        LevelData data  = ((LevelCont)HyperLevelCont.Instance).GetLevelData();

        StartCoroutine( createItem(Random.Range(0.1f,0.4f), data.Blue));
        StartCoroutine(createItem(Random.Range(0.2f, 0.6f), data.Yellow));
        StartCoroutine(createItem(Random.Range(0.2f, 1.2f), data.Red));


        //set box extends according to level
        Vector3 spawnArea = new Vector3(1, 1, 1);
        spawnArea.x = Mathf.Clamp(3 + (LevelCont.Instance.CurrLevel * 0.2f), 1, 12);
        spawnArea.z = Mathf.Clamp(2 + (LevelCont.Instance.CurrLevel * 0.1f), 1, 5);
        GetComponent<BoxCollider>().size = spawnArea;

    }


    public override void Update()
    {
        base.Update();

        if(isStartSpawn && PrefabItem)
        {


            
              
        }

        timer += Time.fixedDeltaTime;
    }

   private IEnumerator createItem(float startDelayTime, BallType ballType)
    {
        yield return new WaitForSeconds(startDelayTime);
        StartCoroutine(spawnItem(ballType.SpawnTimeDelay, ballType));

    }

    private IEnumerator spawnItem(float waitTime, BallType ballType)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);


            Vector3 pos = Utils.GetRandomPositionInBoxCollider(GetComponent<BoxCollider>());

            Ball item = CoreSceneCont.Instance.SpawnItem<Ball>(PrefabItem, pos, transform);
            //item.rb.AddForce(new Vector3(0, -0.5f, 0), ForceMode.Impulse);
            item.rb.AddForce(ballType.StartForce, ForceMode.Impulse);
            item.Type = ballType;
            item.GetComponentInChildren<MeshRenderer>().material.color = ballType.Color;
            item.GetComponentInChildren<Collider>().material = ballType.PhysicMat;
        }
    }
}
