﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Basket : HyperSceneObj
{
    private Dictionary<string, float> listWillCatch;

    public override void Init()
    {
        base.Init();

        CoreInputCont.Instance.EventTouch.AddListener(onTouchScene);
    }

    public override void StartGame()
    {
        base.StartGame();

        listWillCatch = new Dictionary<string, float>();
        LevelData data = ((LevelCont)HyperLevelCont.Instance).GetLevelData();
        listWillCatch.Add(data.Blue.Id, data.Blue.CatchCount);
        listWillCatch.Add(data.Yellow.Id, data.Yellow.CatchCount);
        listWillCatch.Add(data.Red.Id, data.Red.CatchCount);

        updateUI();

    }
    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);


        //if(GetStatus() == GameStatus.STARTGAME )
        //{
        //    Ball target = collision.collider.GetComponent<Ball>();
        //    if (target)
        //    {
        //        score++;
        //        CoreUiCont.Instance.GetPageByClass<PageGame>().SetScore(score);
        //        target.OnCatched();

        //        if(score == 10)
        //        {
        //            CoreUiCont.Instance.GetPageByClass<PageGame>().OpenPopup<Popup>();
        //        }
                
               
        //    }
        //}
       
    }

    private void onTouchScene(RoketGame.Touch touch)
    {
        if(GameStatus == GameStatus.STARTGAME)
        {
            if (touch.Phase == TouchPhase.Moved)
            {
                transform.position = touch.ScenePoints[1];
            }
        }
       
        
    }



    public override void OnContactChild(ContactInfo contact)
    {
        base.OnContactChild(contact);

        if(contact.Target.name == "catch_collider")
        {
            if(contact.Other.transform.parent)
            {
                Ball ball = contact.Other.transform.parent.GetComponent<Ball>();
                if (ball)
                {

                    if (contact.Type == ContactType.TRIGGER_ENTER)
                    {
                        onCatchBall(ball.Type.Id);

                    }
                    if (contact.Type == ContactType.TRIGGER_EXIT)
                    {
                        onDropBall(ball.Type.Id);
                    }

                }
            }
            
        }

       

        
    }

    private void onCatchBall(string id)
    {
        if (listWillCatch[id] > 0)
        {
            listWillCatch[id] -= 1;
        }
       
        updateUI();
        if (GetGameResult())
        {
            LevelCont.Instance.TryEndLevel();
        }
    }


    private void onDropBall(string id)
    {
        if (listWillCatch[id] > 0)
        {
            listWillCatch[id] += 1;
        }

        
    }
    private void updateUI()
    {
        ((HyperUICont)HyperUICont.Instance).GetPageByClass<PageGame>().UpdateWillCatch(listWillCatch);
    }

    public bool GetGameResult()
    {
        float total = 0;
        foreach(KeyValuePair<string, float> elem in listWillCatch)
        {
            total += elem.Value;
        }
        return (total == 0);
       
    }
}
