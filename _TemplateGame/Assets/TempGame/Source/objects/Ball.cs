﻿using JetBrains.Annotations;
using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

[Serializable]
public struct BallType
{
    public string Id;
    public Color Color;
    public PhysicMaterial PhysicMat;
    [HideInInspector] public float CatchCount;
    [HideInInspector] public float SpawnTimeDelay;
    [HideInInspector] public Vector3 StartForce;
}

public class Ball : HyperSceneObj
{
    public BallType Type;
    //public string Id;
    public override void Init()
    {

        base.Init();
       
    }



    public override void StartGame()
    {
        base.StartGame();
       
    }


    public void FixedUpdate()
    {

    }

    public override void Update()
    {

        if (GameStatus == GameStatus.INIT) return;
        base.Update();

    }









}