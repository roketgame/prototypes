﻿using PathCreation;
using UnityEngine;
using UnityEngine.Events;

namespace ColorRoad.ThirdParty.PathCreator.Examples.Scripts
{
    // Moves along a path at constant speed.
    // Depending on the end of path instruction, will either loop, reverse, or stop at the end of the path.
    public class PathFollower : MonoBehaviour
    {
        public PathCreation.PathCreator pathCreator;
        public EndOfPathInstruction endOfPathInstruction;
        public float speed = 5;

        public UnityEvent finishRoad = new UnityEvent();
        private bool _isFinishRoad = false;

        public float height = 0.5f;
        float distanceTravelled;

        void Start()
        {
            if (pathCreator != null)
            {
                // Subscribed to the pathUpdated event so that we're notified if the path changes during the game
                pathCreator.pathUpdated += OnPathChanged;
            }
        }

        void Update()
        {
            if (pathCreator != null)
            {
                distanceTravelled += speed * Time.deltaTime;

                if (!_isFinishRoad)
                {
                    if (transform.position ==
                        pathCreator.path.GetPoint(pathCreator.path.NumPoints - 1) + Vector3.up * height)
                    {
                        _isFinishRoad = true;
                        finishRoad.Invoke();
                    }
                }
                

                transform.position = pathCreator.path.GetPointAtDistance(distanceTravelled, endOfPathInstruction) +
                                     Vector3.up * height;
                transform.rotation = pathCreator.path.GetRotationAtDistance(distanceTravelled, endOfPathInstruction) *
                    Quaternion.Euler(0,0,90);
            }
        }

        // If the path changes during the game, update the distance travelled so that the follower's position on the new path
        // is as close as possible to its position on the old path
        void OnPathChanged()
        {
            distanceTravelled = pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }
    }
}