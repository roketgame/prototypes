﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class LevelData
{
    public GameObject targetObject;
    public GameObject Path;
    // public GameObject hologramObject;
    public Texture2D colorPalette;
    public Sprite ShapeSprite;
    public float totalTime;
    
}

public class LevelCont : HyperLevelCont
{
    public float staticAddLevelScore = 100;
    public float multiplyScorePerLevel = 50;//static eklenen score disinda her levelda eklenecek extra score carpani
    public List<LevelData> LevelDatas;
    
    public LevelData GetLevelData()
    {
        LevelData data = new LevelData();
        if (LevelDatas.Count > 0)
        {
            if (CurrLevel - 1 < LevelDatas.Count)
                data = LevelDatas[CurrLevel - 1];
            else
                data = null;
        }
        else
        {
            data = null;
        }

        return data;
    }

    public override void EndLevel(int levelResult)
    {
         if(levelResult == 2)
        {
           SetLevelScore(staticAddLevelScore + (CurrLevel * multiplyScorePerLevel));
        }

        
        base.EndLevel(levelResult);

       
    }




    public override int GetLevelResult()
    {
        return ((GameController) HyperGameCont.Instance).playerController.GetLevelResult();
    }

    public int GetLevelIndex()
    {
        int indexOfArr = LevelDatas.Count - 1;

        if (CurrLevel - 1 < LevelDatas.Count)
            indexOfArr = CurrLevel - 1;

        return indexOfArr;
    }


    public PathBoxCreator GetPathBoxCreator()
    {
        return GetLevelData().Path.GetComponent<PathBoxCreator>();
    }

    public PathCreation.PathCreator GetPathCreator()
    {
        return GetLevelData().Path.GetComponent<PathCreation.PathCreator>();
    }
}