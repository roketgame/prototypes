﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ColorRoad.Source.controllers;
using RoketGame;
using UnityEngine;
using UnityEngine.UI;

public class GameController : HyperGameCont
{
    // TODO: HyperGameCont (GameController) Cast - isstart game - hypersceneobject

    public ColorController colorController;
    public PlayerController playerController;
    // public CameraController cameraController;
    public CubeController cubeController;
    public bool Debug_CheckColorAlwaysTrue = false;
    // public PathBoxCreator pathBoxCreator;
        public float SuccessProgressRatio = 0.8f; //oyunu basarili tamamalamak icin beklenen min progress orani 
        public float VıbrationInterval = 0.5f; //second
    public override void Init()
    {
        base.Init();
        Application.targetFrameRate = 60;
    }

    public override void StartGame()
    {
        base.StartGame();
        //EnableControl();
    }

    public override void Update()
    {
        base.Update();

        if(RoketGame.Config.Instance.DebugMode > 0)
        {


        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.UpArrow))

             HyperLevelCont.Instance.NextLevel();

        }
    }

    public void DisableControl()
    {
        colorController.enabled = false;
        //playerController.enabled = false;
        playerController.StopAllCoroutines();
        //cameraController.enabled = false;
    }

    public void EnableControl()
    {
        colorController.enabled = true;
        playerController.enabled = true;
        //cameraController.enabled = true;
    }

    public override void GameOver(int result)
    {
        // TODO: RESULTU YÜZDELİK GİBİ GÖNDER KAÇ KÜP OLUŞTURULMUŞ ONA GÖRE BASE'YE 0 - FAILED 1 - SUCCESS OLARAK SÖYLE
        base.GameOver(result);
        
        //DisableControl();
    }
}