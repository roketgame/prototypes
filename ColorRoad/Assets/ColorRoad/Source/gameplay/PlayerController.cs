﻿using System;
using System.Collections;
using System.Collections.Generic;
using ColorRoad.ThirdParty.PathCreator.Examples.Scripts;
using RoketGame;
using UnityEngine;

public enum HoleType
{
    LADDER,
    HOLE,
    GRAVITY
}

public class PlayerController : HyperPlayerCont
{
    [Header("References")] public Transform mainCamera;

    public PathFollower pathFollower;

    public ColorController colorController;
    private Color _currentColor;

    [Header("Hole")] public HoleType holeType;

    // [Header("Game")] public float totalTime;
    private Hole currentHole;
    [HideInInspector] public float refreshDistance;
    [HideInInspector] public float holeRefresh;


    private float _currentDistance;
    private Vector3 _lastPosition;
    private Vector3 _currentPosition;

    private float _acceleration;

    [Header("Particle")] public ParticleController particleController;
    private int cubeCount;
    private int successCubeCount;
    private float vibrateIntervalCounter = 0;


    [Header("Confetti")] public List<ParticleSystem> confettis;

    private void Awake()
    {
        pathFollower.enabled = false;
      
    }

    private void Start()
    {
        vibrateIntervalCounter = 0;
          pathFollower.pathCreator = ((LevelCont) LevelCont.Instance).GetPathCreator();
        pathFollower.finishRoad.AddListener(() => ((LevelCont) HyperLevelCont.Instance).TryEndLevel());
        particleController.target = ((GameController) HyperGameCont.Instance).cubeController.GetCurrentTarget();

        foreach (var confetti in confettis)
        {
            confetti.Stop();
        }

        InvokeRepeating(nameof(CheckColor), 0.25f, refreshDistance);

        _scoreColor = colorController.currentUIColor;
    }

    public float GetProgress()
    {
        float result =  ((float) successCubeCount /        (float)   ((GameController) GameController.Instance).cubeController.totalCubeCount) ;
        // Debug.Log("result " + result);
        return Mathf.Clamp(result,0,1);
    }

    public int GetLevelResult()
    {
        return GetProgress() >  ((GameController)GameController.Instance).SuccessProgressRatio  ? 2 : 1;
    }

    public override void Update()
    {
        base.Update();

        if (!((GameController) HyperGameCont.Instance).IsStartedGame)
        {
            return;
        }

        particleController.StartParticle();

        if (!pathFollower.enabled)
        {
            pathFollower.enabled = true;
        }

        Debug.DrawLine(transform.position, transform.position + Vector3.down * 3, Color.blue);
        if (!Physics.Raycast(transform.position, Vector3.down, out var hit))
        {
            // Debug.Log("Hit not found");
            return;
        }

        Renderer rend = hit.transform.GetComponent<Renderer>();

        currentHole = hit.transform.parent.GetChild(1).GetComponent<Hole>();
        _currentColor = rend.material.color;

        if (!firstScoreCheck)
        {
            firstScoreCheck = true;
            _scoreColor = _currentColor;
        }

        if (_scoreColor != _currentColor)
        {
            StartCoroutine(ScoreCheck());
        }

        _scoreColor = _currentColor;


        particleController.SetColor(_currentColor);


      
    }


    private void LadderHole()
    {
        if(currentHole)
        {
            LeanTween.moveLocalY(currentHole.parent.gameObject, currentHole.parent.localPosition.y + -1, 0.5f)
            .setEase(LeanTweenType.easeSpring);
        }
      
    }

    private void Hole()
    {
        if(currentHole) currentHole.gameObject.SetActive(true);
    }

    private void GravityHole()
    {
        if(currentHole) currentHole.ApplyGravity();
    }

    public Color _scoreColor;
    public bool checkingScore;
    public bool firstScoreCheck;


    void CheckColor()
    {
        if (colorController.currentUIColor == _currentColor || ((GameController) GameController.Instance).Debug_CheckColorAlwaysTrue)
        {
            ((GameController) HyperGameCont.Instance).cubeController.AddCube(_currentColor);
            cubeCount++;
            successCubeCount++;
            particleController.target = ((GameController) HyperGameCont.Instance).cubeController.GetCurrentTarget();
            particleController.SetSuccess(true);

            if (!firstScoreCheck)
            {
                firstScoreCheck = true;
            }

            if(currentHole) currentHole.SetColor(_currentColor);

            switch (holeType)
            {
                case HoleType.LADDER:
                    LadderHole();
                    break;
                case HoleType.HOLE:
                    Hole();
                    break;
                case HoleType.GRAVITY:
                    GravityHole();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

           

                vibrateIntervalCounter += Time.deltaTime;

                if(vibrateIntervalCounter > ((GameController) GameController.Instance).VıbrationInterval)
                {
                        if (Application.platform == RuntimePlatform.Android)
                            MobileVibration.VibrateCustomAndroid(5, true);
                
                        if(Application.platform == RuntimePlatform.IPhonePlayer)
                            MobileVibration.Vibrate(MobileVibration.Level.LOW, true);

                            vibrateIntervalCounter = 0;
                }
        }
        else
        {
            particleController.SetSuccess(false);
            ((GameController) HyperGameCont.Instance).cubeController.AddEmptyCube();
            cubeCount++;
            ((GameController) HyperGameCont.Instance).cubeController.GetCurrentTarget();
        }
           
    }

    public float timer = 0.0f;

    public Color testColor;

    private IEnumerator ScoreCheck()
    {
        yield return new WaitForSeconds(0.1f);

        bool isPerfect = false;
        float perfectTime = 0;

        while (timer <= 1.0f)
        {
            testColor = colorController.currentUIColor;
            timer += Time.deltaTime;
            if (_scoreColor == colorController.currentUIColor)
            {
                Debug.DebugBreak();
                isPerfect = true;
                perfectTime = timer;
                timer = 10;
            }

            yield return null;
        }

        if (isPerfect)
        {

            if (perfectTime < 0.5f)
            {
                float width = Screen.width;
                float height = Screen.height;
                LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromScreenPoint("Screen", 1, "Amazing",
                    new Vector2(width / 2, height / 2), new Vector2(width / 2 + 100, height / 2 + 100), 0.5f);
            }
            else
            {
                float width = Screen.width;
                float height = Screen.height;
                LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromScreenPoint("Screen", 2, "Perfect",
                    new Vector2(width / 2, height / 2), new Vector2(width / 2 + 100, height / 2 + 100), 0.5f);
            }
        }
        else
        {
            // Debug.Log("Not Perfect : " + perfectTime);
        }


        timer = 0;
        checkingScore = false;
    }


    public override void GameOver(int result)
    {
        base.GameOver(result);

        CancelInvoke(nameof(CheckColor));

            bool isSuccess =  true;//(result == 2);
           

            FinishAnimation(isSuccess);

        // LeanTween.rotateLocal(mainCamera.gameObject,
            // new Vector3(45, -87, 15), 2.5f).setOnComplete(o => { StartCoroutine(FinishCameraRotation()); });
    }

    // private IEnumerator FinishCameraRotation()
    // {
    //     while (true)
    //     {
    //         mainCamera.transform.RotateAround(Vector3.zero, Vector3.up, 50 * Time.deltaTime);
    //         yield return null;
    //     }
    // }

    private void FinishAnimation(bool isSuccess)
    {
        if (isSuccess)
        {
            foreach (var confetti in confettis)
            {
                confetti.Play();
            }
        }
    }
}