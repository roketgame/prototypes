﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGame : HyperPageGame
{
    public GameObject slider;
    public ProgressShape ProgressShape;

    public override void Init()
    {
        base.Init();
        slider.SetActive(false);

    }

    public override void Update()
    {
        base.Update();
    }

    public override void StartGame()
    {
        base.StartGame();
        slider.SetActive(true);
    }

    protected override void onBtnStartClicked(TouchUI _touch)
    {
        base.onBtnStartClicked(_touch);
    }

    public void UpdateWillCatch(Dictionary<string, float> listWillCatch)
    {
        foreach (KeyValuePair<string, float> elem in listWillCatch)
        {
            SetText(elem.Key, elem.Value.ToString());
        }
    }
}