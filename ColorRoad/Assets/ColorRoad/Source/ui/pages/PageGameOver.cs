﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGameOver : HyperPageGameOver
{
    public override void Init()
    {
        base.Init();

        
    }


    public override void SetGameResult(int _result)
    {
        base.SetGameResult(_result);

        ProgressShape progress = GetComponentInChildren<ProgressShape>();
        progress.UpdateSprite();

       
        if (_result == 1)
            SetText("TxtResult", "YOU FAILED");
        if (_result == 2)
            SetText("TxtResult", "LEVEL COMPLETED!");

        SetText("TxTLevel", "Level "+ LevelCont.Instance.CurrLevel.ToString());

        BtnRestart.gameObject.active = false;
        BtnNextLevel.gameObject.active = false;


        StartCoroutine(enabledButtons(1.0f));

       
    }

    // every 2 seconds perform the print()
    private IEnumerator enabledButtons(float waitTime)
    {
        
            yield return new WaitForSeconds(waitTime);
            BtnRestart.gameObject.active = true;
        BtnNextLevel.gameObject.active = true;
        
    }






}
