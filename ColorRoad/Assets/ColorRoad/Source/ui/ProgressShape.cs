﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.UI;
using TMPro;

public class ProgressShape : CoreUI
{
    private TextMeshProUGUI text;
    public RectTransform ProgressBar;
    public Image ShapeImage ;
    public override void Init()
    {
        base.Init();

        text = GetComponentInChildren<TextMeshProUGUI>();
        
        SetProgress(0,false);



    }

    public  void UpdateSprite()
    {

            if(ShapeImage)
            {
                ShapeImage.sprite = ((LevelCont) LevelCont.Instance).GetLevelData().ShapeSprite;
            }
    }

    public override void Update()
    {
        base.Update();

          SetProgress(  ((PlayerController) PlayerController.Instance).GetProgress());

    }

    public void SetProgress(float _ratio, bool _useTween=true)
    {

        if(text)
        {
            int percent = (int)(_ratio * 100);
             text.SetText("% "+percent.ToString());

        }

        if(ProgressBar)
        {
            if(_useTween)
                ProgressBar.transform.LeanScaleY(_ratio, 0.5f).setEase(LeanTweenType.easeOutBack);
            else
            {
                Vector3 scale = ProgressBar.transform.localScale ;
                scale.y = _ratio;
                 ProgressBar.transform.localScale = scale;
            }
           
        }
       


    }

}
