﻿using System.Collections;
using System.Collections.Generic;
using ColorRoad.Source.utils;
using RoketGame;
using UnityEngine;
using UnityEngine.UI;

public class LevelEditor : HyperSceneObj
{
    public Transform targetObjectParent;
    public Transform targetObjectHologramParent;
    public Material hologramMaterial;

    public override void Init()
    {
        base.Init();


        Debug.Log("LevelEditor");

        LevelData levelData = ((LevelCont) HyperLevelCont.Instance).GetLevelData();

        if (levelData == null)
        {
            Debug.LogError("Level-" + ((LevelCont) HyperLevelCont.Instance).CurrLevel + " not found");
            CoreUiCont.Instance.GetPageByClass<PageGame>().BtnStart.EventTouch.RemoveAllListeners();
            return;
        }

        GameObject createdTargetObject =          Instantiate(levelData.targetObject, Vector3.up * 50, Quaternion.identity, targetObjectParent).transform.GetChild(0).gameObject;
        // Instantiate(levelData.hologramObject, Vector3.up * 50, Quaternion.identity, targetObjectParent).GetComponentInChildren<Renderer>().material = hologramMaterial;
        // colorPalette.sprite = TextureUtil.CreateSprite(levelData.colorPalette);


        ((GameController)GameController.Instance).colorController.paletteReference = levelData.colorPalette;

        (GameController.Instance as GameController)?.cubeController.CreateCubes(createdTargetObject);
        (GameController.Instance as GameController)?.colorController.CreateColors();
        // (GameController.Instance as GameController)?.pathBoxCreator.CreateRoad();
        ((LevelCont) LevelCont.Instance).GetPathBoxCreator().CreateRoad();
    }
}