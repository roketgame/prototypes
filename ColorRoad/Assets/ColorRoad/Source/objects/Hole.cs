﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : MonoBehaviour
{
    public Transform parent;
    public Material holeMaterial;
    private Material _holeInstantiateMaterial;

    private Color _color;

    public Renderer holeRenderer;


    public void SetColor(Color color)
    {
        _color = color;

        Material material = new Material(holeMaterial);
        _holeInstantiateMaterial = material;
        _holeInstantiateMaterial.SetColor("_Color", _color);
        holeRenderer.material = _holeInstantiateMaterial;
    }

    public void SetScale(float scale)
    {
        holeRenderer.GetComponent<Transform>().localScale = Vector3.one * scale;
    }

    public void ApplyGravity()
    {
        LeanTween.moveLocalY(parent.gameObject, -10, 1.5f).setOnComplete(o =>
        {
            LeanTween.scale(parent.gameObject, Vector3.zero, 0.5f).setEase(LeanTweenType.easeSpring)
                .setOnComplete(() => { });
        });
    }
}