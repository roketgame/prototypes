﻿using PathCreation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using static PathCreation.VertexPath;

public class PathBoxCreator : HyperSceneObj
{
    public Color[] colors; //bunu test olarak ekledim. sen bu liste yerinde ekledigimiz paletten rengi secmelisin.
    public GameObject PlatformPrefab;
    public float PlatformWidth;
    public float PlatformHeight;
    public PathCreator Path;
    public Transform pathHolder;
    private float totalLength; //tum path uzunlugu;

    public void CreateRoad()
    {
        colors = (GameController.Instance as GameController)?.colorController.GetColors();

        if (PlatformPrefab)
        {
            totalLength = 0;
            {
                Vector3 prev = Path.path.localPoints[0];
                Vector3 next;
                for (int i = 0; i < Path.path.localPoints.Length; i++)
                {
                    next = Path.path.localPoints[i];
                    totalLength += (next - prev).magnitude;
                    prev = next;
                }
            }

            float w = PlatformWidth;
            float h = PlatformHeight;
            int count = Mathf.FloorToInt(totalLength / h);
            int colorLen =
                Mathf.FloorToInt(count / colors.Length); //1 rengin toplam kac box itemdan olusacagi sayisi
            HyperSceneObj platform;
            for (int i = 0; i < count; i++)
            {
                platform = SceneCont.Instance.SpawnItem<HyperSceneObj>(PlatformPrefab, Vector3.zero, pathHolder,
                    transform);
                platform.transform.GetChild(1).gameObject.SetActive(false);
                float ratioH = (float) i / (float) count;
                updatePlatformMove(platform, ratioH);
                platform.transform.localScale = new Vector3(w, platform.transform.localScale.y, h);

                {
                    //colorLen bilindigine gore rengi belirliyoruz 
                    int colorIndex = Mathf.Clamp(Mathf.FloorToInt(i / colorLen), 0, colors.Length - 1);
                    Color color = colors[colorIndex];
                    platform.GetComponentInChildren<Renderer>().material.color = color;
                }
            }
        }
    }

    private void updatePlatformMove(HyperSceneObj _platform, float _index)
    {
        Vector3 loc = Path.path.GetPointAtTime(_index, EndOfPathInstruction.Loop);
        Vector3 dir = Path.path.GetDirection(_index, EndOfPathInstruction.Loop);
        Quaternion qua = Path.path.GetRotation(_index, EndOfPathInstruction.Loop);
        Vector3 rot = qua.eulerAngles;
        rot.z += 90;

        _platform.transform.position = loc;
        _platform.transform.rotation = Quaternion.Euler(rot);
    }
}