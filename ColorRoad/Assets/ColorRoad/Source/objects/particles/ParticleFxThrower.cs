﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ParticleUnit
{
    public Vector3 SpawnPosition;
    public bool Enabled;
}

public class ParticleFxThrower : ParticleFx
{
    private             ParticleSystem.Particle[] _listParticles = new ParticleSystem.Particle[1000];
    private           ParticleUnit[] _listUnits = new ParticleUnit[1000];
    public float Force = 80f;
    public override void Init()
    {
        base.Init();
        for (int i = 0; i < 1000; i++)
        {
            _listUnits[i] = new ParticleUnit();
        }

    }

    private Vector3 getRandTargetPos(Vector3 _target, float offset)
    {
        float x = UnityEngine.Random.Range(_target.x - offset, _target.x + offset);
        float y = UnityEngine.Random.Range(_target.y - offset, _target.y + offset);
        float z = UnityEngine.Random.Range(_target.z - offset, _target.z + offset);
        return new Vector3(x, y, z);
    }

    public float ShaderOutlineOffset = 0.01f; //shader outline offset degeri
    public float OutlineShaderDarkColorMin= 0.6f; //random belirlenen shader outline renginin dark range min degeri
    public float OutlineShaderDarkColorMax= 0.9f; //random belirlenen shader outline renginin dark range max degeri
    public override void UpdateParticles(Vector3 _target, Color _currentColor, bool _isMatchedColor)
    {
        
        base.UpdateParticles(_target, _currentColor, _isMatchedColor);

        Color dark = _currentColor;
        Particle.GetComponent<Renderer>().material.SetColor("_OutlineColor", makeDark(_currentColor,Random.Range(OutlineShaderDarkColorMin, OutlineShaderDarkColorMax)));
        Particle.GetComponent<Renderer>().material.SetFloat("_Outline", ShaderOutlineOffset);

        Vector3 forcedir = ( getRandTargetPos(_target,20) - this.transform.position ).normalized;
        ForceOverLifetimeModule forceoverlife = Particle.forceOverLifetime;
        forceoverlife.enabled = true;
        forceoverlife.space = ParticleSystemSimulationSpace.World;
        forceoverlife.x = forcedir.x * Force;
        forceoverlife.z = forcedir.z * Force;
        forceoverlife.y = forcedir.y * Force / 2;
         Debug.DrawLine(this.transform.position, forcedir* 10, Color.cyan);
        return;


             Particle.GetParticles(_listParticles);
            int _count = _listParticles.Length;
            for (int i = 0; i < _count; i++)
            {
                Particle p = _listParticles[i];
                Vector3 v1 = Particle.transform.TransformPoint(p.position);
                Vector3 v2 = _target;
                if (p.remainingLifetime < p.startLifetime)
                {

                    if(p.remainingLifetime  -  p.startLifetime < 0.1f)
                      _listUnits[i].Enabled = false;

                    if(!_listUnits[i].Enabled)
                    {
                        _listUnits[i].Enabled = true;
                        _listUnits[i].SpawnPosition =  v1;
                    }

                    v1 = _listUnits[i].SpawnPosition;
                    Vector3 tarPosi = (v2 - v1) * (p.remainingLifetime / p.startLifetime);
                 
                    // tarPosi.y = p.position.y;
                    // p.velocity = (p.position - _target).normalized * 200;
                    // p.position = Particle.transform.InverseTransformPoint(v2 - tarPosi);
                    p.position = Particle.transform.InverseTransformPoint(v2 - tarPosi);
                    
                    if(tarPosi.magnitude == 0)
                    {
                          _listUnits[i].Enabled = false;
                    }

                    //  Debug.Log("vel " + p.velocity);
                    // Vector3 rot = Vector3.zero;
                    //  rot.y += Mathf.Sin(Time.time) * 2;// Time.deltaTime;
                    //  p.rotation3D = rot;

                    _listParticles[i] = p;
                }
            }
            Particle.SetParticles(_listParticles, _count);


        

            
       
   

    }
}
