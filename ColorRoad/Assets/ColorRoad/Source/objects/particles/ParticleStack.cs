﻿using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;




public class ParticleStack : ParticleCore
{


    public override void Update()
    {
        base.Update();

       
    }
private int counter;
     protected override HyperSceneObj createItem(Transform _parent)
    {
        HyperSceneObj item=  base.createItem(_parent);
        Vector3 pos = item.transform.position;
        int xCount = 3;
        int yCount = 3;
        int zCount = 2;
        pos.x -= (counter % xCount ) * 1f;
        // pos.z += ( Mathf.Floor( counter / yCount )) * 1f;
        // pos.y += ( Mathf.Floor( counter / yCount )) * 1f;
        item.transform.position = pos;
       
        //   Vector3 _target = targetOfGeneratedMesh;
            // _target.y = targetOfGeneratedMesh.y;
            //  Vector3 force = (_target - item.transform.position ).normalized;
             Vector3 force = Vector3.zero;
             force.x = Random.Range(-1,1);
             force.y = Random.Range(1,3);
             
            //  force.y += Random.Range(0.1f,0.5f);
             
            //   item.GetComponent<BoxCollider>().isTrigger = true;
            //   item.rb.useGravity = false;
            //   item.rb.isKinematic = true;
            //   item.transform.parent = PrefabParent;
            item.rb.AddForce(force.normalized *0.2f , ForceMode.Force);
            // item.rb.angularVelocity = new Vector3(90,90,90);

            counter++;

            if(counter >= 3*3)
            counter = 0;

            //  StartCoroutine(enablePhysicItem( 0.2f, item));
              return item;
    }

     private IEnumerator enablePhysicItem( float delay, HyperSceneObj item)
    {
        yield return new WaitForSeconds(delay);
         item.GetComponent<BoxCollider>().isTrigger = false;
              item.rb.useGravity = true;
               item.rb.isKinematic = false;

    }

    public override void SetColorMatchStatus(bool _status)
    {

        IsEnable = _status;

    //     if(_status)
    //     {
    //         MinInterval = 0.1f;
    //         MaxInterval = 0.2f;
    //     }
    //     else{
    //          MinInterval = 2f;
    //         MaxInterval = 3f;
    //     }
    }

}
