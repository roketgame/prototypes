﻿using System.Collections;
using System.Collections.Generic;
using RoketGame;
using UnityEngine;

/* 
ParticleCore : unity'nin particlesytem'i karistirilmamali!
Bu tamamen custom, cube meshlerle calisan bir fizik
*/

public class ParticleCore : HyperSceneObj
{
   public bool  IsEnable;
   public GameObject Prefab;
   public GameObject SpawnPoint;
   public bool IsRandomPosInSpawnPoint = true;
   public Transform SpawnParent;
    public float MinInterval = 0.1f;
    public float MaxInterval = 0.2f;

    public float LifeTime = 2;
    public int CountOfPerInterval = 2; //her interval de kac tane spawn edilecek
    public float MinScale = 1;
    public float MaxScale = 1;
    private float timeCounter;


    public override void Update()
    {
        base.Update();
float interval = Random.Range(MinInterval,MaxInterval);
         if(timeCounter > interval && Prefab && IsEnable)
        {
            timeCounter = 0;
            
            for (int i = 0; i < CountOfPerInterval; i++)
            {
                 float scale = Random.Range(0.3f,0.6f);
                HyperSceneObj item =   createItem(SpawnParent);
            }

          
        }

        timeCounter += Time.deltaTime;

    }

    public virtual void SetColorMatchStatus(bool _status)
    {

    }

    protected virtual HyperSceneObj createItem(Transform _parent)
    {

        Vector3 pos = SpawnPoint.transform.position;

        if(IsRandomPosInSpawnPoint)
        pos = Utils.GetRandomPositionInBoxCollider( SpawnPoint.GetComponent<BoxCollider>());
        CubeStack item = CoreSceneCont.Instance.SpawnItem<CubeStack>(Prefab, pos, _parent, true);
        float _scale = Random.Range(MinScale,MaxScale);
        item.transform.localScale = new Vector3(_scale,_scale,_scale);
         item.transform.localEulerAngles = Vector3.zero;
        item.StartGame();
            StartCoroutine(killItem( LifeTime, item));

            return item;
    }

    
   private IEnumerator killItem( float delay, HyperSceneObj item)
    {
        yield return new WaitForSeconds(delay);
        CoreSceneCont.Instance.DeSpawnItem(item);

    }
}


/*
playerin oldugu noktada cube'ler olusturup, force ile target'a firlatilir
*/
public class ParticleThrower : ParticleCore
{
    // public GameObject SpawnPoint;
    public float MinForceMagnitue = 1f;
    public float MaxForceMagnitue = 2f;

    public override void Update()
    {
        base.Update();

       
    }


     protected override HyperSceneObj createItem(Transform _parent)
    {
        HyperSceneObj item=  base.createItem( _parent);
        Vector3 rand = Utils.GetRandomPositionInVector(new Vector3[]{ new Vector3(-2,-2,-2), new Vector3(2,2,2) });
        Vector3 startPos = SpawnPoint.transform.position + rand;
        item.transform.position =  startPos;
        // item.transform.LookAt(Vector3.zero);
        Vector3 targetOfGeneratedMesh =  ((GameController) HyperGameCont.Instance).cubeController.GetCurrentTarget();
          Vector3 _target = targetOfGeneratedMesh;
          _target = Vector3.zero;
        //   _target.x = Random.Range(-2,2);
        //   _target.z = Random.Range(-2,2);
            _target.y = targetOfGeneratedMesh.y;
             Vector3 force = (_target -item.transform.position ).normalized;
            //  Vector3 force = item.transform.forward;
            //  force.y += Random.Range(0.1f,0.5f);
             
              item.GetComponent<BoxCollider>().isTrigger = true;
              item.rb.useGravity = false;
            float forceMagn = Random.Range(MinForceMagnitue,MaxForceMagnitue);
            item.rb.AddForce(force *forceMagn , ForceMode.Force);
            // item.rb.AddForce(new Vector3(0,1,0) , ForceMode.Force);
            // item.rb.angularVelocity = new Vector3(10,0,0);

            // float scaleTo = item.transform.localScale.y * 0.4f;
            // item.transform.LeanScaleY(scaleTo, 0.4f).setEase(LeanTweenType.easeOutSine);
              return item;
    }

 

}
