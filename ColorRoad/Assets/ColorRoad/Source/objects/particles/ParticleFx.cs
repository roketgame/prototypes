﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class ParticleFx : HyperSceneObj
{
    public float EmissionRateOnFail = 1; //color match false ise emission sayisi
    public float EmissionRateOnSucc = 1; //color match true ise emission sayisi
        public float ShaderDarkColorMin = 0.8f; //random belirlenen shader main renginin dark range min degeri
    public float ShaderDarkColorMax = 1f; //random belirlenen shader main renginin dark range max degeri
    public ParticleSystem Particle { get =>  GetComponent<ParticleSystem>();  }
     public bool ColorMatchStatus;
        private float t = 0f;
        private float currEmissionRate = 0f;
    public override void Init()
    {
        // Particle = GetComponent<ParticleSystem>();
        base.Init();
    }


    
    public virtual void UpdateParticles(Vector3 _target, Color _currentColor, bool _isMatchedColor)
    {
        if (!Particle.isPlaying) return;

            
            Particle.GetComponent<Renderer>().material.SetColor("_Color", makeDark(_currentColor, Random.Range(ShaderDarkColorMin, ShaderDarkColorMax)));
            // Particle.GetComponent<Renderer>().material.SetColor("_Color", _currentColor);
            EmissionModule emission = Particle.emission;
            float newEmissionRate = _isMatchedColor ? EmissionRateOnSucc : EmissionRateOnFail;
             currEmissionRate= (float)Mathf.Lerp(currEmissionRate , newEmissionRate, t);
             emission.rateOverTime = currEmissionRate;
            t += 0.05f * Time.deltaTime;
       
        if (t > 1.0f)  t = 0.0f;
       
        
       



        
    }

        protected Color makeDark( Color _color, float darkValue)
    {
        Color dark = _color;
        dark.b *= darkValue;
        dark.g *= darkValue;
        dark.r *= darkValue;
        return dark;
    }

}
