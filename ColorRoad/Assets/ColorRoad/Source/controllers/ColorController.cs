﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ColorRoad.Source.utils;
using RoketGame;
using UnityEngine;
using UnityEngine.UI;

public class ColorController : HyperSceneObj
{
    [Header("Material & Image")] public Image gradientImage;
    // public Material roadMaterial;
    // public Material roadHoleMaterial;
    // public Material roadSideMaterial;
    public Color currentUIColor;
     public Material playerMaterial;

    [HideInInspector] public Texture2D paletteReference;

    [Header("UI")] public RectTransform slider;
    public RectTransform toggle;

    [Header("Game")] public Vector3 _firstPosition;
    public Vector3 _lastPosition;
    public float _firstValue;
    public float speed = 80;
    public float _value;
    public float _maxWidth;

    private Color[] _colors;
    public Color[] GetColors()
    {
        return _colors;
    }

    // Start is called before the first frame update
    public void CreateColors()
    {
        // Generate Colors
        Color[] colors = TextureUtil.GetColorsFromTexture(paletteReference);
        colors = colors.ToList().OrderBy(x => Guid.NewGuid()).ToArray();
        List<Color> _takenColors = colors.ToList().Take(9).ToList();

        Color roadSideColor = _takenColors[_takenColors.Count - 1];

        _takenColors.RemoveAt(_takenColors.Count - 1);

        Color[] takenColors = _takenColors.ToArray();


        List<Color> roadColorList = new List<Color>();

        roadColorList.AddRange(takenColors.ToList());
        roadColorList.AddRange(takenColors.ToList());

        Color[] roadColors = roadColorList.ToArray();

        _colors = roadColors;

        Color[] uiColors = takenColors.ToList().OrderBy(x => Guid.NewGuid()).ToArray();

        //colors.Shuffle();

        // Create TextureProperties
        TextureProperties roadTextureProperties =
            new TextureProperties(TextureWrapMode.Clamp, FilterMode.Point, false, false);

        TextureProperties uiTextureProperties =
            new TextureProperties(TextureWrapMode.Clamp, FilterMode.Point, false, false);


        // Create Texture for Material
        Texture2D materialTexture =
            TextureUtil.GenerateTexture(1, roadColors.Length, roadColors, roadTextureProperties);


        Texture2D materialRoadSide = TextureUtil.GenerateTexture(1, 1, new[] {roadSideColor}, roadTextureProperties);

        // roadMaterial.mainTexture = materialTexture;
        // roadHoleMaterial.mainTexture = materialTexture;
        // roadSideMaterial.mainTexture = materialRoadSide;


        // Shuffle Color List


        // Create Texture for UI Image
        Texture2D uiTexture = TextureUtil.GenerateTexture(uiColors.Length, 1, uiColors, uiTextureProperties);

        gradientImage.sprite = TextureUtil.CreateSprite(uiTexture);


        _maxWidth = slider.sizeDelta.x - toggle.sizeDelta.x;
        toggle.anchoredPosition = new Vector2(_maxWidth / 2, 0);


        _resolutionMultiple = 100;
    }

    private List<Vector3> positions = new List<Vector3>();

    private float _resolutionMultiple;

    public override void Update()
    {
        base.Update();

        if (!HyperGameCont.Instance.IsStartedGame)
        {
            return;
        }

        if (Input.GetMouseButton(0))
        {
            if (Input.GetMouseButtonDown(0))
            {
                _firstPosition = Camera.main.ScreenToViewportPoint(InputPosition());
                _firstValue = toggle.anchoredPosition.x;
            }

            _lastPosition = Camera.main.ScreenToViewportPoint(InputPosition());

            _value = (((_lastPosition.x - _firstPosition.x) * speed) * Screen.dpi * _resolutionMultiple) + _firstValue;

            _value = Mathf.Clamp(_value, 15, _maxWidth - 15);

            toggle.anchoredPosition = new Vector2(_value, 0);
            if (Input.GetMouseButtonUp(0))
            {
                _firstPosition = Vector3.zero;
                _lastPosition = Vector3.zero;
            }
        }


        currentUIColor = gradientImage.sprite.texture.GetPixel(
            (int) (toggle.anchoredPosition.x * gradientImage.sprite.texture.width / _maxWidth), 1);


        // Debug.Log("UI Color : " + currentUIColor);

        // uiCurrentColor.color = currentUIColor;
        playerMaterial.SetColor("_Color", currentUIColor);
        //playerTrailMaterial.color = currentUIColor;
    }

    private Vector3 InputPosition()
    {
        Vector3 input = Input.mousePosition;

        float mouseRatioX = Input.mousePosition.x / Screen.width;
        float mouseRatioY = Input.mousePosition.y / Screen.height;

        input = new Vector3(mouseRatioX - 0.5f, mouseRatioY - 0.5f, input.z);

        return input;
    }
}