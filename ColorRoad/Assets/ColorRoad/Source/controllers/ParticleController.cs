﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using static UnityEngine.ParticleSystem;
using RoketGame;


public class ParticleController : HyperSceneObj
{
    [HideInInspector] public Vector3 target;
    private ParticleStack ParticleStack;
    private ParticleThrower ParticleThrower;
    private Color currentColor;
    public List<ParticleFx> ParticleSystems;


    public override void Init()
    {
        base.Init();

        ParticleStack = GetComponent<ParticleStack>();
        ParticleThrower = GetComponent<ParticleThrower>();

        StopParticle();

        if (ParticleSystems.Count > 0)
        {
            StartParticle();
        }


        enabled = true;
    }

    void Start()
    {
    }

    public void StopParticle()
    {
        foreach (ParticleFx p in ParticleSystems)
        {
            if (p.Particle.isPlaying) p.Particle.Play();
        }
    }

    public void StartParticle()
    {
        foreach (ParticleFx p in ParticleSystems)
        {
            if (!p.Particle.isPlaying) p.Particle.Stop();
        }
    }

    public void SetColor(Color color)
    {
        currentColor = color;
    }

    private bool success;

    public void SetSuccess(bool value)
    {
        success = value;
        if (ParticleStack) ParticleStack.SetColorMatchStatus(success);
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);
        StopParticle();
    }


    public override void Update()
    {
        Vector3 targetOfParticles = Vector3.zero;
        targetOfParticles.y = target.y;
        updateParticles(targetOfParticles);


        base.Update();
    }


    private void updateParticles(Vector3 _target)
    {
        foreach (ParticleFx p in ParticleSystems)
        {
            p.UpdateParticles(_target, currentColor, success);
        }
    }


    private Vector3 getRandTargetPos(Vector3 _target)
    {
        float offset = 1;
        float x = UnityEngine.Random.Range(_target.x - offset, _target.x + offset);
        float y = UnityEngine.Random.Range(_target.y - offset, _target.y + offset);
        float z = UnityEngine.Random.Range(_target.z - offset, _target.z + offset);
        return new Vector3(x, y, z);
    }


    
}