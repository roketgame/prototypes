﻿using System;
using System.Collections.Generic;
using System.Linq;
using PathCreation;
using UnityEngine;

namespace ColorRoad.Source.controllers
{
    public class CubeController : HyperSceneObj
    {
        private List<Vector3> _tempVertices = new List<Vector3>();
        private List<Vector3> _points = new List<Vector3>();
        private List<Vector3> _centerPoints = new List<Vector3>();
        public int divide;

        public int totalCubeCount;


        public float tolerance = 1.2f;

        public PlayerController playerController;
        public PathCreator pathCreator;

        public Transform cubeHolder;
        public GameObject cube;
        public GameObject emptyCube;

        private int _currentIndex;

        private float _time;

        public float heightOffset;

        public void CreateCubes(GameObject targetObject)
        {
            int a = 0;

            MeshFilter meshFilter = targetObject.GetComponent<MeshFilter>();

            foreach (var vertex in meshFilter.mesh.vertices)
            {
                _tempVertices.Add(vertex);
                a++;

                if (a >= 8)
                {
                    Vector3 tempCenterPoint = Vector3.zero;
                    foreach (var tempVertex in _tempVertices)
                    {
                        tempCenterPoint += tempVertex;
                    }

                    Vector3 tempCalcCenterPoint = Vector3.up * 60 + (tempCenterPoint / divide);


                    tempCalcCenterPoint.x = (float) Math.Round(tempCalcCenterPoint.x, 1);
                    tempCalcCenterPoint.y = (float) Math.Round(tempCalcCenterPoint.y, 1) + heightOffset;
                    tempCalcCenterPoint.z = (float) Math.Round(tempCalcCenterPoint.z, 1);

                    if (!_centerPoints.Contains(tempCalcCenterPoint))
                    {
                        _centerPoints.Add(tempCalcCenterPoint);
                    }

                    a = 0;
                    _tempVertices = new List<Vector3>();
                }
            }

            totalCubeCount = _centerPoints.Count;
            // Sıralama

            _centerPoints = _centerPoints.OrderBy(vector3 => vector3.y).ToList();

            targetObject.SetActive(false);

            float totalRoadLength = 0;

            for (int i = 0; i < pathCreator.path.localPoints.Length - 1; i++)
            {
                totalRoadLength +=
                    Vector3.Magnitude(pathCreator.path.localPoints[i + 1] - pathCreator.path.localPoints[i]);
            }

            //totalRoadLength -= 100;


            // Bounds cubeBounds = cube.GetComponent<Renderer>().bounds;
            // playerController.refreshDistance = (cubeBounds.max.x + cubeBounds.min.x) / 2 * _centerPoints.Count / totalRoadLength ; 
            //playerController.refreshDistance = totalRoadLength / _centerPoints.Count;


            // (Yol / Cube Count) * totaltime / Yol 

            float totalTime = ((LevelCont)LevelCont.Instance).GetLevelData().totalTime;
            totalTime = Mathf.Clamp(totalTime, 10, 100);
            playerController.refreshDistance =   totalRoadLength / _centerPoints.Count * (totalTime / totalRoadLength);

            playerController.pathFollower.speed = totalRoadLength / totalTime;

            playerController.holeRefresh = 2.0f / playerController.pathFollower.speed;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;

            foreach (var point in _centerPoints)
            {
                Gizmos.DrawSphere(point, 0.5f);
            }
        }

        public Vector3 GetCurrentTarget()
        {
            Vector3 vector = Vector3.zero;

            if (_currentIndex < _centerPoints.Count)
            {
                vector = _centerPoints[_currentIndex];
                _currentIndex++;
            }

            return vector;
        }


        public void AddCube(Color currentColor)
        {
            if (_currentIndex < _centerPoints.Count)
            {
                Transform cubeTransform =
                    Instantiate(cube, _centerPoints[_currentIndex], Quaternion.identity, cubeHolder).transform;
                cubeTransform.GetComponent<Renderer>().material.color = currentColor;
            }
        }

        public void AddEmptyCube()
        {
            if (_currentIndex < _centerPoints.Count)
            {
                Transform cubeTransform =
                    Instantiate(emptyCube, _centerPoints[_currentIndex], Quaternion.identity, cubeHolder).transform;
            }
        }
    }
}