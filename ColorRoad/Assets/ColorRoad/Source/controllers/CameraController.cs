﻿using System;
using System.Collections;
using System.Collections.Generic;
using PathCreation;
using PathCreation.Examples;
using UnityEngine;

public class CameraController : HyperSceneObj
{
    public float orbitDictance = 30f;
    private bool isGameOver = false;

    public GameObject orbitPos;
    
    public override void Update()
    {
        // transform.RotateAround(lookAtTarget.position, Vector3.up, -10 * Time.deltaTime);

        base.Update();
    }

    public override void GameOver(int result)
    {
        if(!isGameOver)
        {
            isGameOver = true;
             // LeanTween.rotateLocal(this.gameObject,  new Vector3(45, -87, 15), 2.5f).setOnComplete(o => { StartCoroutine(FinishCameraRotation()); });
            this.transform.parent = orbitPos.transform;
            // LeanTween.move(this.gameObject, orbitPos.position, 2f).setOnComplete(o => { FinishCameraRotation(); });
            LeanTween.moveLocal(this.gameObject,new Vector3(0,40,orbitDictance), 5f).setOnComplete(o => { FinishCameraRotation(); });
            LeanTween.rotateLocal(this.gameObject,new Vector3(0,180,0), 5f);

            LeanTween.rotateAround(this.transform.parent.gameObject, Vector3.up, 360, 10f).setLoopClamp();
            // Transform childCam =  this.transform.GetChild(0);
            // LeanTween.moveLocal(childCam.gameObject, new Vector3(0,0,orbitDictance), 2f);
            // LeanTween.rotateLocal(childCam.gameObject, new Vector3(0,180,0), 2f);
        }
      
       
       
    }

    private void FinishCameraRotation()
    {
        // while (true)
        // {
        //     // this.transform.RotateAround(Vector3.zero, Vector3.up, 50 * Time.deltaTime);
        //     // this.transform.RotateAround(Vector3.zero, Vector3.up, 50 * Time.deltaTime);
        //     yield return null;
        // }




       

    }
    
}