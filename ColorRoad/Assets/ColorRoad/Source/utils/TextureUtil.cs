﻿using System.Collections.Generic;
using UnityEngine;

namespace ColorRoad.Source.utils
{
    public static class TextureUtil
    {
        private static bool CheckColorCount(Color[] colors)
        {
            if (colors == null || colors.Length == 0)
            {
                Debug.LogError("No colors assigned");
                return false;
            }

            if (colors.Length > 64)
            {
                Debug.LogWarning("Too many colors! maximum is 10, assigned: " + colors.Length);
                return false;
            }

            return true;
        }

        public static Sprite CreateSprite(Texture2D texture2D)
        {
            return Sprite.Create(texture2D, new Rect(0, 0, texture2D.width, texture2D.height), new Vector2(0.5f, 0.5f));
        }

        private static Gradient CreateGradient(int length, Color[] colors)
        {
            // create gradient
            Gradient gradient = new Gradient();
            var colorKeys = new GradientColorKey[length];
            var alphaKeys = new GradientAlphaKey[length];

            float steps = length - 1f;

            for (int i = 0; i < length; i++)
            {
                float step = i / steps;

                colorKeys[i].color = colors[i];
                colorKeys[i].time = step;
                alphaKeys[i].alpha = colors[i].a;
                alphaKeys[i].time = step;
            }

            gradient.SetKeys(colorKeys, alphaKeys);
            return gradient;
        }

        private static Texture2D CreateTexture(int width, int height, TextureProperties properties)
        {
            Texture2D texture = new Texture2D(width, height, TextureFormat.ARGB32, false, properties.isLinear);
            texture.wrapMode = properties.textureWrapMode;
            texture.filterMode = properties.filterMode;
            return texture;
        }


        public static Texture2D GenerateGradientTexture(int width, int height, Color[] colors,
            TextureProperties properties)
        {
            if (!CheckColorCount(colors))
            {
                return null;
            }

            int length = colors.Length;

            Gradient gradient = CreateGradient(length, colors);
            Texture2D texture = CreateTexture(width, height, properties);

            if (width > height)
            {
                for (int i = 0; i < width; i++)
                {
                    texture.SetPixel(i, 0, gradient.Evaluate((float) i / (float) width));
                }
            }
            else if (width < height)
            {
                for (int i = 0; i < height; i++)
                {
                    texture.SetPixel(0, i, gradient.Evaluate((float) i / (float) height));
                }
            }

            for (int i = 0; i < width; i++)
            {
            }

            texture.Apply(properties.hasMipMap);

            SaveTextureToFile(texture, "gradient.png");

            Debug.Log("Dosya Kaydedildi -> " + Application.persistentDataPath + "/" + "gradient.png");
            return texture;
        }

        public static Texture2D GenerateTexture(int width, int height, Color[] colors, TextureProperties properties)
        {
            if (!CheckColorCount(colors))
            {
                return null;
            }

            Texture2D texture = CreateTexture(width, height, properties);

            if (width > height)
            {
                for (int i = 0; i < width; i++)
                {
                    texture.SetPixel(i, 0, colors[i]);
                }
            }
            else if (width < height)
            {
                for (int i = 0; i < height; i++)
                {
                    texture.SetPixel(0, i, colors[i]);
                }
            }


            texture.Apply(properties.hasMipMap);

            SaveTextureToFile(texture, "texture.png");

            //Debug.Log("Dosya Kaydedildi -> " + Application.persistentDataPath + "/" + "texture.png");
            return texture;
        }

        private static void SaveTextureToFile(Texture2D texture, string filename)
        {
            System.IO.File.WriteAllBytes(Application.persistentDataPath + "/" + filename, texture.EncodeToPNG());
        }

        public static Color[] GenerateColorsRandom(int colorCount)
        {
            List<Color> colors = new List<Color>();

            if (colorCount <= 0)
            {
                Debug.LogError("ColorCount cannot be 0 or less!");
                return colors.ToArray();
            }

            for (int i = 0; i < colorCount; i++)
            {
                string seed = Random.ColorHSV().ToString();
                System.Random random = new System.Random(seed.GetHashCode());

                Color color = new Color(
                    ((float) random.NextDouble()),
                    ((float) random.NextDouble()),
                    ((float) random.NextDouble()),
                    1
                );

                colors.Add(color);
            }

            return colors.ToArray();
        }

        public static Color[] GenerateColors(int colorCount)
        {
            List<Color> colors = new List<Color>();

            if (colorCount <= 0)
            {
                Debug.LogError("ColorCount cannot be 0 or less!");
                return colors.ToArray();
            }

            for (int i = 0; i < colorCount; i++)
            {
                string seed = Random.ColorHSV().ToString();
                System.Random random = new System.Random(seed.GetHashCode());

                Color color = new Color(
                    ((float) random.NextDouble()),
                    ((float) random.NextDouble()),
                    ((float) random.NextDouble()),
                    1
                );

                colors.Add(color);
            }

            return colors.ToArray();
        }

        public static Color[] GetColorsFromTexture(Texture2D texture2D)
        {
            List<Color> _colors = new List<Color>();

            Color[] colors = texture2D.GetPixels();
            if (colors.Length <= 0)
            {
                Debug.Log("Texture yok");
                return null;
            }

            for (int i = 0; i < colors.Length; i++)
            {
                _colors.Add(colors[i]);
            }

            return _colors.ToArray();
        }

        public static Color[] GetColorsFromTextureForUI(Texture2D texture2D)
        {
            List<Color> _colors = new List<Color>();

            Color[] colors = texture2D.GetPixels();
            if (colors.Length <= 0)
            {
                Debug.Log("Texture yok");
                return null;
            }

            for (int i = 0; i < colors.Length; i++)
            {
                if (!_colors.Contains(colors[i]))
                {
                    _colors.Add(colors[i]);
                }
            }

            return _colors.ToArray();
        }
    }

    public class TextureProperties
    {
        public TextureWrapMode textureWrapMode;
        public FilterMode filterMode;
        public bool isLinear;
        public bool hasMipMap;

        public TextureProperties()
        {
            textureWrapMode = TextureWrapMode.Clamp;
            filterMode = FilterMode.Point;
            isLinear = false;
            hasMipMap = false;
        }

        public TextureProperties(TextureWrapMode textureWrapMode, FilterMode filterMode, bool isLinear, bool hasMipMap)
        {
            this.textureWrapMode = textureWrapMode;
            this.filterMode = filterMode;
            this.isLinear = isLinear;
            this.hasMipMap = hasMipMap;
        }
    }
}