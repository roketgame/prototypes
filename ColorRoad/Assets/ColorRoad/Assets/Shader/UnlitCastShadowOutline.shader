﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "RuckcatGames/UnlitCastShadowOutline" 
{
	Properties 
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Color Tint", Color) = (1,1,1,1) 
		_OutlineColor ("Outline Color", Color) = (0,1,0,1) 
        _Outline ("Outline width", Range (0.002, 0.03)) = 0.01 
	}
	
	SubShader 
	{
		Tags {"RenderType"="Opaque" }
		LOD 100
		Lighting Off Fog { Mode Off }
        ColorMask RGB
		
		// Pass to render object without lighting and shading
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
		    #pragma fragmentoption ARB_precision_hint_fastest
		    #include "UnityCG.cginc"
			#pragma multi_compile_fog
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half3 normal : NORMAL;
				float4 color : COLOR;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			half _Outline;
	        half4 _OutlineColor;
			
			v2f vert (appdata v)
			{
				v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.color = v.color;
                o.uv = v.uv;
                return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col = tex2D(_MainTex, i.uv) * i.color * _Color;
			    return col;
			}
			ENDCG
		}
		
		// Pass to render object as a shadow caster
		Pass 
		{
			Name "CastShadow"
			Tags { "LightMode" = "ShadowCaster" "LightMode" = "Always" }
	
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_shadowcaster
			#include "UnityCG.cginc"
		    #pragma fragmentoption ARB_precision_hint_fastest
	
            struct appdata { 
                 float4 vertex : POSITION; 
                 float3 normal : NORMAL;
             }; 
    
             struct v2f { 
                float4 pos : POSITION; 
                float4 color : COLOR;
             }; 
	
	        float _Outline; 
            float4 _OutlineColor; 
	
			v2f vert( appdata_base v )
			{
				v2f o;
				TRANSFER_SHADOW_CASTER(o)
				o.pos = UnityObjectToClipPos(v.vertex); 
                float3 norm = UnityObjectToViewPos(v.normal);
                norm.x *= UNITY_MATRIX_P[0][0]; 
                norm.y *= UNITY_MATRIX_P[1][1]; 
                o.pos.xy += norm.xy * o.pos.z * _Outline;
                o.pos.z += 0.001;
        
                o.color = _OutlineColor; 
				return o;
			}
	
			float4 frag( v2f i ) : COLOR
			{
				SHADOW_CASTER_FRAGMENT(i)
				return i.color;
			}
			ENDCG
			
			  Cull Front 
             ZWrite On 
             ColorMask RGB 
             Blend SrcAlpha OneMinusSrcAlpha 
             //? -Note: I don't remember why I put a "?" here 
             SetTexture [_MainTex] { combine primary } 
		}
	}
}