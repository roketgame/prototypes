﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class FinishObj : HyperSceneObj
{

    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.tag == "Player")
        {
            Invoke("Win",0.5f);
        }
    }
    private void Win()
    {
        LevelCont.Instance.EndLevel(2);
    }
}
