﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;

public class ThrowHook : HyperSceneObj
{
    public enum Weapons { Star, Spear/*, Boomerang*/ };

    public GameObject spear_Weapon;
    public GameObject star_Weapon;
    public Weapons selectedWeapon;
    public GameObject hook;
    public GameObject nodePrefab;
    public GameObject nullShotPoint;
    [Tooltip("dusmani vurma mesafesi")]
    public float rayMaxDistance;
    [Tooltip("bos atis yapildiginde hook'un gidecegi mesafe")]
    public float nullShotPointDistance;
    // public float searchRadiusForNearbyEnemy;
    // public Vector3 searchBoxScaleForNearbyEnemy;
    [Tooltip("reklam icin yakindaki targeti arama(gercekci degil)")]
    public bool forAdOverlapBox;
    [Tooltip("duvarin yakininda arama yapabilmek icin Box boyutu")]
    public Vector3 WallOverlapBoxScale;
    [Tooltip("duvardan seken atis enemy'e denk geldiginde enemy'nin yakininda arama yapabilmek icin Box boyutu")]
    public Vector3 EnemyOverlapBoxScale;
    [Tooltip("arama yapilan box'i gormek icin = true")]
    public bool overlapBoxDebug;
    public GameObject overlapBox;
    public bool multiKilling;
    public int multiKillingLimits;
    public int wallLimits;

    [NonSerialized]
    public int initCreateHookCount = 40;
    [NonSerialized]
    public List<GameObject> Hooks = new List<GameObject>();
    [NonSerialized]
    public List<GameObject> Nodes = new List<GameObject>();
    [NonSerialized]
    public bool throwActive = false;
    [NonSerialized]
    public bool ropeActive;
    [NonSerialized]
    public List<GameObject> hookWay = new List<GameObject>();
    [NonSerialized]
    public List<GameObject> selecteds = new List<GameObject>();
    [NonSerialized]
    public List<GameObject> killed = new List<GameObject>();
    [NonSerialized]
    public bool shotWallControl;
    [NonSerialized]
    public GameObject currWeapon;



    private GameObject player;
    private GameObject curHook;
    private List<GameObject> selectedsWalls = new List<GameObject>();
    private List<string> selectedsNames = new List<string>();
    private List<string> selectedsWallsNames = new List<string>();
    private GameObject firstHit;
    private GameObject _null;
    private int counter = 0;

    


    public override void StartGame()
    {
        base.StartGame();
        player = GameObject.FindGameObjectWithTag("Player");
        createHookObject();

        //initPlayerSpeed = player.GetComponent<PlayerCont>().currentSpeed;

        if (selectedWeapon == Weapons.Spear)
        {
            currWeapon = Instantiate(spear_Weapon, Vector3.zero, Quaternion.Euler(180,0,0), curHook.transform);
            currWeapon.transform.localPosition = Vector3.zero;
            currWeapon.GetComponent<Weapon>().rotatable=false;
        }
        else if (selectedWeapon == Weapons.Star)
        {
            currWeapon = Instantiate(star_Weapon, Vector3.zero, Quaternion.identity, curHook.transform);
            currWeapon.transform.localPosition = Vector3.zero;
            currWeapon.GetComponent<Weapon>().rotatable = true;
            curHook.GetComponent<LineRenderer>().widthMultiplier = 0 ;
        }
        /*else if (selectedWeapon == Weapons.Boomerang)
        {

        }*/

    }

    public override void Update()
    {
        base.Update();
        if (!throwActive)
        {
            for(int i = 1; i < Nodes.Count; i++)
            {
                Nodes[i].transform.position = Nodes[0].transform.position;
            }
        }
    }


    public void createHookObject()
    {
        curHook = (GameObject)Instantiate(hook, transform.position, Quaternion.identity);
        curHook.SetActive(false);
        Nodes.Add(curHook);

        for (int i = 0; i < initCreateHookCount; i++)
        {
            createNodeObjects();
        }
    }

    public void createNodeObjects()
    {
        GameObject go = (GameObject)Instantiate(nodePrefab, Vector3.zero, Quaternion.identity);
        //go.transform.SetParent(curHook.transform);
        go.SetActive(false);
        Nodes.Add(go);
    }



    
    public void tryThrowHook(Vector3 pos, Vector3 dir)
    {
       // smallCollider = true;
        firstHit = null;
        killed.Clear();
        selecteds.Clear();
        selectedsNames.Clear();
        selectedsWalls.Clear();
        selectedsWallsNames.Clear();
        hookWay.Clear();

        RaycastHit _hit;
        LayerMask mask = LayerMask.GetMask("firstShot");
        if (Physics.Raycast(pos,dir,out _hit,rayMaxDistance, mask))
        {
            if (_hit.collider.tag == "enemyRayCollider" && _hit.collider.transform.parent.transform.GetComponentInChildren<Enemy>().isAlive)
            {
                firstHit = _hit.collider.transform.parent.gameObject;
                killed.Add(_hit.collider.transform.parent.gameObject);
                selecteds.Add(_hit.collider.transform.parent.gameObject);
                selectedsNames.Add(_hit.collider.transform.parent.gameObject.name);
                hookWay.Add(_hit.collider.transform.parent.gameObject);
                shotWallControl = false;
                //Debug.DrawLine(pos, _hit.transform.position, Color.yellow, 2);
                Vector3 _dir = (_hit.collider.transform.parent.transform.position - pos).normalized;
                getTheNearestTarget(_hit.collider.transform.parent.transform.position,Vector3.zero /*searchBoxScaleForNearbyEnemy*/, new Vector3(_dir.x * 50, 0, _dir.z * 50));
            }
        }
        else mask = LayerMask.GetMask("Body");
        if (Physics.Raycast(pos, dir, out _hit, rayMaxDistance, mask))
        {
            if (_hit.collider.gameObject.tag == "Wall")
            {
                firstHit = _hit.collider.gameObject;
                selectedsWalls.Add(_hit.collider.gameObject);
                selectedsWallsNames.Add(_hit.collider.gameObject.name);
                hookWay.Add(_hit.collider.gameObject);
                shotWallControl = true;
                //Debug.DrawLine(pos, _hit.transform.position, Color.black, 2);
                getTheNearestTarget(_hit.collider.transform.position, WallOverlapBoxScale, dir);
            }
        }
        else
        {
            _null = Instantiate(nullShotPoint);
            _null.transform.position = (player.transform.position + new Vector3(nullShotPointDistance * dir.x / 50, 0, nullShotPointDistance * dir.z / 50));
            selecteds.Add(_null);
            selectedsNames.Add(_null.name);
            hookWay.Add(_null);
            //(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(norm.x*50, 2, norm.y*100));
            Debug.DrawLine(pos, _null.transform.position, Color.yellow, 2);
            // getTheNearestTarget((player.transform.position + (dir * 100)), searchBoxScaleForNearbyEnemy, dir);


            //Debug.DrawRay(pos, dir, Color.red, 2);
        }

        if (selecteds.Count>0)
        {
            throwActive = true;
            //player.GetComponent<PlayerCont>().currentSpeed = slowSpeed;
            StartCoroutine(throwOnAllTarget(curHook.GetComponent<HookRope>().speed / hookWay.Count));
            player.GetComponentInChildren<Player_Anim>().PlayThrowAnimation();
            
        }
        else if (selectedsWalls.Count>0)
        {
            throwActive = true;
            //player.GetComponent<PlayerCont>().currentSpeed = slowSpeed;
            StartCoroutine(throwOnAllTarget(curHook.GetComponent<HookRope>().speed / hookWay.Count));
            player.GetComponentInChildren<Player_Anim>().PlayThrowAnimation();

        }
        else
        {
            throwActive = false;

        }



    }
    //recursive bir func her vurulan dusmanın etrafında adam var mı diye tekrar tekrar kontrol eder
    private void getTheNearestTarget(Vector3 pos,Vector3 scale,Vector3 dir)
    {
        // Collider[] _hit = Physics.OverlapSphere(pos, radius);
        if (shotWallControl)
        {
            Collider[] _hit;

            if (forAdOverlapBox)
            {
                _hit = Physics.OverlapBox(new Vector3(pos.x, pos.y, pos.z ), scale, Quaternion.identity);
                if (overlapBoxDebug)
                {
                    GameObject go = Instantiate(overlapBox, new Vector3(pos.x, pos.y, pos.z ), Quaternion.identity);
                    go.transform.localScale = scale * 2;
                }
            }
            else
            {

                _hit = Physics.OverlapBox(new Vector3(pos.x, pos.y, pos.z + (scale.z / 2) + 10), scale, Quaternion.identity);
                if (overlapBoxDebug)
                {
                    GameObject go = Instantiate(overlapBox, new Vector3(pos.x, pos.y, pos.z + (scale.z / 2) + 10), Quaternion.identity);
                    go.transform.localScale = scale * 2;
                }
            }

            
            

            GameObject _nearestTarget = gameObject;

            if (_hit.Length > 0)
            {
                float _distance = 999;

                // find nearest enemy
                foreach (Collider rayCast in _hit)
                {
                    if (rayCast.tag == "enemyBody" && rayCast.transform.parent.gameObject.tag == "Enemy" && !selectedsNames.Contains(rayCast.transform.parent.gameObject.name) && rayCast.transform.parent.transform.GetComponentInChildren<Enemy>().isAlive)
                    {
                        if (_distance > Vector3.Distance(pos, rayCast.transform.parent.transform.position))
                        {
                            _distance = Vector3.Distance(pos, rayCast.transform.parent.transform.position);
                            _nearestTarget = rayCast.transform.parent.gameObject;
                            //Debug.Log(_nearestTarget.name);
                        }
                    }
                    else if (firstHit.tag == "Wall" && rayCast.gameObject.tag == "Wall" && !selectedsWallsNames.Contains(rayCast.gameObject.name) && selectedsWalls.Count < wallLimits)
                    {
                        if (_distance > Vector3.Distance(pos, rayCast.transform.position))
                        {
                            _distance = Vector3.Distance(pos, rayCast.transform.position);
                            _nearestTarget = rayCast.gameObject;

                            //Debug.Log(_nearestTarget.name);
                        }
                    }
                }




                foreach (Collider rayCast in _hit)
                {
                   // Debug.Log(_nearestTarget.name + " ---- " + rayCast.transform.parent.gameObject.name + " ---- " + rayCast.name);
                    if (rayCast.transform.parent.gameObject.name == _nearestTarget.name || rayCast.name == _nearestTarget.name)
                    {
                        
                        if ((multiKilling && selecteds.Count < multiKillingLimits) || selecteds.Count == 0)
                        {
                            if (rayCast.gameObject.tag == "Wall")
                            {
                                // Debug.Log("wall" +_nearestTarget.name + " ---- " + rayCast.transform.parent.gameObject.name + " ---- " + rayCast.name);
                                selectedsWalls.Add(rayCast.gameObject);
                                selectedsWallsNames.Add(rayCast.gameObject.name);
                                hookWay.Add(rayCast.gameObject);
                                getTheNearestTarget(rayCast.transform.position, WallOverlapBoxScale, dir);
                                break;


                            }
                            else if (rayCast.transform.parent.gameObject.tag == "Enemy" && rayCast.tag == "enemyBody")
                            {
                                killed.Add(rayCast.transform.parent.gameObject);
                                selecteds.Add(rayCast.transform.parent.gameObject);
                                selectedsNames.Add(rayCast.transform.parent.gameObject.name);
                                hookWay.Add(rayCast.transform.parent.gameObject);

                                if (firstHit.tag == "Wall")
                                {
                                    //Debug.Log(_nearestTarget.name + " ---- " + rayCast.transform.parent.gameObject.name + " ---- " + rayCast.name);

                                    getTheNearestTarget(_nearestTarget.transform.position, EnemyOverlapBoxScale, dir);

                                }
                                //else getTheNearestTarget(rayCast.transform.parent.transform.position, searchBoxScaleForNearbyEnemy, dir);
                                break;

                            }

                            // Debug.DrawLine(pos, rayCast.transform.position, Color.blue, 2);


                        }
                    }
                }

            }
            /*if (_nearestTarget==gameObject)
            {
                nullWayObjectCreate(hookWay[hookWay.Count - 1].transform.position + dir , dir);
            }*/


        }
        else
        {
            LayerMask mask = LayerMask.GetMask("Body");
            RaycastHit _hit;

            if (Physics.Raycast(pos, dir, out _hit, nullShotPointDistance, mask))
            {

                //firstHit = go.collider.gameObject;
                if (_hit.collider.tag == "enemyBody" && _hit.collider.transform.parent.gameObject.tag == "Enemy" && _hit.collider.transform.parent.transform.GetComponentInChildren<Enemy>().isAlive && !selectedsNames.Contains(_hit.collider.transform.parent.gameObject.name))
                {
                    Debug.Log(_hit.collider.name);

                    selecteds.Add(_hit.collider.transform.parent.gameObject);
                    selectedsNames.Add(_hit.collider.transform.parent.gameObject.name);
                    killed.Add(_hit.collider.transform.parent.gameObject);
                    _null = Instantiate(nullShotPoint);
                    _null.transform.position = (_hit.point);
                    hookWay.Add(_null);
                    //(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(norm.x*50, 2, norm.y*100));
                    Debug.DrawLine(pos, _null.transform.position, Color.yellow, 2);

                    getTheNearestTarget(_null.transform.position + dir / 100, EnemyOverlapBoxScale, dir);
                    //debug.DrawLine(pos, go.collider.transform.position, Color.yellow, 2);
                    // getTheNearestTarget(_hit.transform.position, searchBoxScaleForNearbyEnemy, dir);
                }
                else if (_hit.collider.gameObject.tag == "Wall")
                {
                    firstHit = _hit.collider.gameObject;
                    selectedsWalls.Add(_hit.collider.gameObject);
                    selectedsWallsNames.Add(_hit.collider.gameObject.name);
                    hookWay.Add(_hit.collider.gameObject);
                    shotWallControl = true;
                    //Debug.DrawLine(pos, _hit.transform.position, Color.black, 2);
                    getTheNearestTarget(_hit.collider.transform.position, WallOverlapBoxScale, dir);

                }
                
                
            }
            else
            {
                nullWayObjectCreate(pos, dir);
            }
            
        }





    }


    void nullWayObjectCreate(Vector3 pos, Vector3 dir)
    {
        _null = Instantiate(nullShotPoint);
        _null.transform.position = (pos + new Vector3(nullShotPointDistance * dir.x / 100, 0, nullShotPointDistance * dir.z / 100));
        selecteds.Add(_null);
        selectedsNames.Add(_null.name);
        killed.Add(null);
        hookWay.Add(_null);
        //(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(norm.x*50, 2, norm.y*100));
        Debug.DrawLine(pos, _null.transform.position, Color.yellow, 2);
        // getTheNearestTarget((player.transform.position + (dir * 100)), searchBoxScaleForNearbyEnemy, dir);
    }


    private IEnumerator throwOnAllTarget(float waitTime)
    {

        counter = 0;
        int killNum = 0;
        foreach (GameObject go in hookWay)
        {
            if (counter==0)
            {
                curHook.transform.position = transform.position;
            }
            if (hookWay.Count-1==counter)
            {
                //if (hookWay[counter].tag == "Wall") continue;
            }
            
            Vector3 destiny = go.transform.position;
            curHook.SetActive(true);
            curHook.GetComponent<HookRope>().setVariables();
            if (go.tag =="Enemy" || go.tag=="Node")
            {
                if (killed.Count>killNum)
                {
                    curHook.GetComponent<HookRope>().moveToTarget(destiny, curHook, killed[killNum]);
                    killNum++;
                }
                else
                {
                    curHook.GetComponent<HookRope>().moveToTarget(destiny, curHook, null);

                }
            }
            else if (go.tag =="Wall")
            {
                curHook.GetComponent<HookRope>().moveToTarget(destiny, curHook, null);
            }
            
            ropeActive = true;
        

            counter++;

            yield return new WaitForSeconds(waitTime);
        }
        //pullBackHookObjects();
        //StartCoroutine(callBack(0.1f));
        if (selecteds.Count>0)
        {
            curHook.GetComponent<HookRope>().comeBack();
            throwActive = false;
            if (HyperConfig.Instance.ShotEnemyTutorial)
            {
                Tutorial.Instance.Stop();
                HyperConfig.Instance.ShotEnemyTutorial = false;
            }
            if (selectedsWalls.Count > 0)
            {
                if (HyperConfig.Instance.ShotWallTutorial)
                {
                    Tutorial.Instance.Stop();
                    HyperConfig.Instance.ShotWallTutorial = false;
                }
            }
        }
        else if (selectedsWalls.Count>0)
        {
            curHook.GetComponent<HookRope>().comeBack();
            throwActive = false;
            if (HyperConfig.Instance.ShotWallTutorial)
            {
                Tutorial.Instance.Stop();
                HyperConfig.Instance.ShotWallTutorial = false;
            }
        }

        if (killed.Count > 3 && killed.Count < 6)
        {
            LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromScreenPoint("Screen",2, "NICE", new Vector2(1000, 2500) / 2.56f, new Vector2(1100, 2550) / 2.56f, 0.5f);
        }
        else if (killed.Count > 6)
        {
            LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromScreenPoint("Screen", 0, "PERFECT", new Vector2(800, 2500) / 2.56f, new Vector2(900, 2450) / 2.56f, 0.5f);
        }
        if (killed.Count > 6)
        {
            LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromScreenPoint("Screen", 1, "WOOOW", new Vector2(Screen.width/2, 1150) / 2.56f, new Vector2(Screen.width / 2, 200) / 2.56f, 0.5f);
        }
    }


}
