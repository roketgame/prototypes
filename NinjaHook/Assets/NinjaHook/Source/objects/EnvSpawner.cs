﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class EnvSpawner : HyperSceneObj
{
    public GameObject[] PrefabItem;
    private bool isStartSpawn;
    private float timer = 0;
    private int counter = 3;
    public float BuildLenght = 0;
    GameObject player;
    public int leftRandMin=-15;
    public int leftRandMax=15;
    public int rightRandMin = -30;
    public int rightRandMax = 30;
    public float SpawnYMin = 25;
    public float SpawnYMax = 25;
    public float SpawnX=25;
    public bool spawnStart=false;
    public int initSpawnCount=25;
    public override void Init()
    {
        base.Init();
        isStartSpawn = true;
        enabled = true;
        player = GameObject.FindGameObjectWithTag("Player");
        counter = (transform.childCount - 1) / 2;
        if (spawnStart)
        {
            for (int i = 0; i < initSpawnCount; i++)
            {
                spawnEnvItem();
            }
        }
    }

    public override void StartGame()
    {
        base.StartGame();
        //StartCoroutine(spawnIE(20 / player.GetComponent<PlayerCont>().currentSpeed));

    }
    public override void GameOver(int result)
    {
        base.GameOver(result);
        StopAllCoroutines();

    }
    public void stopAllCoroutines()
    {
        StopAllCoroutines();
    }
    public IEnumerator spawnIE(float seconds)
    {
        float elapsedTime = 0;
        while (elapsedTime < seconds)
        {
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
            spawnEnvItem();
            StartCoroutine(spawnIE(seconds));
    }
    public void spawnEnvItem()
    {
        counter++;
        Vector3 pos = new Vector3(gameObject.transform.position.x, Random.Range(SpawnYMin, SpawnYMax),  (counter * BuildLenght) );


        int rand = Random.Range(leftRandMin, leftRandMax);
        int index = Random.Range(0, PrefabItem.Length);
        spawnItem(index, pos - new Vector3(SpawnX,0,rand),transform);


        index = Random.Range(0, PrefabItem.Length);
        rand = Random.Range(rightRandMin, rightRandMax);
        spawnItem(index, pos + new Vector3(SpawnX, 0,rand), transform);
    }
    
    private void spawnItem(int index ,Vector3 pos ,Transform parent)
    {
        CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(PrefabItem[index], pos, parent);
    }
}