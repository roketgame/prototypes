﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Enemy : HyperSceneObj
{
    public bool moveActive=false;
    public float leftRight_SpeedMin;
    public float leftRight_SpeedMax;
    public float leftLimit;
    public float rightLimit;
    public float runToPlayer_Speed;
    public float LookToPlayer_Speed;
    public float walkingSpeedTowardsPlayer;
    public float seeingDistanceToPlayer;
    public ParticleSystem deadParticle;
    public ChibiExplosion rigBody;
    public GameObject hideSingleBody;

    [System.NonSerialized]
    public Vector3 initPos;
    [System.NonSerialized]
    public bool isAlive = true;

    private int tweenId;
    private GameObject player;
    private string activeDirection;
    private float speed;
    public override void Init()
    {
        base.Init();

        player = GameObject.FindGameObjectWithTag("Player");
        isAlive = true;
        initPos = transform.parent.localPosition;

        setDirection();
        if(moveActive)
            gameObject.GetComponent<Chibi_Anim>().anim.SetBool("start", true);
    }

    public IEnumerator Right()
    {
        //gameObject.transform.parent.transform.LeanMoveX(-8, 2);
        speed = Random.Range(leftRight_SpeedMax, leftRight_SpeedMin);
        gameObject.GetComponent<Chibi_Anim>().anim.speed = ((leftRight_SpeedMax + leftRight_SpeedMin) / 2.0f) / speed;


        tweenId = LeanTween.moveX(gameObject.transform.parent.gameObject, rightLimit,speed).id;

        while (true)
        {
            if (activeDirection == "right" && isAlive)
            {
                if (gameObject.transform.parent.transform.position.x + (8) < 0.5f)
                {
                    gameObject.transform.parent.transform.Rotate(0, +180, 0, Space.Self);
                    activeDirection = "left";
                    StartCoroutine(Left());

                }
            }
            else
            {

                //LeanTween.cancel(tweenId);
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator Left()
    {
        speed = Random.Range(leftRight_SpeedMax, leftRight_SpeedMin);
        gameObject.GetComponent<Chibi_Anim>().anim.speed = ((leftRight_SpeedMax + leftRight_SpeedMin) / 2.0f) / speed;


        tweenId = LeanTween.moveX(gameObject.transform.parent.gameObject, leftLimit, speed).id;
        while (true)
        {
            if (activeDirection == "left" && isAlive)
            {
                if (gameObject.transform.parent.transform.position.x - (8) > -0.5f)
                {
                    gameObject.transform.parent.transform.Rotate(0, +180, 0, Space.Self);
                    activeDirection = "right";
                    StartCoroutine(Right());
                }
            }
            else
            {

                //LeanTween.cancel(tweenId);
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }
    
    public void dead()
    {
        MobileVibration.Vibrate(MobileVibration.Level.LOW);
        rigBody.gameObject.SetActive(true);
        rigBody.Explode();
        deadParticle.Play();
        //Debug.Log(go.name);
        transform.GetComponent<Chibi_Anim>().DEADChibi();
        transform.parent.GetComponent<BoxCollider>().enabled = false;
        foreach (Collider co in transform.parent.GetComponentsInChildren<BoxCollider>())
        {
            if (co.gameObject.tag=="enemyBody" || co.gameObject.tag == "enemyRayCollider")
            {

                co.enabled = false;
            }
        }
        isAlive = false;
        hideSingleBody.SetActive(false);

        LevelCont.Instance.gameObject.GetComponent<Notify>().NotifyTextFromWorldPoint("Money",0,"5",transform.position,transform.position + new Vector3(0, 5, 0) , 0.75f);

        LevelCont.Instance.SetLevelScore(5);
    }

    public void setSpawnParams()
    {
        rigBody.transform.GetChild(0).transform.GetChild(0).localPosition = new Vector3(-0.001325556f, 0.2564822f, 0.01535492f);
        rigBody.transform.GetChild(0).transform.GetChild(0).localRotation = Quaternion.Euler(0, 0, 0);
        rigBody.transform.GetChild(1).transform.GetChild(0).localPosition = new Vector3(-0.0005671271f, 0.2175536f, -0.000203768f);
        rigBody.transform.GetChild(1).transform.GetChild(0).localRotation = Quaternion.Euler(0, 0, 0);


        transform.parent.rotation = Quaternion.Euler(0,180,0);
        isAlive = true;
        transform.parent.localPosition = initPos;
        transform.parent.GetComponent<BoxCollider>().enabled = true;
        foreach (Collider co in transform.parent.GetComponentsInChildren<BoxCollider>())
        {
            if (co.gameObject.tag=="enemyBody" || co.gameObject.tag == "enemyRayCollider")
            {
                co.enabled = true;
            }
        }
        if (transform.parent.GetComponentInChildren<ChibiExplosion>() != null)
        {
            transform.parent.GetComponentInChildren<ChibiExplosion>().gameObject.SetActive(false);
        }
        
        setDirection();
        moveLeftRight();

    }
    public void setDirection()
    {
        if (moveActive)
        {
            int rand = Random.Range(0, 2);
            if (rand == 0)
            {
                activeDirection = "left";
                gameObject.transform.parent.transform.Rotate(0, -90, 0, Space.Self);
                // StartCoroutine(Left());
            }
            else
            {
                activeDirection = "right";
                gameObject.transform.parent.transform.Rotate(0, +90, 0, Space.Self);
                // gameObject.transform.parent.transform.LeanMoveX(-8, 2);
                //StartCoroutine(Right());

            }
        }
    }
    public void moveLeftRight()
    {
        if (moveActive)
        {
            if (activeDirection == "left")
            {
                StartCoroutine(Left());
            }
            else
            {
                StartCoroutine(Right());

            }
        }
    }

    public override void Update()
    {
        base.Update();
        if (!isAlive)
        {
            LeanTween.cancel(tweenId);
        }

        if (GameStatus == GameStatus.STARTGAME)
        {
            if (isAlive)
            {

                Collider[] __hit = Physics.OverlapBox(transform.parent.position-new Vector3(0,0, seeingDistanceToPlayer), new Vector3(15, 1, seeingDistanceToPlayer), Quaternion.identity);

                if (__hit.Length > 0)
                {
                    int counter = 0;

                    foreach (Collider go in __hit)
                    {
                        if (go.tag == "Player")
                        {
                            counter++;
                            LeanTween.cancel(tweenId);

                            transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, Quaternion.LookRotation(player.transform.position - transform.parent.position), LookToPlayer_Speed * Time.deltaTime);
                            transform.parent.position += transform.parent.forward * runToPlayer_Speed * Time.deltaTime;
                           // runToPlayer_Speed = Random.Range(1, runToPlayer_Speed);
                        }
                    }
                    if (counter == 0)
                    {
                        //Debug.Log("fast");
                    }
                }
                else
                {
                    //Debug.Log("fast");
                }
            }
        }
        if (GameStatus == GameStatus.GAMEOVER)
        {
            if (isAlive)
            {
                if (moveActive)
                {
                    LeanTween.cancel(tweenId);
                }
               /* transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, Quaternion.LookRotation(player.transform.position - transform.parent.position), LookToPlayer_Speed * Time.deltaTime);
                
                transform.parent.position += transform.parent.forward * runToPlayer_Speed * 2 * Time.deltaTime;*/
            }
        }
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);
        if (result == 1)//lose
        {
            if (isAlive)
            {
                if (gameObject.activeInHierarchy)
                {
                    StartCoroutine(moveToPlayer());
                }
            }
        }
        else if (result == 2)//win
        {
            if (isAlive)
            {
                gameObject.GetComponent<Chibi_Anim>().DEADChibi();

                isAlive = false;
            }
        }
    }


    public IEnumerator moveToPlayer()
    {
        while (true)
        {
            transform.parent.rotation = Quaternion.Slerp(transform.parent.rotation, Quaternion.LookRotation(player.transform.position - transform.parent.position), LookToPlayer_Speed * Time.deltaTime);

            transform.parent.position += transform.parent.forward * runToPlayer_Speed * 2 * Time.deltaTime;

            if (player.GetComponent<PlayerCont>()._enemyStopDistance > Vector3.Distance(player.transform.position, transform.parent.position))
            {
                //Debug.Log(" ss" + transform.parent.name + " " + Vector3.Distance(player.transform.position, transform.parent.position));
                player.GetComponent<PlayerCont>()._enemyStopDistance += 5;
                gameObject.GetComponent<Chibi_Anim>().WinChibi();
                break;
            }

            yield return new WaitForEndOfFrame();
        }
    }



}
