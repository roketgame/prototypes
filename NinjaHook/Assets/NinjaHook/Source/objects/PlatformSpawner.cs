﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;
using Random = UnityEngine.Random;

public class PlatformSpawner : HyperSceneObj
        
{
    //public GameObject[] PrefabItem;
    public GameObject platformPrefabWithoutBlocks;
    private bool isStartSpawn;
    private float timer = 0;
    private int counter=3;
    public float Platformlength = 0;
    public bool platformsWithoutBlocks = false;
    private int platformCounter = 0;
    public EnvSpawnerControl[] envSpawnerControl;
    //public int levelDiff = 0;

    float totalPrefab;
    List<GameObject> randomPrefabs;
    List<GameObject> linearPrefabs;
    GameObject endLevel;
    int levelIndex;
    public override void Init()
    {
        base.Init();
        isStartSpawn = true;
        enabled = true;

        counter = transform.childCount - 1;
        levelIndex = ((LevelCont)HyperLevelCont.Instance).GetLevelIndex();
        randomPrefabs = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformRandomPrefabs(levelIndex);
        linearPrefabs = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformLinearPrefabs(levelIndex);
        endLevel = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformEndLevel(levelIndex);

        //levelDiff = (((LevelCont)HyperLevelCont.Instance).CurrLevel / 10);
        for (int i=0;i<6;i++)
        {
            spawnItem();
        }
    }
  
    public override void StartGame()
    {
        base.StartGame();
        /*prefabs = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformPrefabs();
        endLevel = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformEndLevel();*/
    }
    float totalRange;
    public void spawnItem()
    {
        float jumpRange;
        if (platformCounter >= ((LevelCont)HyperLevelCont.Instance).GetTotalPrefab(levelIndex))
        {
            spawnFinishItem();
            return;
        }
        

        if (Random.Range(0,5) >= 3)
        {
            jumpRange = Random.Range(5.0f,10.0f);
            totalRange += jumpRange;
        }


        
        counter++;
        Vector3 pos = new Vector3(gameObject.transform.position.x, 5, 20 + (counter * Platformlength) + totalRange);
        CoreSceneObject item;
        if (platformsWithoutBlocks)
        {
             item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(platformPrefabWithoutBlocks, pos, transform);
        }
        else
        {
            //linear
            if (platformCounter < ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformLinearPrefabs(levelIndex).Count)
            {
               
                item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(linearPrefabs[platformCounter], pos, transform);
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    if (item.transform.GetChild(i).tag == "Enemy")
                    {
                        item.transform.GetChild(i).GetComponentInChildren<Enemy>().hideSingleBody.SetActive(true);
                        item.transform.GetChild(i).GetComponentInChildren<Enemy>().setSpawnParams();
                    }
                    else if (item.transform.GetChild(i).tag == "Wall")
                    {
                        item.transform.GetChild(i).GetComponent<Wall>().setVariables();
                    }
                }
            }
            else //Random
            {
                int platform = Random.Range(0, (randomPrefabs.Count));
                item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(randomPrefabs[platform], pos, transform);
                for (int i = 0; i < item.transform.childCount; i++)
                {
                    if (item.transform.GetChild(i).tag == "Enemy")
                    {
                        item.transform.GetChild(i).GetComponentInChildren<Enemy>().hideSingleBody.SetActive(true);
                        item.transform.GetChild(i).GetComponentInChildren<Enemy>().setSpawnParams();
                    }
                    else if (item.transform.GetChild(i).tag == "Wall")
                    {
                        item.transform.GetChild(i).GetComponent<Wall>().setVariables();
                    }
                }
            }
        }
        platformCounter++;
    }


    bool finished;
    public void spawnFinishItem()
    {
        if (!finished)
        {
            finished = true;
            platformCounter++;
            counter++;
            Vector3 pos = new Vector3(gameObject.transform.position.x, 5, 20 + (counter * Platformlength) + totalRange);
            CoreSceneObject item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(endLevel, pos, transform);
            // item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(PrefabItem[platform], pos, transform);
            foreach (EnvSpawnerControl es in envSpawnerControl)
            {
                es.finished = true;
            }
        }

    }
}
