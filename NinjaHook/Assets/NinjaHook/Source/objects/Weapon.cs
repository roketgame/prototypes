﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Weapon : HyperSceneObj
{
    public bool rotatable=false;
    public override void Init()
    {
        base.Init();
    }
    public override void Update()
    {
        base.Update();

        if (rotatable)
        {
            if (gameObject.activeSelf)
            {
                if (gameObject.tag == "rotatable")
                {
                    //Debug.Log(gameObject.name);
                    transform.Rotate(0, 15, 0, Space.Self);
                }
            }
        }
        
    }
}
