﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ChibiExplosion : HyperSceneObj
{
    public Transform explosionPoint;
    public float radius = 5.0F;

    public float power = 10.0F;

    public override void Init()
    {
        base.Init();
        if (transform.GetComponentInChildren<Rigidbody>() != null)
        {
            foreach (var chibiRigidbody in transform.GetComponentsInChildren<Rigidbody>())
            {
                chibiRigidbody.isKinematic = true;

            }
        }
        gameObject.SetActive(false);
    }
    public void Start()
    {
        
    }

    public void Explode()
    {
       // if (transform.GetComponentInChildren<Rigidbody>() != null)
        {
            foreach (var chibiRigidbody in transform.GetComponentsInChildren<Rigidbody>())
            {
                chibiRigidbody.isKinematic = false;
            }
        }

        Vector3 forceDir = new Vector3();
        forceDir.y = -350;
        forceDir.x = Random.Range(-2f, 2f);
        forceDir.z = Random.Range(0, 5);

        Vector3 explosionPos = explosionPoint.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if(rb != null)
            {
                if (rb.gameObject.tag == "forceBody")
                {

                    rb.AddForce(forceDir, ForceMode.Impulse);
                }
                //rb.AddForce(hit.transform.up * (-100), ForceMode.Impulse);
            }
            /*if (rb != null) 
                rb.AddExplosionForce(power, explosionPos, radius, 3.0F);*/
        }
    }
}