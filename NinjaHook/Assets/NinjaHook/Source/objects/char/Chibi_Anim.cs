﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Chibi_Anim : HyperSceneObj
{
    public Animator anim;

    public override void StartGame()
    {
        base.StartGame();
        anim.applyRootMotion = false;
        anim.SetBool("start", true);
    }
    public void DEADChibi()
    {
         anim.Play("Dead");
    }
    public void WinChibi()
    {
        anim.applyRootMotion = true;
        anim.Play("Win");

    }
}
