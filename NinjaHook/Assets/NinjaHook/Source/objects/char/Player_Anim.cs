﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Player_Anim : HyperSceneObj
{
    public Animator anim;
    private static readonly int Start = Animator.StringToHash("Start");
    private static readonly int Throw = Animator.StringToHash("Throw");
    private static readonly int StopThrow = Animator.StringToHash("StopThrow");
    private static readonly int Walk = Animator.StringToHash("Walk");

    public override void StartGame()
    {
        base.StartGame();
        anim.applyRootMotion = false;
        anim.SetBool(Start, true);
    }

    public void PlayDeadAnimation()
    {
        anim.Play("Dead");
    }

    public void PlayWinAnimation()
    {
        anim.applyRootMotion = true;
        anim.Play("Win");
    }

    public void PlayThrowAnimation()
    {
        anim.SetTrigger(Throw);
        anim.speed = 0.15f;
    }

    public void StopThrowAnimation()
    {
        anim.SetTrigger(StopThrow);
        anim.speed = 1f;
    }

    public void PlayWalkAnimation(bool walk)
    {
        anim.SetBool(Walk, walk);
    }

    public void jumpStart()
    {
        //anim.Play("jumpStart");
        anim.SetBool("jumpStartParam", true);
    }
    public void jumpFinish()
    {
        anim.SetBool("jumpStartParam", false);
        anim.SetTrigger("jumpFinishParam");
    }

    public void runJump()
    {
        
        anim.SetTrigger("runJump");
    }
}