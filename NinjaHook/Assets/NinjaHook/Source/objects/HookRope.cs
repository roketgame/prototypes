﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;

public class HookRope : HyperSceneObj
{
    public GameObject nodePrefab;
    public Material ropeMaterial;
    public float speed = 0.2f;
    public float getComeBackAllNodesTime = 0.2f;
    //public float getInLineAllNodesTime = 0.2f;
    //public float getComeBackAllNodesTime = 0.2f; //old

    [NonSerialized]
    public Vector3 destiny;
    [NonSerialized]
    public List<GameObject> Nodes = new List<GameObject>();
    [NonSerialized]
    public GameObject player;
    [NonSerialized]
    public GameObject lastNode;
    [NonSerialized]
    public LineRenderer lr;



    private int activeNodeCounter = 1; // start 1 
    private float _Allspeed;
    private int vertexCount = 2; //start 2 
    private bool comeback = false;

    public void setVariables()
    {
        lr = GetComponent<LineRenderer>();
        player = GameObject.FindGameObjectWithTag("Player");
        Nodes = player.GetComponent<ThrowHook>().Nodes;
        _Allspeed = speed / player.GetComponent<ThrowHook>().hookWay.Count;
    }
    public void moveToTarget(Vector3 targetPos, GameObject gameObject, GameObject go)
    {
        transform.LookAt(targetPos);
        if (player.GetComponent<ThrowHook>().hookWay[player.GetComponent<ThrowHook>().hookWay.Count-1] == go)
        {
            LeanTween.move(gameObject, targetPos, 0.1f).setEaseOutBack();
            StartCoroutine(MoveOverSeconds(gameObject, targetPos, _Allspeed, go));


        }
        else
        {
            LeanTween.move(gameObject, targetPos, _Allspeed).setEaseInSine();
            StartCoroutine(MoveOverSeconds(gameObject, targetPos, _Allspeed, go));


        }

    }


    public IEnumerator MoveOverSeconds(GameObject objectToMove, Vector3 end, float seconds, GameObject go)
    {
        float elapsedTime = 0;
        Vector3 startingPos = objectToMove.transform.position;
        while (elapsedTime < seconds)
        {
            objectToMove.transform.position = Vector3.Lerp(startingPos, end, elapsedTime / seconds);
            elapsedTime += Time.deltaTime;

            yield return new WaitForEndOfFrame();
        }

        if (go)
        {
            if (go.tag == "Enemy")
            {
                go.GetComponentInChildren<Enemy>().dead();
            }

        }
        // objectToMove.transform.position = end;

        //active 1 node
        setNode(end);
    }


    void setNode(Vector3 pos)
    {
        GameObject go = player.GetComponent<ThrowHook>().Nodes[activeNodeCounter];
        go.transform.position = pos;
        activeNodeCounter++;
        vertexCount++;
        go.SetActive(true);

        RenderLine();
        /*

        lr.positionCount = (vertexCount);
        int i;*/


        //nodes
        /*for (i = 1; i < Nodes.Count; i++)
        {
            if (Nodes[i].gameObject.activeSelf)
            {
                LeanTween.value(Nodes[i], Nodes[i].transform.position, Nodes[i].transform.position + Vector3.forward*5, 0.2f).setOnUpdate((Vector3 val) =>
                {
                    lr.SetPosition(vertexCount - i - 1, val);
                    Debug.Log(val);
                });
                
            }
            else break;
        }*/
    }

    public override void Update()
    {
        base.Update();
        if (player.GetComponent<ThrowHook>().throwActive)
        {
            RenderLine();
        }
        else comeBack();
    }


    void RenderLine()
    {
        lr.positionCount = (vertexCount);
        int i;

        lr.SetPosition(0, Nodes[0].transform.position);//hook-first
        lr.SetPosition(vertexCount - 1, player.transform.position);//player-last


        //nodes
        for (i = 1; i < Nodes.Count; i++)
        {
            if (Nodes[i].gameObject.activeSelf)
            {
                lr.SetPosition(vertexCount - i - 1, Nodes[i].transform.position);
            }
            else break;
        }

    }
    public void comeBack()
    {
        float percentile = 1.0f / ((float)vertexCount);

        lr.positionCount = (vertexCount);
        int i;

        if (!comeback)
        {
            lr.SetPosition(0, Nodes[0].transform.position);//hook-first
        }

        lr.SetPosition(vertexCount - 1, player.transform.position);//player-last

        //StartCoroutine(VectorLerp(vertexCount - 1 - 1, Nodes[1], Vector3.Lerp(player.transform.position, Nodes[0].transform.position, percentile * 1), 10));
        /*for (i = 0; i < Nodes.Count; i++)
        {
            if (Nodes[i].activeSelf)
            {
                if (!comeback)
                {
                    if (i == 0)
                    {
                        LeanTween.value(Nodes[0], lr.GetPosition(0), player.transform.position, getInLineAllNodesTime);
                        LeanTween.value(Nodes[0], lr.GetPosition(0), player.transform.position, getInLineAllNodesTime).setOnUpdate((Vector3 val) =>
                        {
                            lr.SetPosition(0, val);
                            Debug.Log(val);
                        });
                    }
                    else
                    {
                        LeanTween.value(Nodes[i], lr.GetPosition(0), player.transform.position, getInLineAllNodesTime);
                        LeanTween.value(Nodes[i], lr.GetPosition(vertexCount - i - 1), Vector3.Lerp(player.transform.position, Nodes[0].transform.position, percentile * i), getInLineAllNodesTime).setOnUpdate((Vector3 val) =>
                        {
                            lr.SetPosition(vertexCount - i - 1, val);

                        });

                    }
                }
            }
            else break;
        }*/
        
        StartCoroutine(getInLineAllNodes(percentile, getComeBackAllNodesTime));

    }


    public IEnumerator getInLineAllNodes(float percentile, float seconds)
    {
        yield return new WaitForSeconds(0.1f);
        float elapsedTime = 0;
        transform.LookAt((player.GetComponent<ThrowHook>().hookWay[player.GetComponent<ThrowHook>().hookWay.Count - 1].transform.position - player.transform.position).normalized + player.GetComponent<ThrowHook>().hookWay[player.GetComponent<ThrowHook>().hookWay.Count - 1].transform.position);


        

        while (elapsedTime < seconds)
        {

            int i;
            for (i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].activeSelf)
                {
                    if (!comeback)
                    {
                        if (i==0)
                        {
                            if (seconds>0.5f)
                            {
                                Nodes[0].transform.position = Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (400.0f * seconds) / seconds));
                                lr.SetPosition(0, Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (400.0f * seconds) / seconds)));
                            }
                            else
                            {
                                Nodes[0].transform.position = Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (80.0f * seconds) / seconds));
                                lr.SetPosition(0, Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (80.0f * seconds) / seconds)));
                            }
                        }
                        else
                        {
                            if (seconds>0.5f)
                            {
                                Nodes[i].transform.position = Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (100.0f * seconds) /*(20.0f * seconds*0.8f)*/ / seconds));
                                lr.SetPosition(vertexCount - i - 1, Vector3.Lerp(lr.GetPosition(vertexCount - i - 1), Vector3.Lerp(player.transform.position, Nodes[0].transform.position, percentile * i), (elapsedTime / (100.0f * seconds)/*(20.0f * seconds *0.8f)*/ / seconds)));

                            }
                            else
                            {
                                Nodes[i].transform.position = Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / (20.0f * seconds) /*(20.0f * seconds*0.8f)*/ / seconds));
                                lr.SetPosition(vertexCount - i - 1, Vector3.Lerp(lr.GetPosition(vertexCount - i - 1), Vector3.Lerp(player.transform.position, Nodes[0].transform.position, percentile * i), (elapsedTime / (20.0f * seconds)/*(20.0f * seconds *0.8f)*/ / seconds)));

                            }

                        }
                    }
                }
                else break;
            }
            if (elapsedTime / seconds > 0.7f)
            {
                for (i = 0; i < Nodes.Count; i++)
                {
                    if (!Nodes[i].activeSelf)
                        break;
                    Nodes[i].transform.position = player.transform.position;
                    if (lr.positionCount> i)
                    {

                        lr.SetPosition(i, player.transform.position);
                    }
                }
                holdBack();
            }

            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        holdBack();


    }

    /*public IEnumerator getInLineAllNodes(float percentile, float seconds)
    {
        float elapsedTime = 0;
        transform.LookAt(( player.GetComponent<ThrowHook>().hookWay[player.GetComponent<ThrowHook>().hookWay.Count-1].transform.position- player.transform.position).normalized + player.GetComponent<ThrowHook>().hookWay[player.GetComponent<ThrowHook>().hookWay.Count - 1].transform.position);

        while (elapsedTime < seconds)
        {
            int i;

            for (i = 1; i < Nodes.Count; i++)
            {
                if (Nodes[i].activeSelf)
                {
                    if (!comeback)
                    {

                        lr.SetPosition(vertexCount - i - 1, Vector3.Lerp(lr.GetPosition(vertexCount - i - 1), Vector3.Lerp(player.transform.position, Nodes[0].transform.position, percentile * i), (elapsedTime /  8 / * (/ *10.0f * 10 * / seconds))* / / seconds)));
                    }
                }
                else break;
            }
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        StartCoroutine(ComeBackAllNodes(getBackAllNodesTime));

    }
    public IEnumerator ComeBackAllNodes(float seconds)
    {
        float elapsedTime = 0;
        comeback = true;

        while (elapsedTime < seconds)
        {
            int i;
            //lr.SetPosition(0, Vector3.Lerp(lr.GetPosition(0), player.transform.position, (elapsedTime / seconds)));
            for (i = 0; i < Nodes.Count; i++)
            {
                if (Nodes[i].activeSelf)
                {
                    Nodes[i].transform.position = Vector3.Lerp(lr.GetPosition(0), player.transform.position,((elapsedTime/ (10.0f * 8 * seconds)) / seconds));

                    lr.SetPosition(i, Vector3.Lerp(lr.GetPosition(0), player.transform.position, ((elapsedTime / (10.0f * 8 * seconds)) / seconds)));

                }
                else break;
            }/ *
            if (elapsedTime / seconds > 0.80f)
            {
                holdBack();
            }* /
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        holdBack();
    }*/

    void holdBack()
    {

        for (int i = 0; i < Nodes.Count; i++)
        {
            if (Nodes[i].activeSelf)
            {
                Nodes[i].transform.position = player.transform.position;
            }
            else break;
        }


        for (int i = 0; i < Nodes.Count; i++)
        {
            if (Nodes[i].activeSelf)
            {
                Nodes[i].SetActive(false);
            }
            else break;
        }

        player.GetComponent<ThrowHook>().ropeActive = false;
        player.GetComponent<ThrowHook>().throwActive = false;
        //player.GetComponent<ThrowHook>().smallCollider = false;
        activeNodeCounter = 1;
        vertexCount = 2;
        //player.GetComponent<PlayerCont>().currentSpeed = player.GetComponent<ThrowHook>().initPlayerSpeed;
        player.GetComponentInChildren<Player_Anim>().StopThrowAnimation();
        gameObject.SetActive(false);


    }

}