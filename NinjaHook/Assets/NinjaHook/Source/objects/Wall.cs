﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Wall : HyperSceneObj
{
    public bool movailableWall;
    public float movingZLimit=10f;
    public float movingTime=5;

    private int tweenId=0;
    public void setVariables()
    {
        if (tweenId == 0)
        {
            if (movailableWall)
            {
                tweenId = LeanTween.move(gameObject, new Vector3(
                    gameObject.transform.position.x,
                    gameObject.transform.position.y,
                    gameObject.transform.position.z + movingZLimit), movingTime).setLoopPingPong().id;
            }
        }
        else
        {
            if (movailableWall)
            {
                LeanTween.cancel(tweenId);
                tweenId = LeanTween.move(gameObject, new Vector3(
                    gameObject.transform.position.x,
                    gameObject.transform.position.y,
                    gameObject.transform.position.z + movingZLimit), movingTime).setLoopPingPong().id;
            }
        }
    }

}
