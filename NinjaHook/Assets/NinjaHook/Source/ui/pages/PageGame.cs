﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGame : HyperPageGame
{

    public override void Init()
    {
        base.Init();
    
    }

    public override void Update()
    {
        base.Update();
    }



    protected override void onBtnStartClicked(TouchUI _touch)
    {
        base.onBtnStartClicked(_touch);
       
       
    }

    public void UpdateWillCatch(Dictionary<string, float> listWillCatch)
        {
            foreach (KeyValuePair<string, float> elem in listWillCatch)
            {
                SetText(elem.Key, elem.Value.ToString());
            }

            
        }




}
