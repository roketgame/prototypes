﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class CameraCont : HyperCameraCont
{
    public void SetNextCam(int stateNum, float animTime)
    {
        Transform item = transform.GetChild(stateNum);
        AnimTo(item.gameObject.name, animTime);
    }

}
