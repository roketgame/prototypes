﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelItem
{
    public float TotalPrefab = 10;
    public List<GameObject> LinearPrefabs;
    public List<GameObject> RandomPrefabs;
    public GameObject LevelEndPrefab;
    public float speed;
}
public class LevelCont : HyperLevelCont
{
    public List<LevelItem> ListLevelPlatforms;
    public ParticleSystem[] particles;
    public float prefabCounter;

    //public Material skyMat;
    // public Light mainLight;
    // public float lightMin;
    // float initLightIntensity;
    public override void Init()
    {
        base.Init();
        //initLightIntensity = mainLight.intensity;

    }
    public override void InitLevel()
    {
        base.InitLevel();
        LevelTotalTime = 10 + CurrLevel * 5;

    }

    public override void Update()
    {
        base.Update();

        //skyMat.SetTextureOffset("_MainTex",new Vector2( (LevelTime/2.0f) / LevelTotalTime, 0)); // LevelTime = LevelTotalTime;
        // mainLight.intensity = Mathf.Clamp(initLightIntensity - ((LevelTime / 2.0f) * 0.4f) / LevelTotalTime, initLightIntensity-0.2f, initLightIntensity);
    }
    public override int GetLevelResult()
    {
        return 2; //oyun zamani doldugunda kazanir
    }
    public override void EndLevel(int levelResult)
    {
        base.EndLevel(levelResult);

        if (levelResult == 2)
        {
            SetLevelScore(50);
            for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }


            PlayerCont.Instance.gameObject.GetComponentInChildren<Player_Anim>().PlayWinAnimation();
            StartCoroutine(setWinCam(3, 2));
        }
    }
    public int GetLevelIndex()
    {
        int indexOfArr = ListLevelPlatforms.Count - 1;

        if (CurrLevel - 1 < ListLevelPlatforms.Count)
            indexOfArr = CurrLevel - 1;

        return indexOfArr;
    }
    public List<GameObject> GetLevelPlatformLinearPrefabs(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].LinearPrefabs;
    }
    public List<GameObject> GetLevelPlatformRandomPrefabs(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].RandomPrefabs;
    }
    public GameObject GetLevelPlatformEndLevel(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].LevelEndPrefab;
    }
    public float GetLevelSpeed(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].speed;
    }
    public float GetTotalPrefab(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].TotalPrefab;

    }

    private IEnumerator setWinCam(int cam, float waitTime)
    {
        ((CameraCont)HyperCameraCont.Instance).SetNextCam(cam, waitTime);

        yield return new WaitForSeconds(waitTime + 1);
    }
}
