﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class PlayerCont : HyperPlayerCont
{
    public float playerSpeed = 10; //currentSpeed
    public float maxSpeed = 50;
    public float distanceToSeeTheEnemy = 50;
    public float deadDistance = 10;
    [Tooltip("Game fail durumunda enemylerin art arda durma mesafesi")]
    public float _enemyStopDistance = 5;

    private LayerMask mask;
    private RaycastHit _hit;
    private bool jumpControl = false;
    private int tweenId;
    private bool enemyTutorialCont = true;
    private bool wallTutorialCont = true;
    private bool isFirstTouch = true;



    public override void Init()
    {
        base.Init();
        CoreInputCont.Instance.EventTouch.AddListener(onTouch);
        CoreInputCont.Instance.EventSwipe.AddListener(onSwipe);
        //LeanTween.move(gameObject, gameObject.transform.position + Vector3.forward * 15, 5).setEaseInOutElastic();
        mask = LayerMask.GetMask("platform");
        playerSpeed = ((LevelCont)HyperLevelCont.Instance).GetLevelSpeed(((LevelCont)HyperLevelCont.Instance).GetLevelIndex());
    }
    public override void Update()
    {
        base.Update();
        if (GameStatus == GameStatus.STARTGAME)
        {
            float _speed = Mathf.Clamp(playerSpeed, -maxSpeed, maxSpeed) * Time.deltaTime;
            gameObject.transform.parent.transform.Translate(0, 0, _speed);

            gameObject.transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x, -5, 5), Mathf.Clamp(gameObject.transform.position.y, 2.5f, 10), gameObject.transform.position.z);

            if (!Physics.Raycast(gameObject.transform.position + new Vector3(0, 2, 2), -Vector3.up * 10, out _hit, 100, mask))
            {
                if (!jumpControl)
                {

                    gameObject.GetComponentInChildren<Player_Anim>().jumpStart();
                    tweenId = LeanTween.moveY(gameObject.transform.parent.gameObject, 5, 0.25f).id;
                    //Debug.Log("jump");
                    jumpControl = true;
                }
            }
            else
            {
                if (Physics.Raycast(gameObject.transform.position + new Vector3(0, 2, 1), -Vector3.up * 10, out _hit, 100, mask))
                {
                    if (jumpControl)
                    {

                        LeanTween.cancel(tweenId);
                        LeanTween.moveY(gameObject.transform.parent.gameObject, 0, 0.25f);
                        gameObject.GetComponentInChildren<Player_Anim>().jumpFinish();

                        //Debug.Log("jumpFinish");
                        jumpControl = false;
                    }

                }
            }

            // Debug.DrawRay(gameObject.transform.position + new Vector3(0, 2, 10), -Vector3.up * 10, Color.red, 5);

            /* //run-Walk
             if (!GetComponent<ThrowHook>().ropeActive)
             {

                 Collider[] __hit = Physics.OverlapBox(new Vector3(transform.position.x + 0, transform.position.y + 0, transform.position.z + 12.5f), new Vector3(9, 1, distanceToSeeTheEnemy), Quaternion.identity);

                 if (__hit.Length > 0)
                 {
                     int counter = 0;

                     foreach (Collider go in __hit)
                     {
                         if (go.tag == "Enemy" && go.GetComponentInChildren<Enemy>().isAlive)
                         {
                             counter++;
                             //Debug.Log(go.name);

                             GetComponentInChildren<Player_Anim>().PlayWalkAnimation(true);
                             currentSpeed = walkSpeed;
                             //Debug.Log("Slow");
                         }
                     }
                     if (counter == 0)
                     {
                         GetComponentInChildren<Player_Anim>().PlayWalkAnimation(false);
                         //Debug.Log("fast");
                         currentSpeed = runSpeed;
                     }
                 }
                 else
                 {
                     GetComponentInChildren<Player_Anim>().PlayWalkAnimation(false);
                     //Debug.Log("fast");
                     currentSpeed = runSpeed;
                 }
             }*/
            //Dead
            //if (!GetComponent<ThrowHook>().ropeActive)
            {

                Collider[] other = Physics.OverlapBox(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(9, 1, deadDistance), Quaternion.identity);

                if (other.Length > 0)
                {
                    foreach (Collider go in other)
                    {
                        if (go.tag == "Enemy")
                        {
                            if (go.GetComponentInChildren<Enemy>().isAlive)
                            {
                                //Debug.Log("GameOver");
                                GetComponentInChildren<Player_Anim>().PlayDeadAnimation();
                                LevelCont.Instance.EndLevel(1);
                                StartCoroutine(setGameOverCam(1, 2));
                            }
                        }
                    }
                }
                tutorialCont();
            }
        }
    }

    private void tutorialCont()
    {
        if (HyperConfig.Instance.ShotEnemyTutorial)
        {
            if (enemyTutorialCont)
            {
                Collider[] others = Physics.OverlapBox(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(15, 1, 40), Quaternion.identity);

                if (others.Length > 0)
                {
                    foreach (Collider go in others)
                    {
                        if (go.tag == "Enemy")
                        {
                            if (go.GetComponentInChildren<Enemy>().isAlive)
                            {
                                Vector3 start = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position);
                                Vector3 end = Camera.main.WorldToScreenPoint(go.transform.position);
                                Tutorial.Instance.SavePositions(0, start, end, 1);
                                Tutorial.Instance.Play(0);
                                enemyTutorialCont = false;
                            }
                        }
                    }
                }
            }
        }
        if (HyperConfig.Instance.ShotWallTutorial)
        {
            if (wallTutorialCont)
            {
                Collider[] others = Physics.OverlapBox(new Vector3(transform.position.x, transform.position.y, transform.position.z), new Vector3(15, 1, 30), Quaternion.identity);

                if (others.Length > 0)
                {
                    foreach (Collider go in others)
                    {
                        if (go.tag == "Wall")
                        {
                            Vector3 start = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position);
                            Vector3 end = Camera.main.WorldToScreenPoint(go.transform.position);
                            Tutorial.Instance.SavePositions(0, start, end, 1);
                            Tutorial.Instance.Play(0);
                            wallTutorialCont = false;
                        }
                    }
                }
            }
        }
    } 

    
    private IEnumerator setGameOverCam(int cam,float waitTime)
    {
        ((CameraCont)HyperCameraCont.Instance).SetNextCam(cam, waitTime);

        yield return new WaitForSeconds(waitTime+1);
        if ((cam+1)<3)
        {
            StartCoroutine(setGameOverCam(cam+1,2));
        }
    }

    private void onTouch(RoketGame.Touch _touch)
    {
        
        if (GameStatus == GameStatus.STARTGAME)
        {

            /*if (swipeControl)
            {
                float xDifference = (_touch.Points[1].x - _touch.Points[0].x);

                float _speed = Mathf.Clamp((xDifference * (LeftRightSpeedMultiplier / 20))*100, -LeftRightMaxSpeed, LeftRightMaxSpeed);
                gameObject.transform.Translate(_speed/100, 0, 0);

            }
            else
            {
                float xPoint = _touch.Points[1].x;
                if (xPoint > 0.5f)
                {
                    gameObject.transform.Translate(LeftRightSpeedMultiplier / 100, 0, 0);
                }
                else if (xPoint < 0.5f)
                {
                    gameObject.transform.Translate(LeftRightSpeedMultiplier / -100, 0, 0);
                }
            }*/
        }
    }
    
    private void onSwipe(RoketGame.Touch _touch)
    {
        
        //
        if (GameStatus == GameStatus.STARTGAME && !isFirstTouch)
        {
         
            if (_hit.collider)
            {


                ThrowHook throwHook = GetComponent<ThrowHook>();

                if (_touch.Phase == TouchPhase.Ended)
                {
                    Vector3 norm = (_touch.GetPoint(1) - new Vector2(0.5f, 0.3f)).normalized;

                    if (!throwHook.throwActive && !throwHook.ropeActive)
                    {
                        //throwHook.throwActive = true;
                        throwHook.tryThrowHook(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(norm.x * 50, 2, norm.y * 100));
                        //throwHook.tryThrowHook(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(_touch.GetNormal().x * 50, 2, _touch.GetNormal().y * 100));
                    }

                    //Tutorial.Instance.Stop();
                }
            }
            /*if (!throwHook.throwActive && !throwHook.ropeActive)
            {
                //throwHook.throwActive = true;
                //throwHook.tryThrowHook(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(norm.x, 2, norm.y));
                //throwHook.tryThrowHook(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(_touch.GetNormal().x * 50, 2, _touch.GetNormal().y * 100));
            }*/

            // Debug.DrawLine(gameObject.transform.position + new Vector3(0, 2, 0), new Vector3(gameObject.transform.position.x + (_touch.GetNormal().x * 2), 2, gameObject.transform.position.z + (_touch.GetNormal().y * 10)), Color.white, 2.5f);
            /*if (throwHook.ropeActive == true)
            {
                throwHook.pullBackHookObjects();
                return;
            }*/

        }

           isFirstTouch = false;

    }


}
