﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;




public enum ThrowStatus { LOAD, HOLD, PULL_UPDATE, RELEASE }
public enum CharacterStatus { IDLE, WALKING, ON_HOLE, BREAK_JOINT, FALL_INTO_HOLE,  PASSED_HOLE_AND_WALKING }

public class ChangeSideEvent : UnityEvent<Character, int>{}
public class CharAnimEvent : UnityEvent<Character, int>{}

