﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using TMPro;
using System.Globalization;

[System.Serializable]
public struct SongBeat
{
    public float Time;
    public int Column;
}
      
public class GameCont : CoreGameCont
{
      public LevelCont Level;
    public override void Init()
    {
#if UNITY_EDITOR
        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = 15;
#endif




        Level = gameObject.AddComponent<LevelCont>();
        Level.Init();
        Level.RestoreLevel();
        base.Init();

        CoreUiCont.Instance.OpenPage<PageIntro>();

    }


    public override void StartGame()
    {
        base.StartGame();
        CoreUiCont.Instance.OpenPage<PageGame>();
        Level.StartGame();
        Level.UpdateLevel();

        CoreUiCont.Instance.GetPageByClass<PageGame>().TxtScore.SetText("Level: " + Level.CurrLevel);


    }

    public void CheckGameResult()
    {
        int gameResult = 0;
        if (gameResult > 0)
        {
            GameOver(gameResult);
        }
    }

    public override int GameOver(int _result)
    {
        int result = base.GameOver(_result);
        if(result > -1)
        {
            string text = "GAME OVER \n";
            if (_result == 1)
            {
                text = "YOU WIN!";
                LevelCont.Instance.SaveLevel();
               
            }
              
            else if (_result == 2)
                text = "YOU LOSE :((((";

            Invoke("nextLevel", 1);
            CoreUiCont.Instance.OpenPage("PageGameOver").GetComponentInChildren<TextMeshProUGUI>().SetText(text);

        }

        return result;
    }

    private void nextLevel()
    {
        RestartGame();
    }


}
