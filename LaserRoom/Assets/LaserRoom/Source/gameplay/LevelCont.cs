﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;


public class LevelCont : CoreCont<LevelCont>
{
    public int CurrLevel;



    public void UpdateLevel()
    {
        switch(CurrLevel)
        {
            case 0:
            {



            }
            break;
        }

        Debug.Log("UpdateLevel " + CurrLevel);

    }

    public void SetLevel(int _newLevel)
    {
        CurrLevel = _newLevel;
        //UpdateLevel();

    }
    //public void NextLevel()
    //{
    //    SetLevel(CurrLevel + 1);
    //}
    public void RestoreLevel()
    {
        Debug.Log("RestoreLevel " );
        int level = 0;
        if (PlayerPrefs.HasKey("Level"))
        {
             level = PlayerPrefs.GetInt("Level");
           
        }
        SetLevel(level);
    }


    public void  SaveLevel()
    {
        PlayerPrefs.SetInt("Level", CurrLevel + 1);
        PlayerPrefs.Save();
    }
}
