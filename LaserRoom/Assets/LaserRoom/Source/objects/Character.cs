﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Character : CoreSceneObject
{
    public Collider _collider;
    public ParticleSystem particle;
    public float Speed = 0.1f;
    public int ColumnIndex;
    public int BeatIndex;



    public Chibi charMesh;
    private HingeJoint joint;
    private GameObject joinTo;
    private float forward = 0;
    public bool IsReadyForBreak = false; //hole'e collide oldu, joint koparmaya hazir
    public bool IsBrokeJoint= false; //joint koparildi, ama hala yurumeye devam
    public bool IsFall = false; //son evere, hole icine dusus
    private TextMeshProUGUI uiText;





    public override void Init()
    {
        _collider = GetComponent<Collider>();
        charMesh = GetComponentInChildren<Chibi>();
        charMesh.name = this.name + "_chibi";
       

        base.Init();
        rb.useGravity = false;

        uiText = GetComponentInChildren<TextMeshProUGUI>();

    }


  

    public override void StartGame()
    {
        base.StartGame();
        //charMesh.EventCollision.AddListener(onMeshCharContact);
        //charMesh.PlayAnim("dancing");
    }

    public void BreakFromRope()
    {
        if(IsReadyForBreak)
        {

            SetStatus(CharacterStatus.BREAK_JOINT);
           
        }
    }
  

    public void Shake()
    {
        LeanTween.cancel(charMesh.gameObject);
        float randDelay = UnityEngine.Random.Range(0f, 0.1f);
        float randRotZ = UnityEngine.Random.Range(-20f, 20f);
        float randRotX = UnityEngine.Random.Range(-5f, 2f);
        float randTime = UnityEngine.Random.Range(0.4f, 0.5f);
        //LeanTween.rotateLocal(charMesh.gameObject, new Vector3(randRotX, 0, randRotZ), randTime).setEase(LeanTweenType.easeOutElastic).setDelay(randDelay);
        //LeanTween.rotateLocal(charMesh.gameObject, new Vector3(0, 0, 0), randTime).setEase(LeanTweenType.easeOutElastic).setDelay(randDelay + randTime - 0.1f);
    }

    public void FixedUpdate()
    {

    }


    public override void Update()
    {

        if (GetStatus() == GameStatus.INIT) return;
        base.Update();


       

     
    }


    public void SetStatus(CharacterStatus _status)
    {
        //switch (_status)
        //{



        //    case CharacterStatus.WALKING:
        //        break;

        //    case CharacterStatus.ON_HOLE:
        //        IsReadyForBreak = true;
        //        break;

        //    case CharacterStatus.BREAK_JOINT:
        //        removeJoint();
        //        IsReadyForBreak = false;
        //        IsBrokeJoint = true;
        //        showCathTime();
        //        break;

        //    case CharacterStatus.FALL_INTO_HOLE:
        //        rb.useGravity = true;
        //        IsFall = true;
        //        HideText();
        //        break;

        //    case CharacterStatus.PASSED_HOLE_AND_WALKING:
        //        IsReadyForBreak = false;
        //        showText(":(", 0.5f);
        //        break;


              



        //}

        //charStatus = _status;
    }


   



    //private void addJoint(GameObject _other)
    //{
    //    //if (joinTo == null)
    //    if(joinTo != _other.gameObject)
    //    {
           
    //        resetLocalPosition();

    //        if (joint == null)
    //        {
    //            joint = gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
    //        }

    //        Rigidbody targetRb = _other.GetComponent<Rigidbody>();
    //        if (targetRb)
    //        {
    //            //transform.position = _other.transform.position;
    //            Debug.Log(name + " joint " + _other.name);
              
    //            joint.connectedBody = targetRb;
    //            //joint.connectedAnchor = transform.position;
    //            joinTo = _other.gameObject;

    //            rb.velocity = Vector3.zero;
    //            //rb.isKinematic = true;

    //            //transform.position.rot


    //        }


    //    }
    //}

    //public void removeJoint()
    //{
    //    if (joinTo != null)
    //    {

    //        Destroy(joint);

    //    }




    //}
}