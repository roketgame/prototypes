﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Holes : CoreSceneObject, ITimelineListener
{
    public Hole[] ListHOles;

 
    public override void Init()
    {
        base.Init();
        

    }

    public override void StartGame()
    {
        base.StartGame();

        foreach (Hole h in ListHOles)
        {
            h.SetHoleStatus(0);
        }
    }

    //public void SetActive(int _holeIndex)
    //{
    //    if(_holeIndex < ListHOles.Length)
    //    {
    //        Hole h = ListHOles[_holeIndex];
    //            h.SetHoleStatus(1);
             
    //    }
       
    //}

    //public void SetDeActive(int _holeIndex)
    //{
    //    if (_holeIndex < ListHOles.Length)
    //    {
    //        Hole h = ListHOles[_holeIndex];
    //            h.SetHoleStatus(0);
    //    }
    //}

    /* ---- ITimelineListener functions --- */
    public void OnTimelineEvent(string action, int beatIndex, float time)
    {
        Debug.Log("timeline holes  onEvent action/beatIndex/time : " + action +"/"+ beatIndex + "/"+ time);
        SongBeat beat = Timeline.Instance.GetBeat(beatIndex);
        int holeIndex = beat.Column;
        if (holeIndex < ListHOles.Length)
        {
            Hole hole = ListHOles[holeIndex];

            if (action == "SetActive")
            {
                hole.SetHoleStatus(1);
            }

            else if (action == "SetDeactive")
            {
                hole.SetHoleStatus(0);
            }

            //else if (action == "Highlight")
            //{
            //    hole.SetHoleStatus(2);
            //}

        }


    }

    public string GetId()
    {
        return name;
    }

}
