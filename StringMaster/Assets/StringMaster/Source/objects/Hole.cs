﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hole : CoreSceneObject
{
    private GameObject fx;
    public override void Init()
    {
        base.Init();
        fx = transform.GetChild(0).gameObject;
        fx.SetActive(false);

    }
    public void SetHoleStatus(int _status)
    {
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        if (_status == 0)
        {
            this.enabled = false;
            meshRenderer.enabled = false;
            fx.SetActive(false);
        }
        else if(_status == 1)
        {
            this.enabled = true;
            meshRenderer.enabled = true;
        }
        else if (_status == 2)
        {
            fx.SetActive(true);
        }

    }
}
