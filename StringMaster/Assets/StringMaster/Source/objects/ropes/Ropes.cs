﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ropes : CoreSceneObject
{
    public Rope[] ListRopes;


    public override void Init()
    {

        base.Init();
        for (int i = 0; i < ListRopes.Length; i++)
        {
            Rope r = ListRopes[i];
            r.ColumnIndex = i;
            r.CreateNodes();
        }
       


    }
    public Vector3 GetRopePos(int _index)
    {
        Vector3 pos = Vector3.zero;
        
        if (_index < ListRopes.Length)
        {
            Rope r = ListRopes[_index];
            pos = r.transform.position;
        }

        return pos;
    }
}
