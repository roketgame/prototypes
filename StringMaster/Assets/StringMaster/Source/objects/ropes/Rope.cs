﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Rope : CoreSceneObject
{
    public GameObject PrefabRopeItem;
    public GameObject PrefabRopeCorner;
    public GameObject PrefabRopeCenter;
    public Color RopeColor;
    public float PullValueZ;
    public float PushForceZ ;
    public int RopeCountFrontOfCenter = 10;
    public int RopeCountBackOfCenter = 5;
    public float RopeSpace = 1;
    public int ColumnIndex;

    private GameObject ropeCenter;


    public override void Init()
    {
        base.Init();
        
       
    }

    public void CreateNodes()
    {
        ropeCenter = spawnItem(PrefabRopeCenter, Vector3.zero, false);
        spawnItems(1, RopeCountFrontOfCenter);
        spawnItems(-1, RopeCountBackOfCenter);
    }


    public void PullStart()
    {
        ropeCenter.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
 
    public void Push(float _z)
    {
        ropeCenter.transform.localPosition = new Vector3(0,0,_z);
        //ropeCenter.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, PushForceZ), ForceMode.Impulse);
        LeanTween.cancel(ropeCenter.gameObject, false);
        //LeanTween.moveZ(ropeCenter.gameObject, -13, 2f).setEase(LeanTweenType.easeOutElastic);
        LeanTween.moveLocalZ(ropeCenter.gameObject, 0, 1f).setEase(LeanTweenType.easeOutElastic);
        //LeanTween.moveZ(ropeCenter.gameObject, -12, 1f).setEase(LeanTweenType.easeOutBack).setDelay(0.8f);
        ropeCenter.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
    }
    private void spawnItems(int _direction, int  _count)
    {
        float x = (RopeSpace / 2) * _direction ;
        Rigidbody prevRb = ropeCenter.GetComponent<Rigidbody>();
        for (int i = 0; i < _count; i++)
        {
            GameObject go;
            Vector3 pos = new Vector3(x, 0, 0);
            if (i == _count - 1)
            {
                go = spawnItem(PrefabRopeCorner, pos, false);
            }
            else
            {
                go = spawnItem(PrefabRopeItem, pos);
            }

            Rigidbody rb = go.GetComponent<Rigidbody>();
            Joint joint = go.GetComponent<Joint>();
            joint.connectedBody = prevRb;

            prevRb = rb;
            x += _direction * RopeSpace;

        }
    }

    private GameObject spawnItem(GameObject prefab, Vector3 _pos, bool _isVisible = true)
    {
        GameObject go = Instantiate(prefab, _pos, Quaternion.identity);
        //Rope rope = CoreSceneCont.Instance.SpawnItem<Rope>(prefab, _pos, transform);
        //GameObject go =  Instantiate(prefab, _pos, Quaternion.identity);
        go.transform.parent = transform;
        go.transform.localPosition = _pos;
        MeshRenderer meshRend = go.GetComponentInChildren<MeshRenderer>();
        if (meshRend)
        {
            meshRend.enabled = _isVisible;
            meshRend.material.color = RopeColor;
        }

        RopeNode ropeNode = go.GetComponent<RopeNode>();
        if (ropeNode)
        {
            ropeNode.ColumnIndex = ColumnIndex;
            ropeNode.Init();
        }
        return go;
    }

    public override void OnTouch(RoketGame.Touch _info)
    {
        base.OnTouch(_info);

        if(_info.Type == TouchPhase.Began)
        {
            PullStart();
            Push(0.7f);
            checkAndBrokeChars();
            SoundCont.Instance.PlayString(ColumnIndex);
        }
    }

    private void checkAndBrokeChars()
    {
        List<Character> list = CharController.Instance.GetChars(ColumnIndex);
        foreach (Character c in list)
        {
            if (c.IsReadyForBreak)
                c.BreakFromRope();

            c.Shake();
        }
    }
}
