﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Character : CoreSceneObject
{
    public Collider _collider;
    public ParticleSystem particle;
    public float Speed = 0.1f;
    public int ColumnIndex;
    public int BeatIndex;



    public Chibi charMesh;
    private HingeJoint joint;
    private GameObject joinTo;
    private float forward = 0;
    public bool IsReadyForBreak = false; //hole'e collide oldu, joint koparmaya hazir
    public bool IsBrokeJoint= false; //joint koparildi, ama hala yurumeye devam
    public bool IsFall = false; //son evere, hole icine dusus
    private Hole currCollideToHole;
    private TextMeshProUGUI uiText;





    public override void Init()
    {
        _collider = GetComponent<Collider>();
        charMesh = GetComponentInChildren<Chibi>();
        charMesh.name = this.name + "_chibi";
       

        base.Init();
        rb.useGravity = false;

        uiText = GetComponentInChildren<TextMeshProUGUI>();
        HideText();

    }


  

    public override void StartGame()
    {
        base.StartGame();
        charMesh.EventCollision.AddListener(onMeshCharContact);
        //charMesh.PlayAnim("dancing");
    }

    public void BreakFromRope()
    {
        if(IsReadyForBreak)
        {

            SetStatus(CharacterStatus.BREAK_JOINT);
           
        }
    }
  

    public void Shake()
    {
        LeanTween.cancel(charMesh.gameObject);
        float randDelay = UnityEngine.Random.Range(0f, 0.1f);
        float randRotZ = UnityEngine.Random.Range(-20f, 20f);
        float randRotX = UnityEngine.Random.Range(-5f, 2f);
        float randTime = UnityEngine.Random.Range(0.4f, 0.5f);
        //LeanTween.rotateLocal(charMesh.gameObject, new Vector3(randRotX, 0, randRotZ), randTime).setEase(LeanTweenType.easeOutElastic).setDelay(randDelay);
        //LeanTween.rotateLocal(charMesh.gameObject, new Vector3(0, 0, 0), randTime).setEase(LeanTweenType.easeOutElastic).setDelay(randDelay + randTime - 0.1f);
    }

    public void FixedUpdate()
    {

        if (!IsFall)
        {
            Vector3 newPos = new Vector3(0, 0, charMesh.transform.localPosition.z + Speed);
            charMesh.transform.localPosition = newPos;

            //
            {
                float distToHole = getDistanceToHole();
                if (distToHole >= -0.4f && distToHole <= 0.4f  )
                {
                    if (IsBrokeJoint)
                        if(distToHole < 0.2f)
                        SetStatus(CharacterStatus.FALL_INTO_HOLE);
                }
                else if (distToHole < 0.4f)
                {
                    if(!IsBrokeJoint) SetStatus(CharacterStatus.PASSED_HOLE_AND_WALKING);
                }
            }
           


        }
    }


    public override void Update()
    {

        if (GetStatus() == GameStatus.INIT) return;
        base.Update();


       

     
    }


    private void onMeshCharContact(RoketGame.ContactInfo _contact)
    {
        if (_contact.Status == ContactStatus.TRIGGER_ENTER)
        {

            Hole hole = _contact.Other.GetComponent<Hole>();
            if (hole)
            {
                currCollideToHole = hole;
                SetStatus(CharacterStatus.ON_HOLE);
                hole.SetHoleStatus(2);

            }
            else if (!IsReadyForBreak && !IsBrokeJoint)
            {
                RopeNode rope = _contact.Other.GetComponent<RopeNode>();
                if (rope)
                {
                    if (rope.ColumnIndex == ColumnIndex)
                    {
                        addJoint(rope.gameObject);
                        SetStatus(CharacterStatus.WALKING);
                    }

                }
            }





        }
        if (_contact.Status == ContactStatus.TRIGGER_EXIT)
        {

            Hole hole = _contact.Other.GetComponent<Hole>();
            if (hole)
            {
                currCollideToHole = null;
                //SetStatus(CharacterStatus.PASSED_HOLE_AND_WALKING);
                hole.SetHoleStatus(0);
            }






        }
    }


    private float getDistanceToHole()
    {
        float dist = 0;
        if (currCollideToHole)
        {
            //Debug.Log("erdal " + charMesh.transform.position + " currCollideToHole " + currCollideToHole.transform.position);
            dist = (charMesh.transform.position.x - currCollideToHole.transform.position.x);
        }
        return dist;
    }

    public void SetStatus(CharacterStatus _status)
    {
        switch (_status)
        {



            case CharacterStatus.WALKING:
                break;

            case CharacterStatus.ON_HOLE:
                IsReadyForBreak = true;
                break;

            case CharacterStatus.BREAK_JOINT:
                removeJoint();
                IsReadyForBreak = false;
                IsBrokeJoint = true;
                showCathTime();
                break;

            case CharacterStatus.FALL_INTO_HOLE:
                rb.useGravity = true;
                IsFall = true;
                HideText();
                break;

            case CharacterStatus.PASSED_HOLE_AND_WALKING:
                IsReadyForBreak = false;
                showText(":(", 0.5f);
                break;


              



        }

        //charStatus = _status;
    }


   


    public bool OnGround()
    {
        bool r = false;
        List<RaycastHit> listResult = Utils.RAYCAST(transform.position, Vector3.down * 2, new string[] { "Ground" });
        if (listResult.Count > 0)
        {
            r = true;
        }
        return r;

    }

    /* hole'e kac sec kala basildi? daha dogrusu timing ne kadar tutturuldu */
    private void showCathTime()
    {
        float timeDist = Timeline.Instance.GetBeatTime(BeatIndex) -  Timeline.Instance.GetCurrentTime();
        showText(timeDist + " sec", 1);
    }
    private void showText(string _str, float _lifetime)
    {
        uiText.gameObject.SetActive(true);
        uiText.SetText(_str);
        Invoke("HideText", _lifetime);
    }
    private void HideText()
    {
        uiText.gameObject.SetActive(false);
    }

    private void resetLocalPosition()
    {
        Vector3 meshPos = charMesh.transform.position;
        meshPos.x =  charMesh.transform.position.x;
        meshPos.y = transform.position.y;
        meshPos.z = transform.position.z;
        transform.position = meshPos;
        charMesh.transform.localPosition = Vector3.zero;
        forward = 0;
    }


    private void addJoint(GameObject _other)
    {
        //if (joinTo == null)
        if(joinTo != _other.gameObject)
        {
           
            resetLocalPosition();

            if (joint == null)
            {
                joint = gameObject.AddComponent(typeof(HingeJoint)) as HingeJoint;
            }

            Rigidbody targetRb = _other.GetComponent<Rigidbody>();
            if (targetRb)
            {
                //transform.position = _other.transform.position;
                Debug.Log(name + " joint " + _other.name);
              
                joint.connectedBody = targetRb;
                //joint.connectedAnchor = transform.position;
                joinTo = _other.gameObject;

                rb.velocity = Vector3.zero;
                //rb.isKinematic = true;

                //transform.position.rot


            }


        }
    }

    public void removeJoint()
    {
        if (joinTo != null)
        {

            Destroy(joint);

        }




    }
}