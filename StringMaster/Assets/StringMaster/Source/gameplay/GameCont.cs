﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using TMPro;
using System.Globalization;

[System.Serializable]
public struct SongBeat
{
    public float Time;
    public int Column;
}
      
public class GameCont : CoreGameCont
{
      public LevelCont Level;
    public Holes holes;
      public Rope ropeCont;
    public float CharSpeed = 0.3f;
    public string[] SongStringDatabase;
    public List<SongBeat> SongData;
    public override void Init()
    {
#if UNITY_EDITOR
        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = 15;
#endif

        gameObject.AddComponent<Timeline>();
        Timeline.Instance.Init();
        SoundCont.Instance.Init();



        Level = gameObject.AddComponent<LevelCont>();
        Level.Init();
        Level.RestoreLevel();
        base.Init();

        CoreUiCont.Instance.OpenPage<PageIntro>();

    }


    public override void StartGame()
    {
        base.StartGame();
        initializeDataAndTimeline();
        CoreUiCont.Instance.OpenPage<PageGame>();
        Level.StartGame();
        Level.UpdateLevel();

        CoreUiCont.Instance.GetPageByClass<PageGame>().TxtScore.SetText("Level: " + Level.CurrLevel);

        Timeline.Instance.StartTimer();

    }

    public void CheckGameResult()
    {
        int gameResult = 0;
        if (gameResult > 0)
        {
            GameOver(gameResult);
        }
    }

    public override int GameOver(int _result)
    {
        int result = base.GameOver(_result);
        if(result > -1)
        {
            string text = "GAME OVER \n";
            if (_result == 1)
            {
                text = "YOU WIN!";
                LevelCont.Instance.SaveLevel();
               
            }
              
            else if (_result == 2)
                text = "YOU LOSE :((((";

            Invoke("nextLevel", 1);
            CoreUiCont.Instance.OpenPage("PageGameOver").GetComponentInChildren<TextMeshProUGUI>().SetText(text);

        }

        return result;
    }

    private void nextLevel()
    {
        RestartGame();
    }


    private void initializeDataAndTimeline()
    {
        SongData = new List<SongBeat>();
        {//parse song data
            foreach (string s in SongStringDatabase)
            {
                string[] parseArr = s.Split('/');
                //string ss = "0,1";
                //Debug.Log("sss  " + float.Parse(ss));
                if (parseArr.Length > 1)
                {
                    int column = int.Parse(parseArr[0]);
                    float time = 0;// (float)double.Parse(parseArr[1], System.Globalization.NumberStyles.AllowDecimalPoint);

                    System.IFormatProvider format;
                    //float.TryParse(parseArr[1], NumberStyles.AllowDecimalPoint, format, out time);
                   time = float.Parse(parseArr[1], CultureInfo.InvariantCulture);
                    SongBeat beat;
                    beat.Time = time;
                    beat.Column = column;
                    SongData.Add(beat);
                }
            }


            int beatIndex = 0;
            float startOffset = 0; //ilk beat time, spawnTime'dan kucukse sarki gec baslamali */
            for (int i = 0; i < SongData.Count; i++)
            {
                SongBeat b = SongData[i];
                float time = b.Time;
                float pathTime = CharController.Instance.GetTotalPathTime();
                Debug.Log("pathTime; " + pathTime);

                if(i==0)
                {
                    if (time < pathTime)
                    {
                        startOffset = pathTime - time;
                    }
                    Timeline.Instance.AddEvent(SoundCont.Instance, time + startOffset, "PlaySong", beatIndex);
                }

                time += startOffset;
                float spawnTime =  (time - pathTime);
                Timeline.Instance.AddEvent(CharController.Instance, spawnTime, "Spawn", beatIndex);
                
                Timeline.Instance.AddEvent(holes, time-1f, "SetActive", beatIndex);
                //Timeline.Instance.AddEvent(holes, time, "Highlight", beatIndex);
                //Timeline.Instance.AddEvent(holes, time+0.5f, "SetDeactive", beatIndex);
                beatIndex++;
            }
        }
    }
}
