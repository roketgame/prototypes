﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundCont : CoreCont<SoundCont>, ITimelineListener
{
    private AudioSource audioSource;

    public  AudioClip[] StringAudios;
    public AudioClip Song;

    public override void Init()
    {
        base.Init();

        audioSource = GetComponent<AudioSource>();
        //audioSource.clip = Song;
        //audioSource.Play();
    }

    public override void StartGame()
    {
        base.StartGame();

    }
    public void PlayString(int _column)
    {
        AudioClip ac = StringAudios[_column];
        //audioSource.clip = ac;
        //audioSource.Play();
    }

    public void OnTimelineEvent(string action, int beatIndex, float time)
    {
        if(action == "PlaySong")
        {
            audioSource.PlayOneShot(Song);
        }
    }

    public string GetId()
    {
        return name;
    }
}
