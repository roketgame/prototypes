﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;


public interface ITimelineListener
{
    void OnTimelineEvent(string action, int beatIndex, float time);
    string GetId();
}

public struct TimelineItemListener
{
    public ITimelineListener Listener;
    //public string Id;
  
    public string Action;
}
public struct TimelineItem
{
    public  List<TimelineItemListener> Listeners;
    public float Time;
    public int BeatIndex;
    public bool IsCalled;
}

public class Timeline : CoreCont<Timeline>
{
    private List<TimelineItem> list;
    private float currTime;
    private bool isStarted;

    public override void Init()
    {
        base.Init();

        list = new List<TimelineItem> ();
    }

    public void StartTimer()
    {
        isStarted = true;

    }

    public void FixedUpdate()
    {
        //Debug.Log("fixed delta " + Time.deltaTime);
        //Debug.Log("fixed fixedDeltaTime " + Time.fixedDeltaTime);
    //}
    //public override void Update()
    //{
        //base.Update();
        if (!isStarted) return;



        if( list.Count > 0)
        {

            for (int i = 0; i < list.Count; i++)
            {
                TimelineItem item = list[i];
                if(!item.IsCalled)
                {
                    if(currTime > item.Time)
                    {
                        onEvent(item);
                        item.IsCalled = true;
                        list[i] = item;
                    }
                }
            }
          
        }
        Debug.Log(" currTime " + currTime );
        currTime += Time.fixedDeltaTime;



    }

    private void onEvent(TimelineItem _item)
    {
        if(!_item.IsCalled)
        {
            for (int i = 0; i < _item.Listeners.Count; i++)
            {
                TimelineItemListener listener = _item.Listeners[i];
                listener.Listener.OnTimelineEvent(listener.Action, _item.BeatIndex, _item.Time);
            }
            //_item.IsCalled = true;
        }
       
        

    }
    public void AddEvent(ITimelineListener _listener, float _time, string _action, int _beatIndex)
    {
        //int searchListener = list.FindIndex(e => e.Listener == _listener);
        //int searchTime = list.FindIndex(e => e.Time == _time);

        //if(searchListener > -1 && searchTime > -1 && (searchListener != searchTime))
        {
            Debug.Log("timeline AddEvent " + _listener.GetId() + " time: " + _time + " _beatIndex : " + _beatIndex);

            TimelineItemListener listener;
            listener.Listener = _listener;
            //listener.Id = _id;
            listener.Action = _action;


            int searchIndexByTime = list.FindIndex(e => e.Time == _time);

            if(searchIndexByTime > -1)
            {
                 list[searchIndexByTime].Listeners.Add(listener);
            }
            else
            {
                TimelineItem item = new TimelineItem();
                item.Listeners = new List<TimelineItemListener>();
                item.Time = _time;
                item.BeatIndex = _beatIndex;

                item.Listeners.Add(listener);
               list.Add(item);


            }


            
            

        }
    }

    public SongBeat GetBeat(int _index)
    {
        //if(_index )
        return ((GameCont)CoreGameCont.Instance).SongData[_index];
    }
    public float GetBeatTime(int _beatIndex)
    {
        float t = GetBeat(_beatIndex).Time;
        return t;
    }

    public float GetCurrentTime()
    {
        return currTime;
    }
}

