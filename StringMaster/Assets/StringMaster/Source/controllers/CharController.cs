﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using Random = UnityEngine.Random;
using TMPro;

public class CharController : CoreCont<CharController>, ITimelineListener
{
    public Ropes Ropes;
    public GameObject CharPrefab;
    public float DistanceToHole = 0f;
    public List<Character> listChars;
    private int createdLastCharIndex = 0;
    public float KillDistance = -10; //x
    public override void StartGame()
    {
        base.StartGame();
        DistanceToHole = transform.position.x - 0;
        listChars = new List<Character>();
    }

 

    public override void Update()
    {
        base.Update();
        if (GetStatus() != GameStatus.STARTGAME) return;

        //if (isSongStarted)
        //{
        //    songTime += Time.fixedDeltaTime;
        //    //Debug.Log("songTime : " + songTime);

        //    List<SongBeat> data = ((GameCont)CoreGameCont.Instance).SongData;
        //    if (createdLastCharIndex < data.Count)
        //    {
        //        float charTimeToHole = GetTotalPathTime();



        //        SongBeat currBeat = data[createdLastCharIndex];
        //        Debug.Log("songTime "+ songTime + " createdLastCharIndex : " + createdLastCharIndex + "  currBeat.time: " + currBeat.Time);
        //        if (songTime > currBeat.Time - charTimeToHole - 0.1f)
        //        {
        //            CreateChar(currBeat.Column);
        //            createdLastCharIndex++;
        //        }
        //    }


        //    foreach (SongBeat b in data)
        //    {
        //        float beforeTimeForStart = 1f;
        //        float afterTimeForEnd = 1f;
        //        if(songTime > b.Time - beforeTimeForStart && songTime < b.Time + afterTimeForEnd)
        //        {
        //            holes.SetActive(b.Column);
        //        }
        //        if (songTime > b.Time + afterTimeForEnd)
        //        {
        //            holes.SetDeActive(b.Column);
        //        }
        //    }



           
     
        //}




    }

    public Character  CreateChar(int _column)
    {
        Character Char = createChar();
        Char.ColumnIndex = _column;
        Vector3 pos = transform.position;
        pos.z = Ropes.GetRopePos(_column).z;
        Char.transform.position = pos;
        Char.transform.eulerAngles = new Vector3(0,-90,0);
        Char.Speed = ((GameCont) CoreGameCont.Instance).CharSpeed;
        listChars.Add(Char);
        return Char;
    }

    public List<Character> GetChars(int _column)
    {
        List<Character> listColumnChars = new List<Character>();
        listColumnChars = listChars.FindAll(elem => elem.ColumnIndex == _column);
        return listColumnChars;
    }

    public Character createChar()
    {
        Character Char = CoreSceneCont.Instance.SpawnItem<Character>(CharPrefab, transform.position, transform);
        Char.name = "Char_" + Char.gameObject.GetInstanceID();
        return Char;
    }

    public float GetTotalPathTime()
    {
        int totalFrame = Mathf.FloorToInt( DistanceToHole / ((GameCont)CoreGameCont.Instance).CharSpeed);
        return totalFrame * Time.fixedDeltaTime;
    }


    /* ---- ITimelineListener funtions --- */
    public void OnTimelineEvent(string action, int beatIndex, float time)
    {
        Debug.Log("timeline charcontroller onEvent " + time + " beatIndex " + beatIndex);
        SongBeat beat = Timeline.Instance.GetBeat(beatIndex);
        //int column = int.Parse(id);
        CreateChar(beat.Column);
    }

    public string GetId()
    {
        return name;
    }
}
 