﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using TMPro;
using System.Globalization;
using System;

public class GameCont : HyperGameCont
{
    public GameObject CursorPrefab;
    public GameObject TaptextPrefab;
    [NonSerialized]
    public GameObject Taptext;
    [NonSerialized]
    public GameObject cursor;
    public override void Init()
    {
        base.Init();
        
    }

    Vector3 left;
    Vector3 right;
    public override void StartGame()
    {
        base.StartGame();
        if (((LevelCont)HyperLevelCont.Instance).CurrLevel < 2)
        {
            cursor = Instantiate(CursorPrefab, PlayerCont.Instance.transform.position + new Vector3(7, 0, 0), Quaternion.identity);
            cursor.transform.parent = CoreUiCont.Instance.transform;
            cursor.transform.localScale = Vector3.one * 4;
            //cursor.SetActive(false);
            Taptext = Instantiate(TaptextPrefab, Vector3.zero, Quaternion.identity, CoreUiCont.Instance.transform);
            Taptext.SetActive(false);

            left = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position - new Vector3(5, 0, 0));
            right = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position + new Vector3(5, 0, 0));
            //cursorGoLeft();
            LeanTween.move(cursor, right, .001f).setOnComplete(cursorGoLeft);
        }
    }
    private void cursorGoLeft()
    {
        LeanTween.move(cursor, left, .5f).setOnComplete(cursorGoRight);
    }

    private void cursorGoRight()
    {
        LeanTween.move(cursor, right, .5f).setOnComplete(cursorGoLeft);
    }

    public override void Update()
    {
        base.Update();


    }

}
