﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using DigitalRuby.Tween;
using System;
using System.ComponentModel.Design;
using Unity.Mathematics;

public class PlayerCont : HyperPlayerCont
{
    public float playerSpeed = 10; //currentSpeed
    public float maxSpeed = 50;
    public float leftRightMoveDistanceMultiplier = 4;
    public float leftRightMoveTime = 0.2f;
    public float followingBombDelayTime = 0.05f;
    public float moveSensitive=0.1f;
    private Vector2 startPoint;
    private Vector2 updatePoint;
    float _distancex;
    bool boomCont;
    [NonSerialized]
    public bool finishCont=false;
    private bool isFirstTouch = true;
    [NonSerialized]
    public float xDest = 0;
    [NonSerialized]
    public bool leftRightmMovable = true;
    [NonSerialized]
    public bool tutorial ;
    public override void Init()
    {
        base.Init();
        CoreInputCont.Instance.EventTouch.AddListener(onTouch);
        CoreInputCont.Instance.EventSwipe.AddListener(onSwipe);

        playerSpeed = ((LevelCont)HyperLevelCont.Instance).GetLevelSpeed(((LevelCont)HyperLevelCont.Instance).GetLevelIndex());
    }
    public override void Update()
    {
        base.Update();
        if (GameStatus == GameStatus.STARTGAME)
        {
            float _speed = Mathf.Clamp(playerSpeed, -maxSpeed, maxSpeed) * Time.deltaTime;
            gameObject.transform.parent.transform.Translate(0, 0, _speed);

        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            //if(!finishCont) gameObject.GetComponent<Bomber>().ExplodeBomb();
            Time.timeScale = 0.07f;
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            Time.timeScale = 1;
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            if (leftRightmMovable)
            {
                if (xDest > -leftRightMoveDistanceMultiplier * 2)
                {
                    //transform.Translate(Vector3.left * leftRightMoveMultiplier);
                    xDest = xDest - leftRightMoveDistanceMultiplier;
                    leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                }

            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            if (leftRightmMovable)
            {
                if (xDest < leftRightMoveDistanceMultiplier * 2)
                {
                    //transform.Translate(Vector3.right * leftRightMoveMultiplier);
                    xDest = xDest + leftRightMoveDistanceMultiplier;
                    leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                }

            }
        }

    }

    public IEnumerator setGameOverCam(int cam,float waitTime)
    {
        ((CameraCont)HyperCameraCont.Instance).SetNextCam(cam, waitTime);

        yield return new WaitForSeconds(waitTime+1);
        if ((cam+1)<3)
        {
            StartCoroutine(setGameOverCam(cam+1,2));
        }
    }
    public IEnumerator leftRightMoveEnable(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        if (!leftRightmMovable) leftRightmMovable = true;
    }
    private void onTouch(RoketGame.Touch _touch)
    {
        if (GameStatus == GameStatus.STARTGAME && !isFirstTouch)
        {
            if (_touch.Phase == TouchPhase.Began)
            {
                if (((LevelCont)HyperLevelCont.Instance).CurrLevel<2)
                {
                    ((GameCont)HyperGameCont.Instance).cursor.SetActive(false);
                    LeanTween.cancel(((GameCont)HyperGameCont.Instance).cursor);
                    if (tutorial)
                    {
                        if (HyperConfig.Instance.ShotEnemyTutorial)
                        {
                            HyperConfig.Instance.ShotEnemyTutorial = false;

                            ((GameCont)HyperGameCont.Instance).Taptext.SetActive(false);

                            LeanTween.cancel(((GameCont)HyperGameCont.Instance).Taptext);
                            if (Time.timeScale != 1) Time.timeScale = 1;
                        }
                    }
                }

                startPoint = _touch.GetPoint(0);
                boomCont = false;
            }
            if (_touch.Phase == TouchPhase.Moved)
            {
                updatePoint = _touch.GetPoint(1);
                _distancex = math.abs(updatePoint.x - startPoint.x);
                if (_distancex > 0.01f) boomCont = true;
                if (_distancex > moveSensitive)
                {
                    if (leftRightmMovable)
                    {
                        boomCont = true;
                        if (updatePoint.x - startPoint.x > 0)
                        {
                            if (xDest < leftRightMoveDistanceMultiplier * 2)
                            {
                                //transform.Translate(Vector3.right * leftRightMoveMultiplier);
                                xDest = xDest + leftRightMoveDistanceMultiplier;
                                leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                            }
                        }
                        else
                        {
                            if (xDest > -leftRightMoveDistanceMultiplier * 2)
                            {
                                //transform.Translate(Vector3.left * leftRightMoveMultiplier);
                                xDest = xDest - leftRightMoveDistanceMultiplier;
                                leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                            }
                        }
                        startPoint = _touch.GetPoint(1);
                    }
                }
            }
            if (_touch.Phase == TouchPhase.Ended)
            {
                if (!boomCont && !finishCont)
                {
                    gameObject.GetComponent<Bomber>().ExplodeBomb();
                }
            }
        }
    }
    private void onSwipe(RoketGame.Touch _touch)
    {
        if (GameStatus == GameStatus.STARTGAME && !isFirstTouch)
        {
            if (_touch.Phase == TouchPhase.Ended)
            {
                
               /* float _distance = Vector3.Distance(_touch.GetPoint(1),_touch.GetPoint(0));
                if (_distance > 0.1f)
                {
                    //if (!gameObject.GetComponent<Bomber>().jumping)
                    {


                        if (_touch.GetPoint(1).x - _touch.GetPoint(0).x > 0)
                        {
                            if (xDest < leftRightMoveDistanceMultiplier * 2)
                            {
                                //transform.Translate(Vector3.right * leftRightMoveMultiplier);
                                xDest = xDest + leftRightMoveDistanceMultiplier;
                                leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                            }
                        }
                        else
                        {
                            if (xDest > -leftRightMoveDistanceMultiplier * 2)
                            {
                                //transform.Translate(Vector3.left * leftRightMoveMultiplier);
                                xDest = xDest - leftRightMoveDistanceMultiplier;
                                leftRightMove(gameObject, xDest, followingBombDelayTime, leftRightMoveTime);
                            }
                        }
                    }
                }
                else
                {
                    gameObject.GetComponent<Bomber>().ExplodeBomb();
                }*/
            }
            
        }
        isFirstTouch = false;
    }
    public void leftRightMove(GameObject go,float newXPos,float delayTime,float speed)
    {
        LeanTween.cancel(GetComponent<Bomber>().activeTween);
        GetComponent<Bomber>().activeTween = LeanTween.moveLocalX(go, newXPos, speed).setEaseLinear().id;
        gameObject.GetComponent<Bomber>().leftRightFollowingBOMB(delayTime, speed, newXPos);
    }
}
