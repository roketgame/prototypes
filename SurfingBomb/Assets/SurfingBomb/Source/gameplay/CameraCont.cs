﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class CameraCont : HyperCameraCont
{

    private Vector3 defaultPos;

    public void SetNextCam(int stateNum, float animTime)
    {
        Transform item = transform.GetChild(stateNum);
        AnimTo(item.gameObject.name, animTime);
    }
    public override void Init()
    {
        base.Init();
        defaultPos = transform.localPosition;
    }
    public IEnumerator camShake(float duration,float amount)
    {
        float _time=0;
        while (_time < duration)
        {
            transform.localPosition = defaultPos + Random.insideUnitSphere * amount;
            _time += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        transform.localPosition = defaultPos;
    }
    public void camZoom(float num,bool add)
    {
        if (num > 3 && num < 9)
        {
            if (add) StartCoroutine(fieldOfViewCor(transform.GetChild(0).GetComponent<Camera>().fieldOfView, transform.GetChild(0).GetComponent<Camera>().fieldOfView + 2, .2f));
            else StartCoroutine(fieldOfViewCor(transform.GetChild(0).GetComponent<Camera>().fieldOfView, transform.GetChild(0).GetComponent<Camera>().fieldOfView - 2, .2f));

        }
        else if (num <= 3)
        {
            transform.GetChild(0).GetComponent<Camera>().fieldOfView = 60;
        }
    }
    IEnumerator fieldOfViewCor(float start,float end,float time)
    {
        float progress=0;
        while (progress <= 1.0)
        {
            transform.GetChild(0).GetComponent<Camera>().fieldOfView = Mathf.Lerp(start, end, progress);

            progress += Time.deltaTime / time;
            yield return null;
        }
        transform.GetChild(0).GetComponent<Camera>().fieldOfView = end;
    }
}
