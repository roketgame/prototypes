﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelItem
{
    public float TotalPrefab = 10;
    public List<GameObject> LinearPrefabs;
    public List<GameObject> RandomPrefabs;
    public GameObject LevelEndPrefab;
    public float speed;
}
public class LevelCont : HyperLevelCont
{
    public List<LevelItem> ListLevelPlatforms;
    public GameObject particlesParent;
    public GameObject explodeParticle;
    public GameObject wallExplodeParticle;
    public GameObject bombPrefab;
    public Transform freeBombRoot;
    //public float prefabCounter;
    public bool tramboline;
    [NonSerialized]
    public int Multiplier;
    public void spawnExpParticle(Vector3 pos)
    {
        CoreSceneObject item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(explodeParticle, pos, particlesParent.transform);
        item.GetComponent<ParticleSystem>().Play();
        StartCoroutine(deSpawnExpParitcle(item, 3));
    }
    public void spawnWallExpParticle(Vector3 pos)
    {
        CoreSceneObject item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(wallExplodeParticle, pos, particlesParent.transform);
        item.GetComponent<ParticleSystem>().Play();
        StartCoroutine(deSpawnExpParitcle(item, 3));
    }
    private IEnumerator deSpawnExpParitcle(CoreSceneObject item,float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        item.GetComponent<ParticleSystem>().Stop();
        CoreSceneCont.Instance.DeSpawnItem(item);
    }

    
    public override void Init()
    {
        base.Init();
        
        //initLightIntensity = mainLight.intensity;
    }
    public override void StartGame()
    {
        base.StartGame();/*
        for (int i = 0; i < 10; i++)
        {
            spawnBomb(new Vector3(-15,-15,-15));
        }*/
    }
    public Bomb spawnBomb(Vector3 pos)
    {
        
        Bomb item = CoreSceneCont.Instance.SpawnItem<Bomb>(bombPrefab, pos, freeBombRoot);
        if (tramboline)
        {
            item.transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            item.transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
            LeanTween.cancel(item.gameObject);
            //item.transform.localPosition = Vector3.zero;
            item.transform.localRotation = Quaternion.identity;
        }

        item.inLine = false;
        item.JumpProgress = 0;
        item.jumpCont = false;
        item.activeTween = 0;
        item.activeJumpCoroutine = null;
        item.heightProgress = 0;
        item.transform.GetChild(0).localPosition = Vector3.zero;
        item.transform.localScale = Vector3.one;
        return item;
    }
    public void deSpawnBomb(Bomb bomb,bool deSpawn=true)
    {
        bomb.transform.SetParent(freeBombRoot);
        if(deSpawn) CoreSceneCont.Instance.DeSpawnItem(bomb);
    }

    public override void InitLevel()
    {
        base.InitLevel();
        LevelTotalTime = 10 + CurrLevel * 5;

    }

    public override void Update()
    {
        base.Update();

        //skyMat.SetTextureOffset("_MainTex",new Vector2( (LevelTime/2.0f) / LevelTotalTime, 0)); // LevelTime = LevelTotalTime;
        // mainLight.intensity = Mathf.Clamp(initLightIntensity - ((LevelTime / 2.0f) * 0.4f) / LevelTotalTime, initLightIntensity-0.2f, initLightIntensity);
    }
    public override int GetLevelResult()
    {
        return 2; //oyun zamani doldugunda kazanir
    }
    public override void EndLevel(int levelResult)
    {
        base.EndLevel(levelResult);
        if (levelResult == 2)
        {
            SetLevelScore(50 * Multiplier);
            /*for (int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }*/

            
            PlayerCont.Instance.gameObject.GetComponentInChildren<Player_Anim>().PlayWinAnimation();
            StartCoroutine(setWinCam(3, 2));
        }
        Debug.Log("FINISH" + levelResult);
    }
    public int GetLevelIndex()
    {
        int indexOfArr = ListLevelPlatforms.Count - 1;

        if (CurrLevel - 1 < ListLevelPlatforms.Count)
            indexOfArr = CurrLevel - 1;

        return indexOfArr;
    }
    public List<GameObject> GetLevelPlatformLinearPrefabs(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].LinearPrefabs;
    }
    public List<GameObject> GetLevelPlatformRandomPrefabs(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].RandomPrefabs;
    }
    public GameObject GetLevelPlatformEndLevel(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].LevelEndPrefab;
    }
    public float GetLevelSpeed(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].speed;
    }
    public float GetTotalPrefab(int indexOfArr)
    {
        return ListLevelPlatforms[indexOfArr].TotalPrefab;

    }
    private IEnumerator setWinCam(int cam, float waitTime)
    {
        ((CameraCont)HyperCameraCont.Instance).SetNextCam(cam, waitTime);

        yield return new WaitForSeconds(waitTime + 1);
    }
}
