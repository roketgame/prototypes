﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class tutorialCont : HyperSceneObj
{
    public float timeScale;
    
    public override void StartGame()
    {
        base.StartGame();
        
    }
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.tag == "playerTemp")
        {
            if (HyperConfig.Instance.ShotEnemyTutorial)
            {
                collider.transform.parent.GetComponent<PlayerCont>().tutorial = true;

                LeanTween.cancel(((GameCont)HyperGameCont.Instance).cursor);
                //left = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position - new Vector3(5, 0, 0));
                //right = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position + new Vector3(5, 0, 0));
                ((GameCont)HyperGameCont.Instance).cursor.transform.position = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position + new Vector3(1, 0, 0));
                Time.timeScale = timeScale;
                ((GameCont)HyperGameCont.Instance).cursor.SetActive(true);

                ((GameCont)HyperGameCont.Instance).cursor.transform.GetChild(0).localScale = Vector3.one * 4;
                LTDescr d = LeanTween.scale(((GameCont)HyperGameCont.Instance).cursor.transform.GetChild(0).gameObject, Vector3.one * 2, 1);
                if (d != null)
                {
                    d.setIgnoreTimeScale(true);

                    d.setOnComplete(scaleCursor);
                }
                ((GameCont)HyperGameCont.Instance).Taptext.SetActive(true);
                ((GameCont)HyperGameCont.Instance).Taptext.transform.position = Camera.main.WorldToScreenPoint(PlayerCont.Instance.transform.position + new Vector3(1, 8, 0));
                ((GameCont)HyperGameCont.Instance).Taptext.transform.localScale = Vector3.one*2;
                LTDescr dd = LeanTween.scale(((GameCont)HyperGameCont.Instance).Taptext, Vector3.one*4, 1);
                if (dd != null)
                {
                    dd.setIgnoreTimeScale(true);

                    dd.setOnComplete(scaleTapText);
                }

            }
        }
    }
    private void scaleCursor()
    {

        LTDescr d = LeanTween.scale(((GameCont)HyperGameCont.Instance).cursor.transform.GetChild(0).gameObject, Vector3.one * 2, 1);

        if (d != null)
        {
            d.setIgnoreTimeScale(true);

            d.setOnComplete(scaleCursor);
        }
        ((GameCont)HyperGameCont.Instance).cursor.transform.GetChild(0).localScale = Vector3.one*4;

    }

    private void scaleTapText()
    {
        ((GameCont)HyperGameCont.Instance).Taptext.SetActive(true);
        ((GameCont)HyperGameCont.Instance).Taptext.transform.localScale = Vector3.one*2 ;
        LTDescr dd = LeanTween.scale(((GameCont)HyperGameCont.Instance).Taptext, Vector3.one*4, 1);
        if (dd != null)
        {
            dd.setIgnoreTimeScale(true);

            dd.setOnComplete(scaleTapText);
        }
    }
}
