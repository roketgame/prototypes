﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;

public class Bomb : HyperSceneObj
{
    public float heightProgress = 0;
    public IEnumerator activeJumpCoroutine;
    public float JumpProgress;/*{ get; private set; }*/
    public bool inLine;
    public bool jumpCont;
    public int activeTween;
    public int activeJumpTween;
    public int activeZTween;
    private Vector3 localDefaultPos;
    public override void Init()
    {
        base.Init();
        localDefaultPos = transform.localPosition;
        if (((LevelCont)HyperLevelCont.Instance).tramboline)
        {
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            transform.GetChild(0).GetChild(1).gameObject.SetActive(true);
        }
    }
    public void setVariables()
    {
        gameObject.SetActive(true);
        inLine = false;
        JumpProgress = 0;
        jumpCont = false;
        activeTween = 0;
        activeJumpTween = 0;
        activeJumpCoroutine = null;
        heightProgress = 0;
        transform.localPosition = localDefaultPos;
        transform.localScale = Vector3.one;
    }

    public void Jump(Vector3 destination, float maxHeight, float time,float _jumpProgress=0)
    {
        if (activeJumpCoroutine != null)
        {
            StopCoroutine(activeJumpCoroutine);
            activeJumpCoroutine = null;
            JumpProgress = _jumpProgress;
        }
        activeJumpCoroutine = JumpCoroutine(destination, maxHeight, time);
        StartCoroutine(activeJumpCoroutine);
    }
    public void stopCor()
    {
        if (activeJumpCoroutine!=null)
        {
            StopCoroutine(activeJumpCoroutine);
        }
    }
    public void jumpContDisable()
    {
        jumpCont = false;
        activeJumpTween = 0;
    }
    private IEnumerator JumpCoroutine(Vector3 destination, float maxHeight, float time)
    {
        destination.x = 0;
        //destination.z = 0;
        transform.localScale = Vector3.one;

        //LeanTween.pause(activeTween);
        var startPos = transform.GetChild(0).localPosition;
        while (JumpProgress <= 1.0)
        {
            JumpProgress += Time.deltaTime / time;
            var height = Mathf.Sin(Mathf.PI * JumpProgress) * maxHeight;
            if (height < 0f)
            {
                height = 0f;
            }
            heightProgress = maxHeight + (Mathf.Sin(Mathf.PI * 1) * maxHeight) - height;
            if (JumpProgress > 0.5f)
            {
                heightProgress = 0;
            }
            transform.GetChild(0).localPosition = Vector3.Lerp(startPos, destination, JumpProgress) + Vector3.up * height;
            yield return null;
        }
        //Debug.Log(gameObject.name);
        transform.GetChild(0).localPosition = destination;
        activeJumpCoroutine = null;
        JumpProgress = 0.0f;
    }

}
