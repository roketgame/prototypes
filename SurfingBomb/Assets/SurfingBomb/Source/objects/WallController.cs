﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallController : HyperSceneObj
{
    private GameObject player;
    bool active = true;
    RaycastHit _hit;
    private Vector3 localDefaultPos;
    public override void Init()
    {
        base.Init();
        localDefaultPos = transform.localPosition;
        player = GameObject.FindGameObjectWithTag("Player");
    }
    public void setVariables()
    {
        gameObject.SetActive(true);
        transform.localPosition = localDefaultPos;
    }

    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (active)
        {
            if (collider.gameObject.tag == "playerTemp")
            {
                Bomber bomber = collider.transform.parent.gameObject.GetComponent<Bomber>();
                if (bomber.gameObject)
                {
                    collider.transform.parent.gameObject.GetComponent<PlayerCont>().leftRightmMovable = false;
                    StartCoroutine(collider.transform.parent.gameObject.GetComponent<PlayerCont>().leftRightMoveEnable(1.5f));
                    bomber.gameObject.GetComponent<Player_Anim>().PlayFallingAnimation();
                    bomber.stopCor();
                    bomber.tweenCont = false;
                    bomber.activeJumpCoroutine = null;
                    LeanTween.moveLocalY(collider.gameObject, transform.localPosition.y + 2, 0.1f);
                    //LeanTween.moveLocalZ(bomber.gameObject,  3, 0.1f);
                    bomber.JumpProgress = 0.0f;
                    bomber.heightProgress = 0.0f;
                    StartCoroutine(bomber.getLineAllBomb(bomber.BombList, transform.localPosition.y + 0.505f,0.05f, 0, 0.05f,false));
                }
            }
        }
    }

    
    
    
    public override void OnTriggerExit(Collider collider)
    {
        base.OnTriggerExit(collider);

        if (!player.GetComponent<Bomber>().tweenCont)
        {

            /*if (collider.gameObject.tag == "bombTemp")
            {
                Bomb bomb = collider.transform.parent.gameObject.GetComponent<Bomb>();
                if (bomb.gameObject)
                {
                    if (bomb.inLine)
                    {
                        //bomb.activeTween = LeanTween.moveLocalY(bomb.gameObject, 0, 0.5f).id;
                        LeanTween.moveLocalY(bomb.transform.GetChild(0).gameObject, 0, 0.5f);
                        LeanTween.moveLocalZ(bomb.gameObject, player.GetComponent<Bomber>().BombList.IndexOf(bomb) * 2.6f - (player.GetComponent<Bomber>().BombList.Count - 1) * 2.6f, 0.2f);
                        bomb.inLine = false;
                    }

                }
            }
            else */if (collider.gameObject.tag == "playerTemp")
            {
                Bomber bomber = collider.transform.parent.gameObject.GetComponent<Bomber>();
                collider.transform.parent.gameObject.GetComponent<PlayerCont>().leftRightmMovable = true;
                if (bomber.activeJumpCoroutine == null)
                {
                    if (bomber.gameObject)
                    {

                        //if (bomber.BombList.Count > 0) bomber.activeTween = LeanTween.moveLocalY(bomber.gameObject, 2.95f, 0.4f).id;
                        if (bomber.BombList.Count > 0)
                        {
                            LeanTween.moveLocalY(bomber.transform.GetChild(0).gameObject, 2.95f, 0.4f)/*.setOnComplete(bomber.bombDown)*/;
                            StartCoroutine(bomber.getLineAllBomb(bomber.BombList, 0, 0.3f,0.1f, 0, true, 0.01f));
                        }
                        else LeanTween.moveLocalY(bomber.transform.GetChild(0).gameObject, 0.505f, 0.4f);
                        //if (bomber.BombList.Count==0)
                        {
                            bomber.jumping = false;
                        }
                        bomber.animSet();
                    }
                }
            }
        
        }
    }


    public override void Update()
    {
        base.Update();

        if (active)
        {
            Debug.DrawLine(transform.position + new Vector3(0, 1, 0), transform.position + Vector3.down, Color.white, 0.1f);

            if (!Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.down), out _hit, 2))
            {
                //gameObject.SetActive(false);
                active = false;
            }
        }
        

    }

}
