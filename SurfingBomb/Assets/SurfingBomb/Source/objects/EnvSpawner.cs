﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class EnvSpawner : HyperSceneObj
{
    public GameObject[] PrefabItem;
    private bool isStartSpawn;
    private float timer = 0;
    private int counter = 3;

    public float BuildLenght = 0;
    GameObject player;
    public int leftZRandMin=-15;
    public int leftZRandMax=15;
    public int rightZRandMin = -30;
    public int rightZRandMax = 30;
    public float leftSpawnYMin = 25;
    public float leftSpawnYMax = 25;
    public float rightSpawnYMin = 25;
    public float rightSpawnYMax = 25;
    public float rightSpawnX =25;
    public float leftSpawnX=25;
    public bool spawnStart=false;
    public bool leftSpawn;
    public bool rightSpawn;
    public int initSpawnCount=25;
    
    public override void Init()
    {
        base.Init();
        isStartSpawn = true;
        enabled = true;
        player = GameObject.FindGameObjectWithTag("Player");
        counter = (transform.childCount - 1) / 2;
        if (spawnStart)
        {
            for (int i = 0; i < initSpawnCount; i++)
            {
                spawnEnvItem();
            }
        }
    }

    public void spawnEnvItem()
    {
        counter++;
        
        int rand;
        int index;

        if (leftSpawn)
        {
            Vector3 pos = new Vector3(gameObject.transform.position.x, Random.Range(leftSpawnYMin, leftSpawnYMax), (counter * BuildLenght));
            rand = Random.Range(leftZRandMin, leftZRandMax);
            index = Random.Range(0, PrefabItem.Length);
            spawnItem(index, pos - new Vector3(leftSpawnX, 0, rand), transform);
        }

        if (rightSpawn)
        {
            Vector3 pos = new Vector3(gameObject.transform.position.x, Random.Range(rightSpawnYMin, rightSpawnYMax), (counter * BuildLenght));
            index = Random.Range(0, PrefabItem.Length);
            rand = Random.Range(rightZRandMin, rightZRandMax);
            spawnItem(index, pos + new Vector3(rightSpawnX, 0, rand), transform);
        }

    }
    
    private void spawnItem(int index ,Vector3 pos ,Transform parent)
    {
        CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(PrefabItem[index], pos, parent);
    }
}