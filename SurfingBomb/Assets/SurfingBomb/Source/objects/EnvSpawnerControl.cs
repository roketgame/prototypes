﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class EnvSpawnerControl : HyperSceneObj
{

    public bool IsSpawnOnContactEnter;
    public bool IsSpawnOnContactExit;
    public float DelayOfSpawnItem = 2f;
    public EnvSpawner envSpawner;
    public bool finished;

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (IsSpawnOnContactEnter) goSpawner();
    }

    public override void OnCollisionExit(Collision collision)
    {
        base.OnCollisionExit(collision);
        if (IsSpawnOnContactExit) goSpawner();
    }

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        if (IsSpawnOnContactEnter) goSpawner();
    }

    public override void OnTriggerExit(Collider other)
    {
        base.OnTriggerExit(other);
        if (IsSpawnOnContactExit) goSpawner();
    }

    private void goSpawner()
    {
        StartCoroutine(spawnCoroutine(DelayOfSpawnItem));
    }

    private IEnumerator spawnCoroutine(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (!finished)
        {
            envSpawner.spawnEnvItem();
        }
    }
}

