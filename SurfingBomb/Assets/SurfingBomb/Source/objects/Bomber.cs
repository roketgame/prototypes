﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;
using Unity.Mathematics;
using Random = UnityEngine.Random;

public class Bomber : HyperSceneObj
{
    public List<Bomb> BombList;
    public float playerJumpHeight = 20;
    public float playerJumpTime = 1;
    public float bombJumpHeight = 5;
    public float bombJumpTime = 0.9f;
    public float JumpProgress;
    public IEnumerator activeJumpCoroutine;
    public Transform bombsRoot;
    [NonSerialized]
    public int activeTween;
    [NonSerialized]
    public bool tweenCont;
    //[NonSerialized]
    public int bombCombo = 0;
    [NonSerialized]
    public bool jumping = false;
    [NonSerialized]
    public float heightProgress = 0;


    

    private bool jumpCont=true;

    /*public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);

        if (collider.gameObject.tag == "bombTemp")
        {
            Bomb bomb = collider.transform.parent.gameObject.GetComponent<Bomb>();
            if (bomb.gameObject)
            {
                if (!BombList.Contains(bomb))
                {
                    AddBomb(bomb);
                }
            }
        }
    }*/
    public void stopCor()
    {
        if(activeJumpCoroutine != null) StopCoroutine(activeJumpCoroutine);
    }
    public void AddBomb(Bomb bomb)
    {
        if (BombList.Count==0)
        {
            Bomb _bomb = ((LevelCont)HyperLevelCont.Instance).spawnBomb(bomb.transform.position);
            //CoreSceneObject item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(explodeParticle, pos, particlesParent.transform);
            gameObject.GetComponent<Player_Anim>().PlayRunJumpAnimation();
            BombList.Add(_bomb);
            _bomb.transform.SetParent(bombsRoot);
            LeanTween.moveLocalZ(_bomb.gameObject, 0, 0.15f);
            Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, _bomb.transform.position.y + 2.95f,0), 2, 0.2f,false,false);
            LeanTween.moveLocalX(_bomb.gameObject, gameObject.GetComponent<PlayerCont>().xDest, 0.05f);
            //LeanTween.moveLocalY(_bomb.gameObject, 0, 0.05f);
            /*if(_bomb.transform.position.y != 0)
            {
                _bomb.Jump(Vector3.zero, 0, bombJumpTime + (bombCombo - 1) * 0.2f,JumpProgress);      // havada bomba alma sonra yapılacak
            }*/
        }
        else
        {
            Bomb _bomb = ((LevelCont)HyperLevelCont.Instance).spawnBomb(bomb.transform.position);
            gameObject.GetComponent<Player_Anim>().PlayJumpAnimation(0.2f);
            BombList.Add(_bomb);
            _bomb.transform.SetParent(bombsRoot);
            for(int i = BombList.Count - 1; i >=0 ; i--)
            {
                LeanTween.moveLocalZ(BombList[i].gameObject, (BombList.Count-i-1) * (-2.6f), 0.15f);
            }
            Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, _bomb.transform.position.y + 2.95f, 0), 2, 0.2f,false, false);
            LeanTween.moveLocalX(_bomb.gameObject, gameObject.GetComponent<PlayerCont>().xDest, 0.05f);
            ((CameraCont)HyperCameraCont.Instance).camZoom(BombList.Count,true);
            //LeanTween.moveLocalY(_bomb.gameObject, 0, 0.05f);
        }
        bomb.gameObject.SetActive(false);
    }

    public void leftRightFollowingBOMB(float waitTime, float speed, float newPosX)
    {
        StartCoroutine(leftRightFollowingBOMBCoroutine(waitTime, speed, newPosX));
    }
    private IEnumerator leftRightFollowingBOMBCoroutine(float waitTime, float speed, float newPosX)
    {
        if (BombList.Count > 0)
        {
            for (int i = BombList.Count - 1; i >= 0; i--)
            {
                if (BombList.Count > i)
                {
                    LeanTween.cancel(BombList[i].activeTween);
                    BombList[i].activeTween = LeanTween.moveLocalX(BombList[i].gameObject, newPosX, speed).setEaseLinear().id;
                }
                else { Debug.Log("NULL"); continue; }
                yield return new WaitForSeconds(waitTime);
            }
        }
    }
    public IEnumerator getLineAllBomb(List<Bomb> bombList, float newPosY, float speed, float waitTime, float waitTime2, bool setposBombs, float setposBombsTime = 1)
    {
        if (bombList.Count > 0)
        {
            for (int i = bombList.Count - 1; i >= 0; i--)
            {
                yield return new WaitForSeconds(waitTime);
                if (setposBombs) setPosBombs(setposBombsTime);
                if (bombList.Count > i)
                {
                    bombList[i].stopCor();
                    bombList[i].activeJumpTween = LeanTween.moveLocalY(bombList[i].transform.GetChild(0).gameObject, newPosY, speed).setOnComplete(bombList[i].jumpContDisable).id;
                    bombList[i].activeJumpCoroutine = null;
                    bombList[i].JumpProgress = 0.0f;
                    bombList[i].heightProgress = 0.0f;
                    bombList[i].GetComponent<Bomb>().inLine = true;
                    bombList[i].GetComponent<Bomb>().jumpCont = true;
                    //LeanTween.moveLocalZ(bombList[i].gameObject, player.GetComponent<Bomber>().BombList.IndexOf(bombList[i]) * 2.6f - (player.GetComponent<Bomber>().BombList.Count - 1) * 2.6f, 0.01f);
                }
                else { Debug.Log("NULL"); continue;}
                yield return new WaitForSeconds(waitTime2);
            }
        }
    }
    public void setPosBombs(float time=1)
    {
        for (int i = BombList.Count - 1; i >= 0; i--)
        {
            LeanTween.cancel(BombList[i].activeZTween);
            BombList[i].activeZTween = LeanTween.moveLocalZ(BombList[i].gameObject, (BombList.Count - 1 - i) * (-2.6f), time).id;
            //LeanTween.moveLocalX(BombList[i].gameObject, transform.localPosition.x, 0.2f);
        }
    }
    public void bombHitBOOM(Bomb bomb,bool levelEnd=false)
    {
        StartCoroutine(((CameraCont)HyperCameraCont.Instance).camShake(0.2f,0.1f));
        //Time.timeScale = 0.07f;
        MobileVibration.Vibrate(MobileVibration.Level.LOW);

        if (!((LevelCont)HyperLevelCont.Instance).tramboline) ((LevelCont)HyperLevelCont.Instance).spawnWallExpParticle(bomb.transform.position+new Vector3(0,2,0));

        ((LevelCont)HyperLevelCont.Instance).deSpawnBomb(bomb);
        //BombList[BombList.Count - 1].gameObject.SetActive(false);
        BombList.RemoveAt(BombList.Count - 1);
        setPosBombs();
        ((CameraCont)HyperCameraCont.Instance).camZoom(BombList.Count,false);
        /*if (BombList.Count == 0)
        {
            LeanTween.moveLocalY(gameObject, 0.505f, 0.1f);
        }*/
        //if (!jumping)
        if (!levelEnd)
        {
            gameObject.GetComponent<Player_Anim>().PlayJumpAnimation(0);
            if (BombList.Count == 0)
            {
                
                Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, 0.505f, -gameObject.GetComponent<PlayerCont>().playerSpeed), (heightProgress + transform.GetChild(0).position.y / 2.0f + playerJumpHeight/2), playerJumpTime);
            }
            else
            {
                
                Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, 2.95f, -gameObject.GetComponent<PlayerCont>().playerSpeed),
                    (heightProgress + transform.GetChild(0).position.y / 2.0f + playerJumpHeight/2),
                    playerJumpTime);
               
            }
        }
        bombDown();
    }
    public void ExplodeBomb()
    {
        if (BombList.Count>0)
        {
            if (jumpCont)
            {

                StartCoroutine(BombExpDelay(BombList[BombList.Count-1],0.15f));
            }
        }
        else
        {
            // bomba yook :(
        }
    }

    public AnimationCurve test2;
    public IEnumerator BombExpDelay(Bomb expBomb,float waittime,bool dontJump=false)
    {
        MobileVibration.Vibrate(MobileVibration.Level.LOW);
        jumpCont = false;
        if (!((LevelCont)HyperLevelCont.Instance).tramboline) LeanTween.scale(expBomb.gameObject, new Vector3(1.4f, 1.0f, 1.4f), 0.12f);
        else { LeanTween.scaleY(expBomb.gameObject, 1.3f, 0.13f).setEase(test2);
         LeanTween.moveLocalY(transform.GetChild(0).gameObject, 3.5f, 0.12f).setEase(test2);
        }
        yield return new WaitForSeconds(waittime);
        //BombList[BombList.Count - 1].GetComponent<Bomb_Anim>().PlayBOOMAnimation();
        StartCoroutine(((CameraCont)HyperCameraCont.Instance).camShake(0.1f,0.1f));
        if(heightProgress!=0 || bombCombo == 0) bombCombo++;
        if (!jumping) bombCombo = 1;

        if (!dontJump)
        {
            gameObject.GetComponent<Player_Anim>().PlayJumpAnimation(0);

            if (BombList.Count == 1)
            {
                Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, 0.505f, 0), heightProgress + transform.GetChild(0).position.y / 2.0f + playerJumpHeight, playerJumpTime + (bombCombo-1) * 0.2f);
            }
            else
            {
                Jump(new Vector3(gameObject.GetComponent<PlayerCont>().xDest, 2.95f, 0),
                    heightProgress + transform.GetChild(0).position.y / 2.0f + playerJumpHeight,
                    playerJumpTime + (bombCombo -1) * 0.2f);
                StartCoroutine(jumpFollowingBOMB(0.05f));

            }
        }
        

        if (!((LevelCont)HyperLevelCont.Instance).tramboline)
        {
            ((LevelCont)HyperLevelCont.Instance).deSpawnBomb(expBomb);
            ((LevelCont)HyperLevelCont.Instance).spawnExpParticle(expBomb.transform.GetChild(0).position + new Vector3(0, 2, 0));
        }
        else
        {
            int dir = 1;
            if (Random.Range(0, 2)<1)
             {
                dir = -1;
            }
            //Bomb bomb = BombList[BombList.Count - 1];


            BombList[BombList.Count - 1].stopCor();

            LeanTween.moveLocalX(expBomb.gameObject, expBomb.transform.localPosition.x + 15 *dir,0.5f);
            LeanTween.moveLocalY(expBomb.gameObject, expBomb.transform.localPosition.y - 5,2f).setEase(testCurve);
            //LeanTween.moveLocalZ(bomb.gameObject, 30,2f);
            //LeanTween.moveLocal(bomb.gameObject, new Vector3(bomb.transform.localPosition.x + 7 * dir, 10, 20), 1f).setEase(testCurve);
            LeanTween.rotateLocal(expBomb.gameObject, new Vector3(30, -40 * dir, -120 * dir), .5f).setOnComplete((() => completeTrambolineTween(expBomb)));
            LeanTween.scaleY(expBomb.gameObject, 5, 1f).setEaseInBounce();
        }
        //BombList[BombList.Count - 1].Jump(new Vector3(BombList[BombList.Count - 1].transform.localPosition.x, 0,0/*(BombList.Count - 1) * 2.6f - (BombList.Count - 1) * 2.6f*/), BombList[BombList.Count - 1].heightProgress + transform.GetChild(0).position.y / 2.0f + bombJumpHeight, bombJumpTime);
        //BombList[BombList.Count - 1].gameObject.SetActive(false);
        if(BombList.IndexOf(expBomb) != -1) BombList.RemoveAt(BombList.IndexOf(expBomb));

        jumpCont = true;
        ((CameraCont)HyperCameraCont.Instance).camZoom(BombList.Count,false);
        setPosBombs(0.1f);
    }
    public AnimationCurve testCurve;
    private void completeTrambolineTween(Bomb bomb)
    {

        ((LevelCont)HyperLevelCont.Instance).deSpawnBomb(bomb);

    }
    public void bombDown()
    {
        StartCoroutine(getLineAllBomb(BombList, 0, 0.3f, 0.1f, 0, true, 0.01f));
    }
    public IEnumerator jumpFollowingBOMB(float waitTime)
    {
        if (BombList.Count > 0)
        {
            yield return new WaitForSeconds(0.1f);
            for (int i = BombList.Count - 1; i >= 0; i--)
            {
                if (BombList.Count > i)
                {
                    if (BombList[i].activeJumpTween != 0)
                    {
                        BombList[i].activeJumpTween = 0;
                        LeanTween.cancel(BombList[i].gameObject); 
                    }
                    BombList[i].Jump(new Vector3(BombList[i].transform.localPosition.x, 0, 0), BombList[i].heightProgress + transform.GetChild(0).position.y / 2.0f + bombJumpHeight, bombJumpTime + (bombCombo-1) * 0.2f);
                    //if (!BombList[i].jumpCont) BombList[i].Jump(new Vector3(BombList[i].transform.localPosition.x, 0,0 /*i * 2.6f - (BombList.Count - 1) * 2.6f*/), BombList[i].heightProgress + transform.GetChild(0).position.y / 2.0f + bombJumpHeight, bombJumpTime);
                    yield return new WaitForSeconds(waitTime);
                
                }
                else { Debug.Log("NULL"); continue; }
            }
            yield return new WaitForSeconds(0.7f);
        }
    }
    
    public void Jump(Vector3 destination, float maxHeight, float time, bool notAnimation = false, bool posZChange = true)
    {
        if (activeJumpCoroutine != null)
        {
            StopCoroutine(activeJumpCoroutine);
            activeJumpCoroutine = null;
            JumpProgress = 0.0f;
        }
        activeJumpCoroutine = JumpCoroutine(destination, maxHeight, time , notAnimation, posZChange);
        StartCoroutine(activeJumpCoroutine);
    }

    private IEnumerator JumpCoroutine(Vector3 destination, float maxHeight, float time, bool notAnimation = false, bool posZChange=true)
    {
        destination.x = 0;
        jumping = true;
        var startPos = transform.GetChild(0).localPosition;
        //LeanTween.pause(activeTween);
        tweenCont = true;
        while (JumpProgress <= 1.0)
        {
            
            if (BombList.Count == 0)
            {
                destination.y = 0.505f;
            }
            else
            {
                destination.y = 2.95f;
            }
            if (posZChange)
            {
                //if (destination.z != 0 && JumpProgress > 0.5f) destination.z = Mathf.Lerp(destination.z, 0, (JumpProgress - 0.5f) / 0.5f); // hitBlock
                if (destination.z != 0 && JumpProgress > 0.10f) destination.z = Mathf.Lerp(destination.z, 0, (JumpProgress - 0.10f) / 0.9f); // hitBlock
                if (JumpProgress > 0.5f) gameObject.GetComponent<Player_Anim>().PlayFallingAnimation();
            }
            

            JumpProgress += Time.deltaTime / time;
            var height = Mathf.Sin(Mathf.PI * JumpProgress) * maxHeight;
            if (height < 0f)
            {
                height = 0f;
            }
            heightProgress = maxHeight + (Mathf.Sin(Mathf.PI * 1) * maxHeight) - height;
            if (JumpProgress > 0.5f)
            {
                heightProgress = 0;
            }
            transform.GetChild(0).localPosition = Vector3.Lerp(startPos, destination, JumpProgress) + Vector3.up * height;
            yield return null;
        }
        //gameObject.GetComponent<Player_Anim>().PlaySurfAnimation();
        transform.GetChild(0).localPosition = destination;
        jumping = false;
        activeJumpCoroutine = null;
        JumpProgress = 0.0f;
        tweenCont = false ;
        if(!notAnimation) anim();

    }

    public void animSet()
    {
        Invoke("anim", .5f);
    }
    private void anim()
    {
        if (BombList.Count == 0)
        {
            gameObject.GetComponent<Player_Anim>().PlayRunAnimation();
        }
        else
        {
            gameObject.GetComponent<Player_Anim>().PlaySurfAnimation();
        }
    }
}
