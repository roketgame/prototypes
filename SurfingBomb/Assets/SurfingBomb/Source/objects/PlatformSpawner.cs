﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using System;
using Random = UnityEngine.Random;

public class PlatformSpawner : HyperSceneObj
        
{
    
    public GameObject platformPrefabWithoutBlocks;
    private bool isStartSpawn;
    private float timer = 0;
    private int counter=3;
    public float Platformlength = 0;
    public bool platformsWithoutBlocks = false;
    private int platformCounter = 0;
    public EnvSpawnerControl[] envSpawnerControl;
    //public int levelDiff = 0;
    public int initSpawnCount = 6;

    float totalPrefab;
    List<GameObject> randomPrefabs;
    List<GameObject> linearPrefabs;
    GameObject endLevel;
    int levelIndex;
    
    public override void Init()
    {
        base.Init();

        isStartSpawn = true;
        enabled = true;

        counter = transform.childCount - 1;
        levelIndex = ((LevelCont)HyperLevelCont.Instance).GetLevelIndex();
        randomPrefabs = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformRandomPrefabs(levelIndex);
        linearPrefabs = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformLinearPrefabs(levelIndex);
        endLevel = ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformEndLevel(levelIndex);

        for (int i=0;i< initSpawnCount; i++)
        {
            spawnItem();
        }
    }

    float totalRange;
    public void spawnItem()
    {
        
        if (platformCounter >= ((LevelCont)HyperLevelCont.Instance).GetTotalPrefab(levelIndex))
        {
            spawnFinishItem();
            return;
        }
        
        /*
        float jumpRange;
        if (Random.Range(0,5) >= 1)
        {
            jumpRange = Random.Range(5.0f,10.0f);
            totalRange += jumpRange;
        }
        */


        
        counter++;
        Vector3 pos = new Vector3(gameObject.transform.position.x, 0, 20 + (counter * Platformlength) + totalRange);
        CoreSceneObject item;
        if (platformsWithoutBlocks)
        {
             item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(platformPrefabWithoutBlocks, pos, transform);
        }
        else
        {
            //linear
            if (platformCounter < ((LevelCont)HyperLevelCont.Instance).GetLevelPlatformLinearPrefabs(levelIndex).Count)
            {
               
                item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(linearPrefabs[platformCounter], pos, transform);
                
            }
            else //Random
            {
                int platform = Random.Range(0, (randomPrefabs.Count));
                item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(randomPrefabs[platform], pos, transform);
            }
/*

            for (int i = 0; i < item.transform.GetChild(0).childCount; i++)
            {
                if (item.transform.GetChild(0).GetChild(i).tag == "wallController")
                {
                    item.transform.GetChild(0).GetChild(i).GetComponentInChildren<WallController>().setVariables();
                }
                else if (item.transform.GetChild(0).GetChild(i).tag == "Bomb")
                {
                    item.transform.GetChild(0).GetChild(i).GetComponentInChildren<Bomb>().setVariables();
                }
                else if (item.transform.GetChild(0).GetChild(i).tag == "Block" || item.transform.GetChild(0).GetChild(i).tag == "emptyBlock")
                {
                    item.transform.GetChild(0).GetChild(i).GetComponentInChildren<Block>().setVariables();
                }
            }*/
        }
        platformCounter++;
    }


    bool finished;
    public void spawnFinishItem()
    {
        if (!finished)
        {
            finished = true;
            platformCounter++;
            counter++;
            Vector3 pos = new Vector3(gameObject.transform.position.x, 0, 20 + (counter * Platformlength) + totalRange);
            CoreSceneObject item = CoreSceneCont.Instance.SpawnItem<CoreSceneObject>(endLevel, pos, transform);
            foreach (EnvSpawnerControl es in envSpawnerControl)
            {
                es.finished = true;
            }
        }

    }
}
