﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.UIElements;

public class Block : HyperSceneObj
{
    public bool emptyBlock;
    private GameObject player;
    private bool firstBomb=true;
    private LayerMask mask;
    private bool falling;
    private Vector3 localDefaultPos;
    public override void Init()
    {
        base.Init();
        localDefaultPos = transform.localPosition;
        mask = LayerMask.GetMask("block");
        player = GameObject.FindGameObjectWithTag("Player");
    }

    public void setVariables()
    {
        //gameObject.SetActive(true);
        //transform.localPosition = localDefaultPos;
    }

    private RaycastHit _hit;
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (!emptyBlock)
        {
            if (collider.gameObject.tag == "playerTemp")
            {
                PlayerCont player = collider.transform.parent.gameObject.GetComponent<PlayerCont>();
                if (player.gameObject)
                {
                    Bomber bomber = player.GetComponent<Bomber>();
                    player.GetComponent<Player_Anim>().playLoseAnimation();
                    for (int i = bomber.BombList.Count - 1; i >= 0; i--) 
                    {
                        player.GetComponent<Bomber>().bombHitBOOM(bomber.BombList[i], true);
                    }
                    LevelCont.Instance.EndLevel(1); // fail
                    StartCoroutine(player.setGameOverCam(2, 1));
                }
            }
            else if (collider.gameObject.tag == "bombTemp")
            {
                if (player.GetComponent<Bomber>().BombList[player.GetComponent<Bomber>().BombList.Count -1].gameObject == collider.transform.parent.gameObject)
                {
                    Debug.DrawLine(transform.position + new Vector3(0, 1, 0), transform.position + new Vector3(0, 0.7f, 0) + Vector3.up * 15, Color.blue, 2.5f);

                    if (firstBomb)
                    {
                        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), Vector3.up, out _hit, 15, mask))
                        {
                            if (_hit.collider)
                            {
                                _hit.collider.GetComponent<Block>().fallingBlock();
                            }
                        }
                    }
                    Bomb bomb = collider.transform.parent.GetComponent<Bomb>();
                    gameObject.SetActive(false);
                    player.GetComponent<Bomber>().bombHitBOOM(bomb);
                }
            }
        }
        

    }
    public void fallingBlock()
    {
        firstBomb = false;
        falling = true;
        if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), Vector3.up, out _hit, 15, mask))
        {
            if (_hit.collider)
            {
                _hit.collider.GetComponent<Block>().fallingBlock();
            }
        }
    }
    public override void Update()
    {
        base.Update();
        if (falling)
        {

            /* Debug.DrawLine(transform.position + new Vector3(0, 1, 0), transform.position  + Vector3.down, Color.white, 0.1f);

        if (!Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.down), out _hit, 2))*/
            //LayerMask mask = LayerMask.GetMask("firstShot");

            Debug.DrawLine(transform.position + new Vector3(0,1,0), transform.position  + Vector3.down/2, Color.white, 2.5f);

            if (Physics.Raycast(transform.position + new Vector3(0, 1, 0), transform.TransformDirection(Vector3.down), out _hit, 1f))
            {
                if (_hit.collider.tag == "Block")
                {
                    Block bottomBlock = _hit.collider.GetComponent<Block>();
                    if (bottomBlock)
                    {

                        //Debug.Log(bottomBlock.name);
                    }
                }
                else if (_hit.collider.tag == "WillDestroy" /*|| _hit.collider.tag == "emptyBlock"*/)
                {
                    GameObject go = _hit.collider.gameObject;
                    if (go)
                    {
                        //Debug.Log("asd" + go.name);
                    }
                }
                else
                {
                    if (emptyBlock) gameObject.SetActive(false);
                    else transform.Translate(new Vector3(0, -2, 0));
                    //LeanTween.moveLocalY(gameObject, gameObject.transform.localPosition.y - 4, 0.05f);

                }
            }
            else
            {
                if (emptyBlock) gameObject.SetActive(false);
                else transform.Translate(new Vector3(0, -2, 0));
                // LeanTween.moveLocalY(gameObject, gameObject.transform.localPosition.y - 4, 0.05f);

            }

            if(gameObject.transform.localPosition.y < -1) gameObject.SetActive(false);
        }
    }
}
