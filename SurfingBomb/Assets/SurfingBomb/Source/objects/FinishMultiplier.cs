﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class FinishMultiplier : HyperSceneObj
{
    public int Multiplier;
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.tag == "playerTemp")
        {
            ((LevelCont)HyperLevelCont.Instance).Multiplier = Multiplier;

            Win();
        }
    }
    private void Win()
    {
        LevelCont.Instance.EndLevel(2);
    }
}
