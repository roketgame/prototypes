﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bomberTemp : HyperSceneObj
{
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);

        if (collider.gameObject.tag == "bombTemp")
        {
            Bomb bomb = collider.transform.parent.gameObject.GetComponent<Bomb>();
            if (bomb.gameObject)
            {
                if (!transform.parent.GetComponent<Bomber>().BombList.Contains(bomb))
                {
                    transform.parent.GetComponent<Bomber>().AddBomb(bomb);
                }
            }
        }
    }
}
