﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class FinishObj : HyperSceneObj
{
    public bool ExpBomb;
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if (collider.gameObject.tag == "playerTemp")
        {
            if (ExpBomb)
            {
                Bomber bomber = collider.transform.parent.GetComponent<Bomber>();
                int bombSize = bomber.BombList.Count;
                if (bombSize!=0)
                {
                    StartCoroutine(ExpBombFinal(bomber, 0.01f));
                    int height = bombSize > 4 ? 4 : bombSize;
                    int time = bombSize > 11 ? 11 : bombSize;
                    bomber.gameObject.GetComponent<Player_Anim>().playFinishFallingAnimation();
                    bomber.Jump(new Vector3(bomber.gameObject.GetComponent<PlayerCont>().xDest, 0.505f, 0), 7 * height, 1 + 0.5f * time, true);
                    bomber.gameObject.GetComponent<PlayerCont>().playerSpeed = 40;
                }
            }
            else collider.transform.parent.GetComponent<PlayerCont>().finishCont = true;
        }
    }
    private IEnumerator ExpBombFinal(Bomber bomber, float waitTime)
    {
        for (int i = bomber.BombList.Count - 1; i >= 0; i--)
        {
            StartCoroutine(bomber.BombExpDelay(bomber.BombList[i], 0, true));
            yield return new WaitForSeconds(waitTime);
        }
    }
}
