﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Bomb_Anim : HyperSceneObj
{

    public Animator anim;
    private static readonly int Start = Animator.StringToHash("Start");

    public override void StartGame()
    {
        base.StartGame();
        anim.SetBool(Start, true);
    }
    public void PlayBOOMAnimation()
    {
        anim.Play("BOOM");
    }
}