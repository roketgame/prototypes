﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Player_Anim : HyperSceneObj
{


    public Animator anim;
    //bool
    private static readonly int Start = Animator.StringToHash("Start");
    private static readonly int Jump = Animator.StringToHash("Jump");
    //trigger
    private static readonly int RunJump = Animator.StringToHash("RunJump");
    private static readonly int Surfing = Animator.StringToHash("Surfing");
    private static readonly int Run = Animator.StringToHash("Run");
    private static readonly int Falling = Animator.StringToHash("Falling");

    public override void StartGame()
    {
        base.StartGame();
        anim.SetBool(Start, true);
    }
    public void PlayDeadAnimation()
    {
        anim.Play("Dead");
    }
    public void PlayWinAnimation()
    {
        anim.Play("Win");
    }
    public void PlayJumpAnimation(float speed)
    {

        anim.Play("Jump",0,0);
        anim.SetBool(Jump, true);
        anim.SetBool(RunJump, false);
        anim.SetBool(Run, false);
        anim.SetBool(Surfing, false);
        anim.SetBool(Falling, false);
    }
    public void PlayRunJumpAnimation()
    {
        //anim.Play("RunJump");
        anim.SetBool(Jump, false);
        anim.SetBool(RunJump, true);
        anim.SetBool(Run, false);
        anim.SetBool(Surfing, false);
        anim.SetBool(Falling, false);
    }
    public void PlayRunAnimation()
    {
        //anim.Play("Run");
        anim.SetBool(Run, true);
        anim.SetBool(Jump, false);
        anim.SetBool(RunJump, false);
        anim.SetBool(Surfing, false);
        anim.SetBool(Falling, false);
    }
    public void PlaySurfAnimation()
    {
        //anim.Play("Surf",0,0);
        anim.SetBool(Surfing, true);
        anim.SetBool(Run, false);
        anim.SetBool(Jump, false);
        anim.SetBool(RunJump, false);
        anim.SetBool(Falling, false);
    }
    public void PlayFallingAnimation()
    {
        // anim.Play("Falling");
        anim.SetBool(Falling, true);
        anim.SetBool(Surfing, false);
        anim.SetBool(Run, false);
        anim.SetBool(Jump, false);
        anim.SetBool(RunJump, false);
    }
    public void playFinishFallingAnimation()
    {
        anim.Play("FinishFall");
    }
    public void playLoseAnimation()
    {
        LeanTween.moveLocalY(transform.GetChild(0).gameObject, 0, 1);
        anim.Play("Lose");
        
    }
}