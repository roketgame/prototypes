﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGameOver : HyperPageGameOver
{
    public override void Init()
    {
        base.Init();
        ((LevelCont)  HyperLevelCont.Instance).EventPlayComp.AddListener(gameOverAnimCompleted);
    }


    public override void SetGameResult(int _result)
    {
        base.SetGameResult(_result);

       
        if (_result == 1)
            SetText("TxtResult", "YOU FAILED");
        if (_result == 2)
            SetText("TxtResult", "LEVEL COMPLETED!");

       

    }

    private void gameOverAnimCompleted()
    {

    }




}
