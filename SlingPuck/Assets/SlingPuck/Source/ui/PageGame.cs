﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGame : HyperPageGame
{
    public GameObject[] CountDownTexts;
    public float countDownTotalTime = 2f;


    public override void Init()
    {
        base.Init();
        foreach (GameObject go in CountDownTexts)
        {
            go.SetActive(false);
        }

     





    }

    public override void Update()
    {
        base.Update();



            
    }


    public override void Open()
    {
        base.Open();

       
    }

    public void SetMatchScore(int scorePlayer, int scoreEnemy)
    {
        SetText("PlayerScore", scorePlayer.ToString());
        SetText("EnemyScore", scoreEnemy.ToString());
    }


    protected override void onBtnStartClicked(TouchUI _touch)
    {
        BtnStart.gameObject.SetActive(false);
        if (((GameCont)CoreGameCont.Instance).TutorialIsCompleted)
        {
            StartCountDown();
        }
        else
        {
            EventStart.Invoke();
        }
       
    }

    private int currCountDownIndex = 0;
    public void StartCountDown()
    {
        int total = CountDownTexts.Length;
      

        int currCountDownIndex = 0;
        GameObject currCountDownItem = CountDownTexts[currCountDownIndex];

        setItemTween(currCountDownItem, countDownTotalTime/ CountDownTexts.Length);


    }

    private void setItemTween(GameObject item, float time)
    {
        item.GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);
        //item.GetComponent<RectTransform>().LeanScale(new Vector3(1, 1, 1), time).setEase(LeanTweenType.easeOutElastic);
        item.SetActive(true);
        //LeanTween.locals(item.gameObject, new Vector3(1, 1, 1), time).setEase(LeanTweenType.easeOutElastic);
        
           

            TextMeshProUGUI textMesh = item.GetComponent<TextMeshProUGUI>();
            Color color = textMesh.color;
            color.a = 0;
            textMesh.color = color;
            LeanTween.cancel(textMesh.gameObject);
            var _color = textMesh.color;
            var _tween = LeanTween
                .value(textMesh.gameObject, 0f, 1f, time)
                .setOnUpdate((float _value) =>
                {
                    textMesh.rectTransform.localScale = new Vector3(_value, _value, _value);
                    _color.a = _value;
                    textMesh.color = _color;
                }).setEase(LeanTweenType.easeOutBack).setOnComplete(() =>
               {
                       item.SetActive(false);
                   nextCountDown();
               });
            //}
        
    }

    private void nextCountDown()
    {
        if(currCountDownIndex < CountDownTexts.Length-1)
        {
            currCountDownIndex++;
            GameObject currCountDownItem = CountDownTexts[currCountDownIndex];

            setItemTween(currCountDownItem, countDownTotalTime / CountDownTexts.Length);
        }
        else
        {
            EventStart.Invoke();
        }
        
    }


}
