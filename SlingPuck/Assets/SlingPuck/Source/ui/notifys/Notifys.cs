﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using TMPro;

public class Notifys : CoreCont<Notifys>
{
    private Dictionary<string, Notify> listItems;
    public override void Init()
    {
        base.Init();

        listItems = new Dictionary<string, Notify>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Notify item = transform.GetChild(i).GetComponent<Notify>();
            if(item)
            {
                listItems.Add(item.gameObject.name, item);
                item.gameObject.SetActive(false);
            }
        }
    }

    public void OpenNotify(string _name, Vector3 _screenPos)
    {
        Notify item = GetItem(_name);
        if(item)
        {
            item.gameObject.SetActive(true);

            Vector3 pos = Camera.main.WorldToScreenPoint(_screenPos);
            item.GetComponent<RectTransform>().localPosition = pos;

            setItemTween(item, 0.4f);

            StartCoroutine(closeNotify(item, 0.7f)); ;
           
        }
    }

    private void setItemTween(Notify item, float time)
    {
        item.GetComponent<RectTransform>().localScale = new Vector3(0, 0, 0);
        item.GetComponent<RectTransform>().eulerAngles = new Vector3(0, 0, Random.Range(-20,20));

        LeanTween.scale(item.gameObject, new Vector3(1, 1, 1), time).setEase(LeanTweenType.easeOutElastic);
        LeanTween.rotateZ(item.gameObject, 0, time).setEase(LeanTweenType.easeOutElastic);
        for (int i = 0; i < item.transform.childCount; i++)
        {
            GameObject child = item.transform.GetChild(i).gameObject;
           

            TextMeshProUGUI textMesh = child.GetComponent<TextMeshProUGUI>();
            if(textMesh)
            {
                Color color = textMesh.color;
                color.a = 0;
                textMesh.color = color;
                LeanTween.cancel(textMesh.gameObject);
                var _color = textMesh.color;
                var _tween = LeanTween
                    .value(textMesh.gameObject, 0f, 1f, time)
                    .setOnUpdate((float _value) =>
                    {
                        _color.a = _value;
                        textMesh.color = _color;
                    }).setEase(LeanTweenType.easeOutElastic);
            }
    
        }
    }
    private IEnumerator closeNotify(Notify item, float time)
    {
        yield return new WaitForSeconds(time);
        item.gameObject.SetActive(false);
        LeanTween.cancel(item.gameObject);

    }




    public Notify GetItem(string _name)
    {
        Notify item = null;
        if(listItems.ContainsKey(_name))
        {
            item = listItems[_name];
        }
        return item;
    }

    
}
