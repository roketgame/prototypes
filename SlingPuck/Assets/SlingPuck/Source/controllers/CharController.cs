﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using Random = UnityEngine.Random;
using TMPro;


public struct ItemElement
{
    public int Team;
    public Char Item;
}
public class CharController : CoreCont<CharController>
{
    //private List<Char> _playerCharList = new List<Char>();
    //private List<Char> _enemyCharList = new List<Char>();
    private List<ItemElement> itemList = new List<ItemElement>(); //0->player, 1->enemy
    //private Vector3 _enemyThrowPoint;

    public GameObject CharPrefab;

    public List<Transform> playerPoints;
    public List<Transform> enemyPoints;


    public int CharCount;


    public override void Init()
    {
        //base.Init(); //createChar animasyonlarnin bitmesini bekliyoruz
    }
    public void CreateChars()
    {

        CharCount = (2 + ((int)Math.Floor(LevelCont.Instance.CurrLevel / 4f) * 1)) * 2;
        Instance.CharCount = Mathf.Clamp(CharCount, 2, 8);

        for (int i = 0; i < CharCount; i++)
        {
            Char item = null;
            int team = -1;
            ItemElement elem;
            if (i % 2 == 0) //player
            {
                item = createChar(playerPoints);
                team = 0;
            }
            else //enemy
            {
                item = createChar(enemyPoints);
                team = 1;
            }

            elem.Item = item;
            elem.Team = team;
            itemList.Add(elem);
            setTeam(item, team);




        }


        StartCoroutine(spawnAnimOnCompleted(0.6f));
    }

    public List<Char> GetTeamItems(int team)
    {
        List<Char> list = new List<Char>();
       foreach(ItemElement elem in itemList.FindAll(e => e.Team == team))
        {
            list.Add(elem.Item);
        }
        return list;
    }
    public void setTeam(Char item, int team)
    {
        item.SetTeam(team);
        int foundIndex = itemList.FindIndex(e => e.Item == item);
        if(foundIndex > -1)
        {
            ItemElement elem = itemList[foundIndex];
            elem.Team = team;
            itemList[foundIndex] = elem;
        }

        int scorePlayer = GetTeamItems(1).Count;
        int scoreEnemy = GetTeamItems(0).Count;
        if (((PageGame)CoreUiCont.Instance.GetPageByClass<PageGame>()))
        ((PageGame)CoreUiCont.Instance.GetPageByClass<PageGame>()).SetMatchScore(scorePlayer, scoreEnemy);


        if(team == 1)
        {
            item.transform.eulerAngles = new Vector3(0, 180, 0);
            //item.transform.LeanRotateY(180, 0.5f);
        }
    }


    private IEnumerator spawnAnimOnCompleted(float delay)
    { 
        yield return new WaitForSeconds(delay);

        base.Init();

    }

   

    public override void Update()
    {
        base.Update();

     
    }

    public Char createChar(List<Transform> points)
    {
        Char Char = CoreSceneCont.Instance.SpawnItem<Char>(CharPrefab, transform.position, transform);
        Char.changeSideEvent.AddListener(setTeam);
        Transform point = points[Random.Range(0, points.Count - 1)];
        points.Remove(point);
        Char.transform.position = point.position;
      
        Char.name = "Char_" + Char.gameObject.GetInstanceID();
        Char.InitSpawnPos();
        return Char;
    }
    public Char GetChar(bool isPlayer, bool _dontSelectFlyChar=true)
    {
        Char result = null;
        int team = isPlayer ? 0 : 1;
        Char item = getAvaibleItem(team, _dontSelectFlyChar);

        if(item)
        {
            result = item;
        }
        //if (isPlayer)
        //{
          

        //    if (_playerCharList.Count > 0)
        //    {
        //        Char c = getAvaibleCharInLisy(_playerCharList, _dontSelectFlyChar);
        //        if (c)
        //        {
        //            result = c;
        //            _playerCharList.Remove(result);
        //            _playerCharList.Add(result);
                   
        //        }
        //    }

           
           
        //}
        //else
        //{
        //    if (_enemyCharList.Count > 0)
        //    {
        //        Char c = getAvaibleCharInLisy(_enemyCharList, _dontSelectFlyChar);
        //        if (c)
        //        {
        //            result = c;
        //            _enemyCharList.Remove(result);
        //            _enemyCharList.Add(result);

        //        }
        //    }
                


       
        
        //}

        return result;
    }

    private Char getAvaibleItem(int team, bool _dontSelectFlyChar)
    {
        Char r = null;
        foreach (Char Item in GetTeamItems(team))
        {
            if (!Item.isFly && _dontSelectFlyChar)
            {
                r = Item;
                break;
            }
        }
        return r;
    }

    public int GetGameResult()
    {
        int result = 0;



        if (((PlayerCont)CorePlayerCont.Instance).ThrowerPlayer.gameObject.activeSelf)
        {
            if(getTeamIsWon(0))
                result = 2;
        }
        if (((PlayerCont)CorePlayerCont.Instance).ThrowerAI.gameObject.activeSelf)
        {
            if (getTeamIsWon(1))
                result = 1;
        }


        return result;
    }

    private bool getTeamIsWon(int _team)
    {
        bool result = (GetTeamItems(_team).Count == 0);

        return result;

       
    }

    //private void changeItemSide(Char character, int side)
    //{
    //    if (newTeam == 0)
    //    {
    //        _enemyCharList.Remove(character);
    //        _playerCharList.Add(character);
    //        character.SetTeam(0);
           
    //    }
    //    else
    //    {
    //        _playerCharList.Remove(character);
    //        _enemyCharList.Add(character);
    //        character.SetTeam(1);
    //    }


    //}

    //public List<Char> GetCharBySide(int _side)
    //{
    //    if (_side == 0)
    //        return _playerCharList;
    //    else
    //        return _enemyCharList;
    //}
}