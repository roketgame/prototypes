﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Playables;
using UnityEngine.Events;
using System;
using System.Linq;

[Serializable]
public struct GameOverState
{
    public PlayableDirector Timeline;
    public ParticleSystem[] Particles;
    public int ParticlePlayTime;
    public Animator Anim;
}


public class LevelCont : HyperLevelCont
{
    public class EventPlayCompleted : UnityEvent { };
    [HideInInspector] public EventPlayCompleted EventPlayComp = new EventPlayCompleted();
    public int[] RandomStatesRange;
    public GameOverState[] SuccStates;
    public GameOverState[] FailStates;
    public int gameOverStateIndex = -1;
    private GameOverState CurrGameOverState;


    public override void Init()
    {
        base.Init();

        initTimelines(this.gameObject);

    }

    public override void InitLevel()
    {

        base.InitLevel();

        //((LevelCont)HyperLevelCont.Instance).PlayGameOverAnim(true);

        //if (((HyperGameCont)CoreGameCont.Instance).EnableLevelReadFromPlayerPrefs) return;



        if (gameOverStateIndex == -1)
        {

            if (CurrLevel == 1) gameOverStateIndex = 0;
            if (CurrLevel == 2) gameOverStateIndex = 1;
            if (CurrLevel == 3) gameOverStateIndex = 0;
            if (CurrLevel > 3) gameOverStateIndex = UnityEngine.Random.Range(RandomStatesRange[0], RandomStatesRange[1]);

        }


        PlayerCont player = ((PlayerCont)CorePlayerCont.Instance);
        player.ThrowerAI.FireIntervalMin = Mathf.Max(0.2f, 2f - (((float)CurrLevel) * 0.06f));
        player.ThrowerAI.FireIntervalMax = player.ThrowerAI.FireIntervalMin + Mathf.Max(0.4f, 4f - (((float)CurrLevel) * 0.06f));
        player.ThrowerAI.throwForce = Mathf.Min(150, 40f + (((float)CurrLevel) * 4f));
        float pullXRange = Mathf.Max(0.05f, 0.5f - (((float)CurrLevel) * 0.05f));
        float pullZMin = 1;
        float pullZMax = Mathf.Min(4f, 1.0f + (((float)CurrLevel) * 0.05f));
        player.ThrowerAI.ThrowPullMinRange = new Vector3(0 - pullXRange, 0, pullZMin);
        player.ThrowerAI.ThrowPullMaxRange = new Vector3(0 + pullXRange, 0, pullZMax);
    }


    public override void EndLevel(int levelResult)
    {
        base.EndLevel(levelResult);


       
        ((LevelCont)HyperLevelCont.Instance).PlayGameOverAnim(levelResult > 1);

        if (GetLevelResult() == 2)
        {
           
            float levelScore = (Mathf.Floor(CurrLevel / 5) +1) * 10; 
            SetLevelScore(levelScore);
         
        }
       





    }




    /*
     * GetLevelResult
     * 0 -> game continue
     * 1 -> gameover fail!
     * 2 -> gamover succ!
     */
    public override int GetLevelResult()
    {
        int gameResult = CharController.Instance.GetGameResult();
        return gameResult;
    }



    private void initTimelines(GameObject parentObject)
    {
        foreach (Animator anim in parentObject.GetComponentsInChildren<Animator>())
        {
            anim.gameObject.SetActive(false);

        } 
    }


    public void PlayGameOverAnim(bool isWin)
    {

        //GameOverState state = isWin ? StateSucc : StateFail;
        if(isWin)
        {
            int stateIndex = gameOverStateIndex > -1 ? gameOverStateIndex : UnityEngine.Random.Range(RandomStatesRange[0], RandomStatesRange[1]);
            if (stateIndex < SuccStates.Length)
            {
                
                playState(SuccStates[stateIndex]);
            }
               
        }
      



        

    }

    private void playState(GameOverState state)
    {
        CurrGameOverState = state;
        if (CurrGameOverState.Anim && CurrGameOverState.Timeline)
        {
            CurrGameOverState.Anim.gameObject.SetActive(true);
            CurrGameOverState.Timeline.Play();
            CharController.Instance.gameObject.SetActive(false);
        }
       
        if (CurrGameOverState.Particles.Length > 0) StartCoroutine(playParticle(CurrGameOverState.ParticlePlayTime));
    }

    private IEnumerator stopState(float delay)
    {
        yield return new WaitForSeconds(delay);
        if (CurrGameOverState.Anim) CurrGameOverState.Anim.gameObject.SetActive(false);
        EventPlayComp.Invoke();
    }
    private IEnumerator playParticle(float delay)
    {
        yield return new WaitForSeconds(delay);

        foreach (ParticleSystem particle in CurrGameOverState.Particles)
        {
            particle.Play();
        }

    }



}
