﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using TMPro;
using System.Linq;

public class GameCont : HyperGameCont
{
    public bool TutorialIsCompleted ;
    public override void Init()
    {

        //GameAnalytics.Initialize();

        Notifys.Instance.Init();
        Tutorial.Instance.Init();
       
        base.Init();

        Obstacle.Instance.Init();

        //HyperCameraCont.Instance.AnimFrom("spawn_state", 1f);
        initTutorial();

    }
    protected override void uiContOnHandler(GameStatus _status)
    {
        if (_status == GameStatus.LOADED)
        {

            CharController.Instance.CreateChars();
            CharController.Instance.EventGameStatus.AddListener(charContOnGameStatus);
        }

    }
    private void initTutorial()
    {
        int _tutorialIsEnable = 0;
        if (PlayerPrefs.HasKey("TutorialIsEnable"))
        {
            _tutorialIsEnable = PlayerPrefs.GetInt("TutorialIsEnable");

        }
        TutorialIsCompleted = (_tutorialIsEnable > 0);
        if (!TutorialIsCompleted)
        {
            Tutorial.Instance.EventCompleted.AddListener(tutorialOnCompleted);
            Tutorial.Instance.StartTutorial();
        }
        
    }

    private void tutorialOnCompleted()
    {
        PlayerPrefs.SetInt("TutorialIsEnable", 1);
        PlayerPrefs.Save();
        TutorialIsCompleted = true;


    }

 

    private void charContOnGameStatus(GameStatus _status)
    {
        if(_status == GameStatus.INIT)
        {
            ((HyperUICont)CoreUiCont.Instance).OpenPageGame().EventStart.AddListener(onStarCalledFromPageGame);
        }
    }


    public override void StartGame()
    {
        

        ((PageGame)CoreUiCont.Instance.GetPageByClass<PageGame>()).SetText("TxtEnemyName", getRandomUsername());


        //CoreUiCont.Instance.GetPageByClass<HyperPageGame>().BtnStart.gameObject.SetActive(false);
        //if (tutorialIsCompleted)
        {
            //startCountDown();
            base.StartGame();
        }





    }

    public override void Update()
    {
        base.Update();

        //if(Input.GetKeyDown("."))
        //{
        //    StartCoroutine(TinyScreenCapture());
        //}
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);

    }



    private IEnumerator startMatch(float delay)
    {
        yield return new WaitForSeconds(delay);
        base.StartGame();
    }





    private string getRandomUsername()
    {
        string username = "user";
        for (int i = 0; i < 5; i++)
        {
            username += Random.Range(0, 9).ToString();
        }
        return username;
    }
}
