﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class PlayerCont : HyperPlayerCont
{

    public PlayerThrower ThrowerPlayer;
    public AIThrower ThrowerAI;


    public override void Init()
    {
        ThrowerPlayer = GetComponent<PlayerThrower>();
        
        base.Init();
    }
    public override void Update()
    {
        base.Update();

    }



    public void ShakeCamera(float magnitude)
    {
        StartCoroutine(shakeCameraUpdate(0.15f, magnitude));
    }
     private IEnumerator shakeCameraUpdate(float duration, float magnitude)
    {
        Vector3 defaultPos = Cam.transform.localPosition;
        float timer = 0;
        while (timer < duration)
        {
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            Cam.transform.localPosition = new Vector3(defaultPos.x + x, defaultPos.y + y, defaultPos.z);
            timer += Time.deltaTime;
            yield return null;
        }
        Cam.transform.localPosition = defaultPos;
    }
}
