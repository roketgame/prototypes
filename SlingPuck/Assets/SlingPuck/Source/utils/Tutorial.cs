﻿using RoketGame;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Events;

public class Tutorial : CoreCont<Tutorial>
{
    public class EventTutorialCompleted : UnityEvent { };
    public EventTutorialCompleted EventCompleted = new EventTutorialCompleted();
    public static bool IsEnable;
    public GameObject Cursor;
    public bool IsCompleted;
    public List<RectTransform> Points;
    public int currStep;
    private bool isStarted;
    private Vector3 moveToStart;

   public override void Init()
    {
        Cursor.gameObject.SetActive(false);
        IsCompleted = true;
        base.Init();

      
    }
    public void StartTutorial()
    {
        IsCompleted = false;
        isStarted = true;
        IsEnable = true;
        Cursor.gameObject.SetActive(true);
        SetStep(0);
    }
    public void StopTutorial()
    {
        isStarted = false;
        IsEnable = false;
        LeanTween.cancel(Cursor);
        Cursor.SetActive(false);
    }
    public void SetStep(int _step)
    {
        if(_step < Points.Count)
        {

            if(_step == 0)
            {
                Char getRandomChar = CharController.Instance.GetChar(true);
                if(getRandomChar)
                {
                    //moveToStart = getRandomChar.transform;
                    moveToStart = Camera.main.WorldToScreenPoint( getRandomChar.transform.position);
                    //moveToStart.eulerAngles = Vector3.zero;
                }
              
            }
            else
            {
                moveToStart = Points[_step - 1].position;
            }
            currStep = _step;
            moveTo(Points[currStep].position, 1);
        }
        else
        {
            IsCompleted = true;
            EventCompleted.Invoke();
            StopTutorial();
        }
       
    }
    public void NextStep()
    {
        SetStep(currStep + 1);
    }

    private void moveTo(Vector3 _pos, float _time)
    {
        
        LeanTween.cancel(Cursor);
        //Cursor.transform.position = new Vector3(moveToStart.position.x, 0, moveToStart.position.z);
        Cursor.transform.position = moveToStart;
        //Cursor.transform.rotation = moveToStart.rotation;

        LTDescr d = LeanTween.move(Cursor, _pos, _time);
        //LeanTween.rotate(Cursor, _trans.eulerAngles, _time);

        if (d != null)
        {

            d.setOnComplete(moveToCompleted);
        }
    }

    private void moveToCompleted()
    {

        if (isStarted) StartCoroutine(moveToCompletedDelay(0.2f));

    }

    private IEnumerator moveToCompletedDelay(float time)
    {
        yield return new WaitForSeconds(time);
        moveTo(Points[currStep].position, 1);
    }
}
