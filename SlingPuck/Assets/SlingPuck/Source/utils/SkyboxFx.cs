﻿using JetBrains.Annotations;
using RoketGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxFx : CoreObject
{
    public override void StartGame()
    {
        base.StartGame();

        LeanTween.value(gameObject, updateValueExampleCallback, -50, 50, 1f).setEase(LeanTweenType.linear).setLoopPingPong();
        void updateValueExampleCallback(float val)
        {
            gameObject.GetComponent<Skybox>().material.SetFloat("Pitch", val);
        }
    }

}
