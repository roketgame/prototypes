﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public enum ThrowStatus { LOAD, HOLD, PULL_UPDATE, RELEASE }
public enum CharacterStatus { IDLE, DRAG_STARTED, DRAG_UPDATE, DRAG_STOPPED, READY_FOR_THROW, STRETCH_START, STRETCH_UPDATE, THROW_START, HIT_TO_OTHER_ON_FLY, FLY_STOP, HIT_TO_BOARD, HIT_FROM_OTHER }

public class ChangeSideEvent : UnityEvent<Char, int>{}
public class CharAnimEvent : UnityEvent<Char, int>{ }



