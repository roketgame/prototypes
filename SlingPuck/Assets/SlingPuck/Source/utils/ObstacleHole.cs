﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleHole : HyperSceneObj
{
    public BoxCollider box;


    public Vector3 GetRandomPosInBox(float offset)
    {
        if(box==null) box = GetComponent<BoxCollider>();

        Bounds bounds = box.bounds;
        float extentsX = (bounds.extents.x + offset);
        float randX = Random.Range(-extentsX, extentsX);
        Vector3 result = transform.position;
        result.x = randX;
        return result;
    }
}
