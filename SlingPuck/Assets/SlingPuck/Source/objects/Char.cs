﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Char : HyperSceneObj
{
    public float[] spawnDelayRange;
    public CoreSceneObject pusher;
    public Collider _collider;
    public MeshRenderer teamIcon;
    public bool IsPlayer;
    public bool isPlayerSide;
    public ParticleSystem particleHitToOther;
    public ParticleSystem particleHitToObstacle;
    public ParticleSystem particleSpawn;
    public BoxCollider SafeAreaCollider;
    [HideInInspector] public bool isReadyForThrow;
    public bool isFly;
    public bool isThrowed;
    public bool isExitFromSafeArea;
    private bool isSpawned;
    [HideInInspector] public bool isIdleStatus; //default idle durumunda

    public Material[] CharMaterials;
    public GameObject MeshGo;
    public SkinnedMeshRenderer CharMeshRenderer;
    private CharacterStatus charStatus;
    private float direction; //rotation y
    private float newDirection;

    public CharAnim Anim;
    public float speed;
    private Vector3 lastPos;
    private bool isFirstGroundHit = false;
    private float pushForce;
    private float changeIdleAnimTime;
    private float[] changeIdleAnimRange = new float[] { 1,5 };
    private float timerToChangeIdleAnim;
    public ChangeSideEvent changeSideEvent = new ChangeSideEvent();
    public TextMeshPro DebugText;





    public override void Init()
    {
        _collider = GetComponent<Collider>();
        if(DebugText)  DebugText.SetText(name);
        Anim = GetComponentInChildren<CharAnim>();
        Anim.name = this.name + "_anim";
        lastPos = transform.position;
          isReadyForThrow = false;
        isSpawned = false;
        CharMeshRenderer.enabled = false;
        changeIdleAnimTime = getRandomIdleAnimTime();
        base.Init();
    }

    public void InitSpawnPos()
    {
     
        isFirstGroundHit = false;

       


        int _randHit = UnityEngine.Random.Range((int)1, (int)2);
        Anim.Play("hit_" + _randHit);



        Vector3 pos = Anim.gameObject.transform.localPosition;
        pos.y += 30;
        Anim.gameObject.transform.localPosition = pos;
        //rb.AddForce(new Vector3(0, -40, 0), ForceMode.Impulse);
        float delay = UnityEngine.Random.Range(spawnDelayRange[0], spawnDelayRange[1]);
        Anim.gameObject.LeanMoveLocalY(0, 0.8f).setDelay(delay).setEase(LeanTweenType.easeOutBounce).setOnStart(()=> { CharMeshRenderer.enabled = true; if(particleSpawn) particleSpawn.Play();  }).setOnComplete(() => {  });
        
        isSpawned = true;


    }

    private IEnumerator setActiveChar(float time)
    {
        yield return new WaitForSeconds(time);

     


    }



    private float getRandomIdleAnimTime()
    {
        return UnityEngine.Random.Range(changeIdleAnimRange[0], changeIdleAnimRange[1]);
    }

    private void changeIdleAnim()
    {
        int randId = UnityEngine.Random.Range(0, 1);
        Anim.Play("idle_" + randId);
    }




    public override void StartGame()
    {
        base.StartGame();
        Anim.EventAnim.AddListener(onAnimEvent);
        direction = transform.eulerAngles.y;
        newDirection = direction;
        //pusher.EventCollision.AddListener(onPusherTrigger);
        //if (pusher.EventCollision != null)
    }

 
    public  void FixedUpdate()
    {
       
    }


    public override void Update()
    {
   
        if (!isSpawned) return;
        base.Update();



        if (isPlayerSide)
        {
            if (transform.position.z > 0)
            {
                changeSideEvent.Invoke(this, 1);
                dispatchGameOver();


            }
        }
        else
        {
            if (transform.position.z < 0)
            {
                changeSideEvent.Invoke(this,0);
                dispatchGameOver();

            }
        }


        if(!isReadyForThrow)
        {
            direction = Mathf.Lerp(direction, newDirection, Time.deltaTime * speed );

            Vector3 rot = transform.rotation.eulerAngles;
            rot.y = direction;
            transform.eulerAngles = rot;

            if(timerToChangeIdleAnim > changeIdleAnimTime)
            {
                changeIdleAnim();
                timerToChangeIdleAnim = 0;
            }

            timerToChangeIdleAnim += Time.deltaTime;
        }


        {

            if (isFly && GetLayerName() != "Char")
            {
                //if (!SafeAreaCollider.bounds.Contains(transform.position))
                if (!getPointInnerBox(SafeAreaCollider, transform.position))
                {
                    SetLayerName("Char");
                }
            }



        }


        //if (transform.position.x > 10 || transform.position.x < -10)
        //Time.timeScale = 0.02f;
        if (transform.position != lastPos)
        {
            float newSpeed = (transform.position - lastPos).magnitude;
            //float newSpeed = rb.velocity.magnitude;
            bool isIncreaseSpeed = (newSpeed > speed && speed > 0);

            Anim.SetSpeed(speed);

            if (isThrowed)
            {
                if (!getPointInnerBox(SafeAreaCollider, transform.position))
                {
                    isExitFromSafeArea = true;
                }


                if (!isIncreaseSpeed && isExitFromSafeArea) //hiz dusuyor
                {
                    if (speed < 0.4f)
                    {

                        SetStatus(CharacterStatus.FLY_STOP);
                        //SetStatus(CharacterStatus.HIT_TO_BOARD);
                    }

                    if (speed < 0.1f)
                    {
                        IsPlayer = false;
                        isFly = false;
                        isThrowed = false;
                        Anim.SetIsFly(false);
                        Anim.SetIsPlayer(IsPlayer);

                    }
                    //Debug.Log(name + " speed : " + speed + " isIncreaseSpeed: " + isIncreaseSpeed + " isFly: " + isFly + " layer " + GetLayerName());
                }


            }


                speed = newSpeed;
            //Debug.Log(name + " speed : " + speed + " isIncreaseSpeed: " + isIncreaseSpeed + " isFly: " + isFly + " layer " + GetLayerName());
        }
        
        lastPos = transform.position;


    }

    private void dispatchGameOver()
    {
        if (LevelCont.Instance.GetLevelResult() > 0)
            StartCoroutine(dispatchGameOver(1));
    }
    private IEnumerator dispatchGameOver(float delay)
    {
        yield return new WaitForSeconds(delay);
        HyperLevelCont.Instance.TryEndLevel();
    }


    private bool getPointInnerBox(BoxCollider _box, Vector3 _pos)
    {
        bool r = false;
        Vector3 center = _box.bounds.center;
        Vector3 extents = _box.bounds.extents;
        if (
            _pos.x < center.x + extents.x && _pos.x > center.x - extents.x &&
            _pos.z < center.z + extents.z && _pos.z > center.z - extents.z

            )
            r = true;

            return r;
    }
    public void Throw(Vector3 force)
    {
        rb.AddForce( force, ForceMode.Impulse);
  
       
    }




    public void SetStatus(CharacterStatus _status)
    {
        switch(_status)
        {
            case CharacterStatus.IDLE: //DEFAULT DURDUGU STATUS
                isIdleStatus = true;
               
                //Anim.Play("idle");
               

                break;

            case CharacterStatus.DRAG_STARTED:
                {
                    SetTrigger(true);
                    rb.useGravity = false;
                    isIdleStatus = false;
                    Anim.Play("stretch");
                }
                break;

            case CharacterStatus.DRAG_UPDATE:
                break;

            case CharacterStatus.DRAG_STOPPED:
                break;


            case CharacterStatus.READY_FOR_THROW: //cekme pozisyonunda bekliyor. henuz hold_start baslamadi.
                isReadyForThrow = true;
                rb.useGravity = false;
                //setTrigger(true);
                SetLayerName("Char_Trigger");
                Anim.SetHitType(-1);
                Anim.Play("ready");
                if(isPlayerSide)
                {
                    IsPlayer = true;
                    Anim.SetIsPlayer(IsPlayer);
                }
               
                break;
            case CharacterStatus.STRETCH_START: //Cek
                {
                    Anim.Play("stretch");

                }
                break;
            case CharacterStatus.STRETCH_UPDATE: //cekmeye devam
                break;
            case CharacterStatus.THROW_START: //birak! go!
                {
                    isThrowed = true;
                    isExitFromSafeArea = false;
                    isReadyForThrow = false;
                    isFly = true;
                    rb.useGravity = true;
                    SetTrigger(false);
                    Anim.SetIsFly(true);
                    Anim.Play("fly");
                Anim.gameObject.LeanCancel();
                Anim.gameObject.LeanMoveLocalY(10, 0.2f).setOnComplete(() => {
                    Anim.gameObject.LeanMoveLocalY(0, 0.2f);
                    //LeanTween.moveLocalY(Anim.gameObject, 0, 0);
                });
                }
                break;


            case CharacterStatus.FLY_STOP: //UCUS sona erdi, yere kon
                if (charStatus != _status)
                {

                   

                    //Anim.Anim.Play("fly");
                    //Anim.SetIsFly(false);
                    Anim.SetHitType(0);


                }
                break;

            case CharacterStatus.HIT_TO_BOARD: //isPlayer, corner ve engellere carpinca
                if (charStatus != _status)
                {
                    Anim.SetHitType(1);
                    //int randomFallAnim = UnityEngine.Random.Range(1, 3);
                    //Anim.Anim.SetInteger("FallType", randomFallAnim);
                    newDirection = UnityEngine.Random.Range(0, 360);
                    //MobileVibration.Vibrate(MobileVibration.Level.LOW);
                     ((PlayerCont)PlayerCont.Instance).ShakeCamera(0.15f);
                    if (particleHitToObstacle) particleHitToObstacle.Play();
                }
                break;

            case CharacterStatus.HIT_TO_OTHER_ON_FLY: //ucarken, diger char ile hit oldugunda
                if (charStatus != _status)
                {
                    Anim.SetHitType(1);

                    ((PlayerCont)PlayerCont.Instance).ShakeCamera(0.35f);
                    if(particleHitToOther) particleHitToOther.Play();
                    MobileVibration.Vibrate(MobileVibration.Level.NORMAL);

                   
                }
                break;
            case CharacterStatus.HIT_FROM_OTHER: //idle drumunda, diger char bize carparsa
                if (charStatus != _status)
                {
                    Anim.SetHitType(1);
                    Anim.Play("hit");

                }
                break;

        }

        charStatus = _status;
    }


    /* ANIMATION EVENTS */
    private void onAnimEvent(Char _char, int _type)
    {
        if (_type == 0) //on_floor
        {
            SetStatus(CharacterStatus.IDLE);
        }
    }
  
    public void SetTrigger(bool _isTrigger)
    {
        _collider.isTrigger = _isTrigger;
        //if(pusher ) pusher.GetComponent<Collider>().isTrigger = _isTrigger;
    }


    public bool OnGround()
    {
        bool r = false;
        List<RaycastHit> listResult = Utils.RAYCAST(transform.position, Vector3.down * 2, new string[] { "Ground" });
        if(listResult.Count > 0)
        {
            r = true;
        }
        return r;

    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);




        if (GameStatus != GameStatus.STARTGAME || !isSpawned) return;



        //if (isFly && collision.collider.name != "arena")
        if (collision.collider.name != "arena")
         {

            Char otherChar = collision.collider.GetComponent<Char>();
            if (otherChar)
            {

                if (IsPlayer)
                {

                    SetStatus(CharacterStatus.HIT_TO_OTHER_ON_FLY);
                    if (otherChar.isFly)
                        Notifys.Instance.OpenNotify("headshot", otherChar.transform.position);

                    if (!otherChar.isPlayerSide)
                    { 
                        if (!otherChar.isFly) Notifys.Instance.OpenNotify("perfect", otherChar.transform.position);
                    }
                   

                }
                else
                {
                    Anim.SetOtherSpeed(otherChar.speed);
                    SetStatus(CharacterStatus.HIT_FROM_OTHER);



                }
            }
            else
            {
                SetStatus(CharacterStatus.HIT_TO_BOARD);
            }




        }
        else
        {
            if(!isFirstGroundHit)
            {
                isFirstGroundHit = true;

                int randIdle = UnityEngine.Random.Range(1, 2);
                Anim.Play("hit"+ randIdle + "_down");
            }
            
        }















    }

    /* hold_update asamasinda player surekli update ediyor */
    public void SetDirection(Vector3 _dir)
    {
        Quaternion rot = Quaternion.LookRotation(_dir );
        gameObject.transform.rotation = rot;
        //throwDirection = _dir;
    }

    /* 0-player, 1-ai */
    public void SetTeam(int _team)
    {
        int team = Mathf.Clamp(_team, 0, 1);
        if(CharMaterials.Length > 1)
        {
            CharMeshRenderer.material = CharMaterials[team];
        }

        if(teamIcon)
        {
            teamIcon.material.color = (_team == 0) ? Color.red : Color.blue;
        }
        isPlayerSide = (_team == 0);
    }

   
    public void Prepare(Vector3 throwStartPoint, bool isPlayer, float _pushForce)
    {
        //throwStartPos = throwStartPoint;

        rb.velocity = Vector3.zero;
        transform.position = throwStartPoint;
        pushForce = _pushForce;

    }



    //private void addJoint(Char _other)
    //{
    //    if (joinTo == null)
    //    {


    //        Rigidbody targetRb = _other.rb;
    //        if (targetRb)
    //        {
    //            Debug.Log(name + " joint " + _other.name);
    //            //joint = gameObject.AddComponent(typeof(SpringJoint)) as SpringJoint;
    //            //joint.connectedBody = targetRb;
    //            //joint.connectedAnchor = transform.position;
    //            this.isFly = false;
    //            this.joinTo = _other;
    //            this.isJointOwner = true;
    //            _other.joinTo = this;

    //            _other.rb.velocity = Vector3.zero;
    //            _other.rb.isKinematic = true;

    //            _other.transform.parent = this.transform;
    //            _other._collider.isTrigger = true;
    //            _other.transform.localPosition = Vector3.zero;
    //            _other.MeshGo.transform.localPosition = Vector3.forward * 1.5f;
               

    //            Anim.Anim.Play("fly");
    //            Anim.Anim.SetBool("IsFall", true);
    //            Anim.Anim.SetInteger("FallType", 3);
    //            _other.Anim.Anim.Play("ai_collision_player");
    //            Invoke("removeJoint", 2.0f);
    //            //Time.timeScale = 0.3f;
    //        }


    //    }
    //}

    //public void removeJoint()
    //{
    //    //if (joint != null && joinTo != null)
    //    if (joinTo != null)
    //    {
    //        joinTo.transform.parent = CharController.Instance.transform;
    //        joinTo._collider.isTrigger = false;
    //        joinTo.rb.isKinematic = false;
    //        joinTo.MeshGo.transform.localPosition = Vector3.zero;
    //        //Time.timeScale = 1f;
            


    //        SetStatus(CharacterStatus.FLY_STOP);
    //        isFly = false;

    //        //Vector3 force1 = randDirection() * 500;
    //        //Vector3 force2 = randDirection() * 500;


    //        //this.rb.AddForce(force1, ForceMode.Force);
    //        //joinTo.rb.isKinematic = false;
    //        //joinTo.rb.AddForce(force1, ForceMode.Force);

    //        joinTo.joinTo = null;
    //        this.joinTo = null;
    //        this.isJointOwner = false;
    //    }

    //}

    private Vector3 randDirection()
    {
        Vector3 v = new Vector3();
        v.x = UnityEngine.Random.Range(-1f, 1f);
        v.y = UnityEngine.Random.Range(-1f, 1f);
        v.z = UnityEngine.Random.Range(-1f, 1f);
        return v;
    }


  

}