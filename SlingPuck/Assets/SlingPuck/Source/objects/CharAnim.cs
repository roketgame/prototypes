﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class CharAnim : CoreSceneObject
{
    private Char character;
    public CharAnimEvent EventAnim;
    private Animator Anim;
    private string CurrPlay;

    public override void Init()
    {
        base.Init();
        EventAnim = new CharAnimEvent();
        Anim = GetComponent<Animator>();
        character = GetComponentInParent<Char>();
    }

    public void Play(string _id)
    {
        CurrPlay = _id;
        Anim.Play(_id);
        Debug.Log(name + "anim Play " + _id);
    }

    public void SetIsPlayer(bool _isPlayer)
    {
        Anim.SetBool("IsPlayer", _isPlayer);

    }

    public void SetSpeed(float _speed)
    {
        Anim.SetFloat("Speed", _speed);
    }
    public void SetOtherSpeed(float _speedOther)
    {
        if(CurrPlay != "hit")
        {
            Debug.Log(name + "anim SpeedOther " + _speedOther); 
            Anim.SetFloat("SpeedOther", _speedOther);
        }

    }

    

    public void SetIsFly(bool _isFly)
    {
        Anim.SetBool("IsFly", _isFly);
    }

    /*
     * -1 : hit yok
     * 0 : hit yok ama yere dusmesi(dusmesi) gerek
     * 1 : board yada diger char ile hit
     */
    public void SetHitType(int _hitType)
    {
        Anim.SetInteger("HitType", _hitType);
    }


    public void OnFloor()
    {
        //Debug.Log("OnFloor: ");
        EventAnim.Invoke(character, 0);
    }
}
