﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Obstacle : CoreCont<Obstacle>
{
    public List<GameObject> PrefabObstaclesByLevel;
    private GameObject currObstacle;

    public override void Init()
    {
        base.Init();

        if (currObstacle)
            Destroy(currObstacle);

        int currObstacleIndex = (LevelCont.Instance.CurrLevel < PrefabObstaclesByLevel.Count) ? LevelCont.Instance.CurrLevel : PrefabObstaclesByLevel.Count - 1;

        currObstacle = Instantiate(PrefabObstaclesByLevel[currObstacleIndex], transform);
    }

    public  override void StartGame()
    {
        base.StartGame();

     
    }

   
}
