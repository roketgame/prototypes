﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class RopeCont : HyperSceneObj
{
    public GameObject PrefabRopeItem;
    public GameObject PrefabRopeCorner;
    public GameObject PrefabRopeCenter;
    public Color RopeColor;
    public float PullValueZ;
    public float PushForceZ ;
    public int RopeCount = 16;
    public float RopeSpace = 1;

    private GameObject ropeCenter;

    public void Start()
    {
        //Init();
    }
    public override void Init()
    {
        //base.Init();
        ropeCenter = spawnItem(PrefabRopeCenter, Vector3.zero, false);
        spawnItems(1);
        spawnItems(-1);
    }


    public void PullStart()
    {
        ropeCenter.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }
    public void PullUpdate(Vector3 _pull)
    {
        Vector3 pos = _pull * 1.2f;
        //pos.z = _ratioZ * PullValueZ;
        ropeCenter.transform.localPosition = pos;
    }
    public void Push()
    {
        ropeCenter.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, PushForceZ), ForceMode.Impulse);
        ropeCenter.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezeRotation;
    }
    private void spawnItems(int _direction)
    {
        float x = (RopeSpace / 2) * _direction ;
        Rigidbody prevRb = ropeCenter.GetComponent<Rigidbody>();
        int count = RopeCount / 2;
        for (int i = 0; i < count; i++)
        {
            GameObject go;
            Vector3 pos = new Vector3(x, 0, 0);
            if (i == count - 1)
            {
                go = spawnItem(PrefabRopeCorner, pos, false);
            }
            else
            {
                go = spawnItem(PrefabRopeItem, pos);
            }

            Rigidbody rb = go.GetComponent<Rigidbody>();
            Joint joint = go.GetComponent<Joint>();
            joint.connectedBody = prevRb;

            prevRb = rb;
            x += _direction * RopeSpace;

        }
    }

    private GameObject spawnItem(GameObject prefab, Vector3 _pos, bool _isVisible = true)
    {
        GameObject go = Instantiate(prefab, _pos, Quaternion.identity);
        go.transform.parent = transform;
        go.transform.localPosition = _pos;
        MeshRenderer meshRend = go.GetComponentInChildren<MeshRenderer>();
        if (meshRend)
        {
            meshRend.enabled = _isVisible;
            meshRend.material.color = RopeColor;
        }
        return go;
    }
}
