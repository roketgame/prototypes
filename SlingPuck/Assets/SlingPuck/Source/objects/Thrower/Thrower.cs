﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;


public class Thrower : HyperSceneObj
{
    public bool  IsEnableAutoLoad = true;
    public float edgeDistance = 2;
    public Vector3 RopePosOffset = new Vector3(0, 0.7f, 0); //ipin gerilme noktasinin thrower merkezine gore pozisyon offseti
    protected Vector3 firstPosition = Vector3.zero;
    protected Vector3 ThrowDirection = Vector3.zero;
    protected Vector3 LastPosition = Vector3.zero;

    protected Camera MainCamera;
    public float inputPositionOffset = 1;
    public CoreSceneObject ThrowSafeArea; //firlatma sonrasi bu alandan cikinca Char.isTrigger false oluyor (fizik basliyor)
    //public CoreSceneObject ThrowSafeArea2;
    public float maxThrowLimit = 3;
    public float throwForce = 1000;
    public float ForceMin;
    public bool IsPlayer;
    //public float PushForce = 0; //safe area onunde firlatilan char'in diger char'lar ile triggerinda uygulanacak extra push force degeri. buradaki triggeri char/push objesi sagliyor.
    public float ThrowAngleLimitMin;
    public float ThrowAngleLimitMax;

    protected Char CurrentChar;

    //protected RopeController RopeController;
    protected RopeCont rope;
    protected TrajectoryController TrajectoryController;
    protected CharController CharController;
    protected ThrowStatus throwStatus;
    protected bool isOnThrowState; //load'da baslayan ve throw sonrasi biten process

    public override void Init()
    {

        base.Init();


       
            rope = GetComponentInChildren<RopeCont>();
        

        if (GetComponentInChildren<TrajectoryController>() == null)
        {
            Debug.LogError("TrajectoryController not found!");
        }
        else
        {
            TrajectoryController = GetComponentInChildren<TrajectoryController>();
        }

        if (FindObjectOfType<CharController>() == null)
        {
            Debug.LogError("CharController not found!");
        }
        else
        {
            CharController = FindObjectOfType<CharController>();
        }


        if (Camera.main == null)
        {
            Debug.LogError("Main Camera not found!");
        }
        else
        {
            MainCamera = Camera.main;
        }


        //RopeController.CenterPos =  transform.position + RopePosOffset;
        TrajectoryController.CenterPos = transform.position + RopePosOffset;

       
    }

    public override void Update()
    {
        base.Update();

        if (!IsGameStarted) return;

        if (CurrentChar == null && IsEnableAutoLoad)
        {
            SetThrowStatus(ThrowStatus.LOAD);


        }

       

    }
    public override void StartGame()
    {
        base.StartGame();

        if (ThrowSafeArea)
        {
            ThrowSafeArea.EventCollision.AddListener(onTriggerSafeArea);
        }
        //if (ThrowSafeArea2)
        //{
        //    ThrowSafeArea2.EventCollision.AddListener(onTriggerSafeArea2);
        //}
    }

   

public virtual void SetThrowStatus(ThrowStatus _status)
    {
        switch(_status)
        {
            case ThrowStatus.LOAD:
                {
                    if(!CurrentChar)  
                    CurrentChar = CharController.GetChar(IsPlayer);
                    if (CurrentChar)
                    {
                        isOnThrowState = true;
                        CurrentChar.Prepare(transform.position, IsPlayer, 0);
                        CurrentChar.IsPlayer = IsPlayer;
                        CurrentChar.SafeAreaCollider = ThrowSafeArea.GetComponent<BoxCollider>();
                        CurrentChar.SetStatus(CharacterStatus.READY_FOR_THROW);

                   
                }
                }
                break;

            case ThrowStatus.HOLD:
            {
                    if(CurrentChar)
                    {
                        firstPosition = GetMousePosition();
                        //RopeController.StartAim();
                        rope.PullStart();
                        TrajectoryController.StartTrajectory();
                        CurrentChar.SetStatus(CharacterStatus.STRETCH_START);
                    }
                   
                }
            break;
           
            case ThrowStatus.PULL_UPDATE:
            {

                    
                    if (CurrentChar)
                    {
                        LastPosition = GetMousePosition();
                        ThrowDirection = CalculateDirection();
                        //RopeController.StretchRope(ThrowDirection);
                        rope.PullUpdate(ThrowDirection);
                        TrajectoryController.Aiming(ThrowDirection);


                        CurrentChar.SetDirection(ThrowDirection * -1);
                        CurrentChar.transform.position = ThrowDirection + transform.position;
                        CurrentChar.SetStatus(CharacterStatus.STRETCH_UPDATE);


                    }
                }
            break;
            case ThrowStatus.RELEASE:
            {
                    if (CurrentChar)
                        Throw();
                }
            break;
        }
        throwStatus = _status;

    }

    private void onTriggerSafeArea(ContactInfo _info)
    {

      

        //if (_info.Status == ContactStatus.TRIGGER_EXIT)
        //{
        //    Char Char = _info.Other.GetComponent<Char>();

        //    if (Char)
        //    {
        //        Debug.Log(name + "erdal isFly : " + Char.isFly);
        //        if (Char.isFly)
        //        {
        //            Char.SetLayerName("Char");
        //        }


        //    }

        //}



    }


    protected void Throw()
    {
        Vector3 force = -1 * GetForce(ForceMin);
        if(force.magnitude > ForceMin && CurrentChar)
        {
            isOnThrowState = false;
            CurrentChar.Throw(force);
            CurrentChar.SetStatus(CharacterStatus.THROW_START);
            CurrentChar = null;

            //RopeController.ResetAim();
            rope.Push();
            TrajectoryController.ResetTrajectory();
            ResetPositions();
        }
        

    }



    protected virtual Vector3 GetMousePosition()
    {
        return Vector3.zero;
    }

    private Vector3 GetForce(float _min)
    {
        
        Vector3 force = ThrowDirection * throwForce;
        if (Mathf.Abs(force.magnitude ) < _min)
        {
            if (ThrowDirection.magnitude == 0)
                ThrowDirection = IsPlayer ? Vector3.forward * -1 : Vector3.forward;
            force = ThrowDirection * _min;

        }
        return force ;
    }

    protected Vector3 CalculateDirection(float _yAngleLimit=0)
    {
        Vector3 dir = LastPosition - firstPosition;
        float angle = getAngle(dir);
        angle = Mathf.Max(ThrowAngleLimitMin, Mathf.Min(ThrowAngleLimitMax, angle));
        Vector2 _dir = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad) * dir.magnitude, Mathf.Sin(angle * Mathf.Deg2Rad) * dir.magnitude);
        dir = new Vector3(_dir.x, 0, _dir.y);
        dir = Vector3.ClampMagnitude(dir, maxThrowLimit);
        return dir;
    }


    private  float getAngle(Vector3 v)
    {
        Vector2 vec = new Vector2(v.x, v.z);
        Vector2 dist = vec - Vector2.zero;
        float angle = Mathf.Atan2(dist.y, dist.x);
        float angleInDegrees = angle * Mathf.Rad2Deg;
        return angleInDegrees;
    }
    private void ResetPositions()
    {
        firstPosition = Vector3.zero;
        LastPosition = Vector3.zero;
        ThrowDirection = Vector3.zero;
    }


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position, 0.5f);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position + Vector3.right * edgeDistance, 0.25f);
        Gizmos.DrawSphere(transform.position + Vector3.left * edgeDistance, 0.25f);


        if (firstPosition != Vector3.zero)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawSphere(firstPosition, 0.25f);
        }

        if (LastPosition != Vector3.zero)
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(LastPosition, 0.25f);
        }
    }
}
