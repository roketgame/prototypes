﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrajectoryController : HyperSceneObj
{
    public GameObject dotPrefabs;
    public int dotCount = 10;
    private List<Transform> _dotList = new List<Transform>();
    public Vector3 CenterPos;

    [Range(0.25f, 1.0f)] public float startOffset = 0.5f;

    [Range(0.05f, 0.5f)] public float scaleMultiple = 0.05f;

    [Range(1.0f, 3.5f)] public float trajectoryDistance = 1.5f;

    private void Start()
    {

        for (int i = 0; i < dotCount; i++)
        {
            _dotList.Add(Instantiate(dotPrefabs, transform).transform);
        }

        ResetTrajectory();
    }


    public void Aiming(Vector3 direction)
    {
        Vector3 distanceBetweenDots = (direction) / dotCount;
        distanceBetweenDots.y = 0;
       
        for (int i = 0; i < dotCount; i++)
        {
            _dotList[i].gameObject.SetActive(true);
            Vector3 pos = CenterPos - (distanceBetweenDots * i * trajectoryDistance) -  (direction.normalized * startOffset);
            //pos.y = 0;
            _dotList[i].transform.position = pos;


        }
    }

    public void ResetTrajectory()
    {
        for (int i = 0; i < dotCount; i++)
        {
            _dotList[i].localScale = Vector3.one * ((i + 1) * scaleMultiple);
            _dotList[i].gameObject.SetActive(false);
        }
    }

    public void StartTrajectory()
    {
    }
}