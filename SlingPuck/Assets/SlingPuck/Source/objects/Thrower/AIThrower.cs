﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



    

public class AIThrower : Thrower
{
    /* FireInterval kac saniyede bir load yapilsin (min) */
    public float FireIntervalMin = 1;
    /* FireInterval kac saniyede bir load yapilsin (min) */
    public float FireIntervalMax = 10;
    /* ThrowPullTime cekme suresi (sec, min) */
    public float ThrowPullTime = 1f;
    /* ThrowPull cekme degeri (min) */
    public Vector3 ThrowPullMinRange = new Vector3(-0.15f, 0, 2f);
    /* ThrowPull cekme degeri (max) */
    public Vector3 ThrowPullMaxRange = new Vector3(0.15f, 0, 3f);

    private float _currentFireRate;
    private float _time;
    private Vector3 pullRandomPos;
    private float pullRatioTime; //ThrowPullTime suresince 0-1 arasinda deger
    private List<ObstacleHole> obstacleHoles;


    public override void Init()
    {
        base.Init();
     
        _currentFireRate = randomFireRate();
    }


    public override void StartGame()
    {
        base.StartGame();

        //if (!Tutorial.Instance.IsCompleted)
            //Tutorial.Instance.EventCompleted.AddListener(tutorialOnCompleted);


    }

    private void tutorialOnCompleted()
    {

    }



    public override void Update()
    {

        base.Update();

        if (!IsGameStarted || !Tutorial.Instance.IsCompleted) return;



        _time += Time.deltaTime;

        //RopeController.StretchRope(ThrowDirection);
        rope.PullUpdate(ThrowDirection);
        TrajectoryController.Aiming(ThrowDirection);

        if (_time > _currentFireRate)
        {
            _time = 0;
            //Throw();

            pullRandomPos = getRandomPull();
            _currentFireRate = randomFireRate();
            StartCoroutine(throwCoroutine(ThrowPullTime));

        }
    }

    protected override Vector3 GetMousePosition()
    {
        Vector3 pos = Vector3.Lerp(Vector3.zero, pullRandomPos, pullRatioTime);

        return pos;
    }

    private IEnumerator throwCoroutine(float duration)
    {
        float timer = 0;
        while (timer < duration)
        {
            pullRatioTime = timer / duration;
            pullRatioTime = Mathf.Clamp(pullRatioTime, 0f, 1f);
            SetThrowStatus(ThrowStatus.PULL_UPDATE);
            timer += Time.deltaTime;
            yield return null;
        }
        SetThrowStatus(ThrowStatus.RELEASE);
    }


    private Vector3 getRandomPull()
    {
        Vector3 randomPosition = Vector3.zero;
        randomPosition.x = Random.Range(ThrowPullMinRange.x, ThrowPullMaxRange.x);
        randomPosition.y = 0;
        randomPosition.z = Random.Range(ThrowPullMinRange.z, ThrowPullMaxRange.z);
        obstacleHoles = RoketGame.CoreSceneCont.Instance.GetItems<ObstacleHole>();
        if(obstacleHoles.Count > 0)
        {
            int randIndex = Random.Range(0, obstacleHoles.Count - 1);
            ObstacleHole selectedHole = obstacleHoles[randIndex];
            Vector3 target = selectedHole.GetRandomPosInBox(0);
            Vector3 dir = (transform.position - target).normalized;
            Vector3 final = dir * Random.Range(ThrowPullMinRange.z, ThrowPullMaxRange.z);
            randomPosition.x = final.x;
            randomPosition.z = final.z;
            Debug.Log("randomPosition " + randomPosition);
        }
       
        return randomPosition;
    }

    private float randomFireRate()
    {
        if(FireIntervalMax - FireIntervalMin < ThrowPullTime) // atesleme intervali, cekme suresinden kisa olmamalı
        {
            return ThrowPullTime +0.1f;
        }
        else
        {
            return Random.Range(FireIntervalMin, FireIntervalMax);
        }
       
    }
}
