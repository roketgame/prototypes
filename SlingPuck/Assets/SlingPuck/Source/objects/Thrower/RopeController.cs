﻿using UnityEngine;

public class RopeController : HyperSceneObj
{
    private LineRenderer _lineRenderer;
    private Thrower thrower;
    public Vector3 CenterPos; 
    private bool _hold;

    public bool Hold
    {
        get => _hold;
        set => _hold = value;
    }

    void Start()
    {
        thrower = transform.parent.GetComponent<Thrower>();
        _lineRenderer = GetComponent<LineRenderer>();
        
        ResetAim();
    }

    public void StartAim()
    {
        _lineRenderer.enabled = true;
    }


    // Update is called once per frame
    public void StretchRope(Vector3 direction)
    {
        _lineRenderer.SetPosition(0, CenterPos + Vector3.left * thrower.edgeDistance);
        _lineRenderer.SetPosition(2, CenterPos + Vector3.right * thrower.edgeDistance);
        _lineRenderer.SetPosition(1, CenterPos + direction);
    }



    public void ResetAim()
    {
        //_lineRenderer.enabled = false;
        StretchRope(Vector3.zero);
    }
}