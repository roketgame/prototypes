﻿using RoketGame;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Touch = RoketGame.Touch;

public class PlayerThrower : Thrower
{
    [Header("Debug")] public bool debug;
    public GUIStyle guiStyle;
    private Char dragChar;
    private Vector3 lastDragPosition;
    public CoreSceneObject DragArea;
    public float TouchNearestDistance = 3; //ekrana touch edildiginde en yakin char secilirken ne kadar yakinlik icinde bakilacagi
    private Vector2 playerThrowStartInput; //throw asamasinda ilk input pozisyonu
    private Vector2 playerThrowLastInput; //throw asamasinda son(current) input pozisyonu
    public override void Init()
    {
        IsPlayer = true;
        base.Init();

    }

    public override void StartGame()
    {
        base.StartGame();
        CoreInputCont.Instance.EventTouch.AddListener(onTouchScreen);


    }


    public override void Update()
    {
        //if (!IsLevelStarted) return;
        base.Update();

       
       if (dragChar)
        {
            //if (_info.Status == ContactStatus.TRIGGER_ENTER)
            {
                //Char Char = _info.Other.GetComponent<Char>();

                //if (Char == dragChar)
               if( DragArea.GetComponent<BoxCollider>().bounds.Contains(dragChar.transform.position))
                {
                    CurrentChar = dragChar;
                    SetThrowStatus(ThrowStatus.LOAD);
                    SetThrowStatus(ThrowStatus.HOLD);
                    dragChar = null;
                }

            }
        }

    }

    public override void SetThrowStatus(ThrowStatus _status)
    {
        base.SetThrowStatus(_status);

        switch (_status)
        {
            case ThrowStatus.LOAD:
            {
               
                if (Tutorial.IsEnable)
                {
                    Tutorial.Instance.NextStep();
                }

                 playerThrowStartInput = currInputPos;


            }
            break;
            case ThrowStatus.RELEASE:
            {

                if (Tutorial.IsEnable)
                {
                    Tutorial.Instance.NextStep();
                }


            }
            break;

            case ThrowStatus.PULL_UPDATE:
            {

                if (CurrentChar)
                {


                    //if (ThrowDirection.z < -0.5f)
                    {

                        Camera.main.gameObject.LeanCancel();
                        Camera.main.gameObject.LeanMoveLocalX(ThrowDirection.x * -1 * 0.5f, 0.4f);
                    }
                        

                    

                }
            }

            break;
        }
    }



    private Vector2 currInputPos;
    private void onTouchScreen(Touch _touch)
    {
         currInputPos = _touch.GetScreenPoint(1);

        //---------> secili CurrentChar'i firlatma olaylari

        if(CurrentChar)
        {
            if (_touch.Phase == TouchPhase.Began)
                SetThrowStatus(ThrowStatus.HOLD);
            if (_touch.Phase == TouchPhase.Moved)
                SetThrowStatus(ThrowStatus.PULL_UPDATE);
            if (_touch.Phase == TouchPhase.Ended)
                SetThrowStatus(ThrowStatus.RELEASE);


            return;
        }

        if (isOnThrowState || IsEnableAutoLoad || !CoreGameCont.Instance.IsGameStarted) return;


        //---------> ekrandan bir char secip drag etme kisimlari (henuz CurrentChar null)

        if (_touch.Phase == TouchPhase.Began)
            {
                if (dragChar == null)
                {
                    Char nearestChar = getNearestCharOnTouch(_touch.ScenePoints[0]);
                    if (nearestChar)
                    {
                        dragChar = nearestChar;
                        dragChar.SetStatus(CharacterStatus.DRAG_STARTED);

                        if(dragChar.isFly)
                                Notifys.Instance.OpenNotify("amazingcatch", dragChar.transform.position );


                    }

                }

            }
            if (_touch.Phase == TouchPhase.Moved)
            {
                if (dragChar)
                {
                    Vector3 pos = _touch.ScenePoints[1];
                    pos.y = dragChar.transform.position.y;
                //if (pos.z > dragChar.transform.position.z) pos.z = dragChar.transform.position.z;
                pos.z = Mathf.Clamp(pos.z,  this.transform.position.z, dragChar.transform.position.z);


                    bool isGround = checkDragPosIsValid(pos);

                    if (isGround)
                    {
                        dragChar.transform.position = pos;
                        dragChar.SetStatus(CharacterStatus.DRAG_UPDATE);
                    }
                    lastDragPosition = pos;


                }





            }
            if (_touch.Phase == TouchPhase.Ended)
            {
                if (dragChar)
                {
                    dragChar.SetStatus(CharacterStatus.DRAG_STOPPED);
                    dragChar = null;
                }
            }
        

        
    }

    public override void GameOver(int result)
    {
        base.GameOver(result);

        rope.gameObject.SetActive(false);
    }

    private Char getNearestCharOnTouch(Vector3 _pos)
    {
        Char r = null;
        float dist = 1000f;
        foreach (Char c in CharController.Instance.GetTeamItems(0))
        {
            Vector3 posChar = c.transform.position;
            _pos.y = posChar.y;
            float distCurr = (posChar - _pos).magnitude;
            if(distCurr < TouchNearestDistance)
            {
                if (distCurr < dist)
                {
                    r = c;
                    dist = distCurr;
                }
            }
          
        }
        return r;
    }


    public bool checkDragPosIsValid(Vector3 _pos)
    {
        bool r = false;
        List<RaycastHit> listResult = Utils.RAYCAST(_pos, Vector3.down * 2, new string[] { "Ground" });
        if (listResult.Count > 0) //check is floor
        {

            if(_pos.z < lastDragPosition.z) //karsi tarafa suruklenmesin
            {
                r = true;
            }
            
        }
        return r;

    }

    protected override Vector3 GetMousePosition()
    {

        Vector3 pos = Input.mousePosition;
        pos.z = MainCamera.transform.position.y - inputPositionOffset;
        Vector3 r = MainCamera.ScreenToWorldPoint(pos);
        r.y = 0;

        return r;
    }


    private void OnGUI()
    {
        if (!debug)
        {
            return;
        }

        GUI.Label(new Rect(10, 10, 250, 50), "First Position : " + firstPosition, guiStyle);
        GUI.Label(new Rect(10, 60, 250, 50), "Last Position : " + LastPosition, guiStyle);
        GUI.Label(new Rect(10, 110, 250, 50), "Throw Direction : " + ThrowDirection, guiStyle);
        GUI.Label(new Rect(10, 160, 250, 50), "Throw Direction Mag : " + ThrowDirection.magnitude, guiStyle);
    }
}
