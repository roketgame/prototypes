﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMoving : HyperSceneObj
{
    public float moveRange = 0;
    public float animTime = 3;
    public float[] moveRandomIntervalRange;

    private float interval;
    private float counter;
    public override void StartGame()
    {
        base.StartGame();

        interval = Random.Range(moveRandomIntervalRange[0], moveRandomIntervalRange[1]);

    }
    public override void Update()
    {
        base.Update();

        if(counter > interval)
        {
            interval = Random.Range(moveRandomIntervalRange[0], moveRandomIntervalRange[1]);
            counter = 0;
            float x = Random.Range(-moveRange, +moveRange);
            gameObject.LeanCancel();
            gameObject.LeanMoveLocalX(x, animTime).setEase(LeanTweenType.linear);

        }

        counter += Time.deltaTime;
    }
}
