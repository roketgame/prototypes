﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Ball : CoreSceneObject
{
    public override void Init()
    {
        base.Init();
    }
    public override void StartGame()
    {
        base.StartGame();
    }
    public override void Update()
    {
        base.Update();
    }

    public void OnCatched()
    {
        GetComponent<MeshRenderer>().material.color = Color.green;

        StartCoroutine(destroySelf());
    }

    public IEnumerator destroySelf()
    {
        yield return new WaitForSeconds(2);

        CoreSceneCont.Instance.DeSpawnItem(this);
    }


    public override void OnDeSpawn()
    {
        base.OnDeSpawn();

        GetComponent<MeshRenderer>().material.color = Color.white;
    }

  
}
