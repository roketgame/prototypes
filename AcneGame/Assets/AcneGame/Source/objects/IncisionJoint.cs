﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class IncisionJoint : CoreSceneObject
{
    public Vector2 Direction;
    private Vector2 defaultPos;
    private Vector2 targetPos;
    public override void StartGame()
    {

        defaultPos = GameCont.TOV2(transform.localPosition);
        targetPos = Vector2.zero;
        Direction = ( Vector2.zero - GetPos() ).normalized;
        Debug.Log("GetPos " + GetPos().ToString());
        Debug.Log("GetPos GetAngle " + GetAngle().ToString());


        base.StartGame();
    }

    public override void Update()
    {
        base.Update();

        float angle = Vector2.Angle(Vector2.zero, GetPos());
    }

    public void UpdatePos(float _magnitude)
    {
        float ratio = 0;
        //if (_magnitude > 0)
        {
            float magnitude = Mathf.Clamp(_magnitude, 0f, 1f);
             ratio = 1 - magnitude;
        }

        //Debug.Log("UpdatePos ratio : " + ratio);
        Vector2 newPos = defaultPos + ((targetPos- defaultPos) * ratio * 0.2f);
        transform.localPosition = GameCont.TOV3(newPos);
    }
    public void UpdatePosByAngle(float _angleInput, float _magnitude)
    {
        float tolerans = 5f;
       

        
        if (checkAngle(_angleInput, 1, tolerans) || checkAngle(_angleInput, -1, tolerans))
        {
            UpdatePos(_magnitude);
        }
        else
           {

            UpdatePos(1);
        }

    }

    public bool checkAngle(float _inputAngle, int _side, float _tolerans)
    {
        float angle = GetAngle();
        float inputAngle = _inputAngle * _side;
        float tolerans = _tolerans ;

        bool result = (angle < (inputAngle + tolerans) && angle > (inputAngle - tolerans)); ;

        Debug.Log("checkAngle angle:" + angle + " inputAngle: " + inputAngle + " result: " + result);
        return result;
    }
    public Vector2 GetPos()
    {
        return GameCont.TOV2(transform.localPosition);
    }

    public float GetAngle()
    {
        Vector2 v1 = Vector2.zero;
        Vector2 v2 = GetPos();

            return GameCont.GetAngle(v1,v2);
    }
}


