﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;


public struct FingerPoint
{
    public Vector2 Start;
    public Vector2 End;
}
public class Incision : CoreSceneObject
{
    public float MaxDistanceBetweenFingers = 0.2f; //viewport size(0-1)
    private FingerPoint[] listTouches;
    private IncisionJoint[] listJoints;
    private bool isMultiTouch;

    public override void Init()
    {
        base.Init();
        listTouches = new FingerPoint[2];

        listJoints = GetComponentsInChildren<IncisionJoint>();

        CoreInputCont.Instance.EventTouch.AddListener(onTouchHandler);
       
    }
    public override void Update()
    {
        base.Update();

        if (listTouches == null) return;



       
   
}

    private void updateRigs( bool _status)
    {
        Vector2 input = getInputLine();
        drawLine(Vector2.zero, input, Color.black);

        Vector2 t1 = Vector2.zero;
        Vector2 t2 = input;

        float angle = GameCont.GetAngle(t1, t2);


        float magnitude = (t1 - t2).magnitude * 1f;
        Debug.Log("angle / magnitude : " + angle + " / " + magnitude);
        foreach (IncisionJoint j in listJoints)
        {
            //if (magnitude > 0.4f || magnitude == 0)
            //{
            //j.UpdatePos(magnitude);
            //}
            //else
            {
                if(_status)
                j.UpdatePosByAngle(angle, magnitude);
                else
                j.UpdatePosByAngle(0, 1);
            }


        }
    }
    private Vector2 getInputLine()
    {
        Vector2 v = (listTouches[0].End - listTouches[1].End);
        return v;
    }

    public  void onTouchHandler(RoketGame.Touch _info)
    {
        base.OnTouch(_info);


        if (_info.TouchIndex == 0)
        {
            if (_info.Type == TouchPhase.Began)
            {
                listTouches[0].Start = _info.GetPoint(0);
            }
           
            if (_info.Type == TouchPhase.Moved)
            {
               Vector2 endPoint = _info.GetPoint(1);

                Vector2 dist = (endPoint - listTouches[0].Start);


                //Vector2 dir = dist.normalized * -1;
                if(!isMultiTouch)
                {
                    listTouches[1].Start = listTouches[0].Start + (dist.normalized * MaxDistanceBetweenFingers);
                    //listTouches[1].End = listTouches[1].Start - dist;
                }


                float magBtwStarts = (listTouches[0].Start - listTouches[1].Start).magnitude;
                float magToSecondFingerStart = (endPoint- listTouches[1].Start).magnitude;
                //Debug.Log("distBtwStarts.magnitude " + distBtwStarts.magnitude + "dist.magnitude " + dist.magnitude);
                //if(dist.magnitude < distBtwStarts.magnitude/2   )
                if(magToSecondFingerStart > magBtwStarts /2  && magToSecondFingerStart < magBtwStarts)
                {
                    listTouches[0].End = endPoint;
                    if (!isMultiTouch)  listTouches[1].End = listTouches[1].Start - dist;
                    updateRigs(true);
                }
                    


            }
            if (_info.Type == TouchPhase.Ended)
            {
                //listTouches[_info.TouchIndex].End = _info.Points[1];
                listTouches[0].Start = Vector2.zero;
                listTouches[0].End = Vector2.zero;
                listTouches[1].Start = Vector2.zero;
                listTouches[1].End = Vector2.zero;
                updateRigs(false);
            }

            //if (listTouches.Length > 0)  
                //drawLine(listTouches[0].Start, listTouches[0].End, Color.red);
            //if (listTouches.Length > 1) drawLine(listTouches[1].Start, listTouches[1].End, Color.green);
        }
       
    }

    private void drawLine(Vector2 _start, Vector2 _end, Color _color)
    {
        Vector3 v1 = new Vector3(_start.x, 0, _start.y);
        Vector3 v2 = new Vector3(_end.x, 0, _end.y);
        Debug.DrawLine(v1, v2, _color, 0);
    }

    void OnDrawGizmosSelected()
    {
      
        //if(listTouches != null)
        //{
        //    if (listTouches.Length > 0)
        //    {
        //        Gizmos.color = Color.green;
        //        Gizmos.DrawSphere(listTouches[0].Start, 0.1f);
        //        Gizmos.color = Color.blue;
        //        Gizmos.DrawSphere(listTouches[0].End, 0.1f);
        //    }

        //    if (listTouches.Length > 1)
        //    {
        //        Gizmos.color = Color.red;
        //        Gizmos.DrawSphere(listTouches[1].Start, 0.1f);
        //        Gizmos.color = Color.yellow;
        //        Gizmos.DrawSphere(listTouches[1].End, 0.1f);
        //    }
        //}
      
       
    }
    //private void updateTouch(int _index)
    //{
    //    //if ((UnityEngine.Input)
    //    {
    //        //UnityEngine.Touch touch = UnityEngine.Input.GetTouch(_index);
    //        Vector3 pos = Vector2.zero;

    //        if (UnityEngine.Input.GetMouseButton(0))
    //        {
    //            pos = UnityEngine.Input.mousePosition;

    //        }
    //        //if (UnityEngine.Input.GetMouseButtonUp(0))
    //        //{
    //        //    //startTouch(Input.mousePosition);
    //        //    endTouch(UnityEngine.Input.mousePosition);
    //        //}

    //        listTouches[_index] = Camera.main.ScreenToViewportPoint(pos);  
    //    }

    //}

}
