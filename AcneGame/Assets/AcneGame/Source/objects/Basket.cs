﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class Basket : CoreSceneObject
{
    public int score = 0;

    public override void Init()
    {
        base.Init();
    }
    public override void StartGame()
    {
        base.StartGame();
    }
    public override void Update()
    {
        base.Update();
    }

    public override void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);


        if(GetStatus() == GameStatus.STARTGAME )
        {
            Ball target = collision.collider.GetComponent<Ball>();
            if (target)
            {
                score++;
                CoreUiCont.Instance.GetPageByClass<PageGame>().SetScore(score);
                target.OnCatched();

                if(score == 10)
                {
                    CoreUiCont.Instance.GetPageByClass<PageGame>().OpenPopup<Popup>();
                }
                
               
            }
        }
       
    }
}
