﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;
using UnityEngine.Events;
using TMPro;

public class PageGame : PageUI
{
    public TextMeshProUGUI TxtScore;
    public override void Init()
    {
        base.Init();
      
    }

    public void SetScore(int _score)
    {
        TxtScore.SetText("Score : " + _score.ToString());
    }



   
}
