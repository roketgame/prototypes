﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoketGame;

public class GameCont : CoreGameCont
{
       
    public override void Init()
    {
        base.Init();

        //CoreUiCont.Instance.OpenPage<PageIntro>();

    }


    public override void StartGame()
    {
        base.StartGame();

        //CoreUiCont.Instance.OpenPage<PageGame>();
    }

    public static Vector2 TOV2(Vector3 v)
    {
        
        return new Vector2(v.x,v.z);
    }
    public static Vector3 TOV3(Vector2 v)
    {

        return new Vector3(v.x, 0, v.y);
    }
    public static float GetAngle(Vector2 v1, Vector2 v2)
    {

        Vector2 dist = v2 - v1;
        float Angle = Mathf.Atan2(dist.y, dist.x);
        float AngleInDegrees = Angle * Mathf.Rad2Deg;
        return AngleInDegrees;
    }
}
